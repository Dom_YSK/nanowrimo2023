# Zamir

The farmer is in his late thirties, he's spent the last twenty years working the land.

He is specialised in recovering old farmland that's lain fallow since the war of the gods, when most people died. As the
population grows again, they need to recover old farmland. 

He is as religious as anyone else, not hard when the god are literally manifesting in the world regularly.

He loves eating beans and lentils, he's quite the fan of fishing and in his spare time, he plays darts at the inn. 

He's well respected, had a few liaisons, but generally keeps to himself. He loves his work, he is content



Driving characteristics: 

Enjoys some social time, also very happy by himself
