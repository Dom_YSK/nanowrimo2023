Chapter 1) 

The Farmer is asked to help open a new/old field. He agrees and then starts to investigate it. Uncovers a large stone, 
which gives him a shock when he touches it. He later falls into a fever sleep, and wakes up a couple of days later with 
a voice inside his head.

Chapter 2) 
The farmer visits the healer, who tells him to go to the temple. The Gods will treat mental illnesses, but in this case
the god can't even _see_ the farmer until they manifest a physical body. When they do they explain that the farmer was
right to 