# Scion of the Banished Gods

The Gods were killed by the new generation of gods a thousand generations ago. The new gods took away our ability 
to be heroes, to cast magic, to level up. Every day we tithe our potential to them instead. 


They in turn keep us safe, there's no war, no natural disasters, every harvest is great. Life is idyllic. 