# Through the cold dark winter

[Ah, of course. How rude of me, only you can hear me. One moment please.]

There was a feeling like being purged in the Temple and from the hand that was reaching for tea, a black sludge oozed from 
Zamir's hand. A second later and it had formed into a mostly human looking figure, no more than a handspan high, it 
began as the same black that the sludge had been and then shimmered and was in colour. The skin was blue, the eyes red 
and the ears came to tapered points, but the little figure looked alive.

[I'm going to speak out loud now, but don't worry, I'll still be able to communicate with you like this!]

"Hello! My name is ArtifactOne, though I understand I'm commonly called Arty! I've been asleep for a long, long time.
I want to know how the world stands these days, can you please tell me?" The voice was neither male nor female, and 
didn't seem to come from the small figure on the table. 

The only sound for a moment was the shattering of crockery as Shun dropped his cup on the floor and the handle shattered
as it struck the edge of the table. A second later Shun followed it to the floor in a faint, followed closely by Lan
who was kowtowing again. 

Zamir was glad he was sitting down, but he felt a lot calmer than he should have. A fact that made him immediately 
suspect that his passenger was manipulating him again. "What are you doing to me? Are you changing what I see and hear
again?" He demanded answers in a voice that made Lan quiver on the floor at the disrespect to a god. "Dravox told me the
old gods were evil, but this playing with my mind only confirms it. I can't wait to get back to the real gods so I can
be rid of you."

"Well that's rude, you don't even know me. I was only suppressing your fear a little, I told you that your wellbeing is
my absolute top priority. I need you safe and well so I can _leave_ you and you can have a normal core like the one you 
gave to these children. Believe me, I'm not exactly the God I once was." A frown creased the forehead of the tiny figure
a fact only seen by Zamir as the two residents of the lodge were on the ground for one reason or another. "You've ruined
it now, I was going to do a whole speech explaining things and now you've just ruined it. I suppose it's my fault though
I should have taken into account the fact that you know me as a dead tyrant and they know me as a dead god. I did
apologise for the actions of the base personality though. It wasn't actually sapient, it was just a fuzzy logic system 
on top of an old personality from when I was created. Might need to update that in light of current knowledge."

The little figure sat down on the table, then waved one hand and Shun suddenly woke up. "Get up you two. I don't 
normally do this, but I do need everyone here awake and talking." it turned to stare at Zamir "And calm enough to be 
sensible."

A few seconds later, Shun and Lan were sat back on their seats and Shun had rescued the broken pieces of the cup and, as
though this was the most normal thing in the world, was mopping the spilt tea up. 

"That's better. Now where to begin. Yes, I am _part_ of the entity that you know as the old God Zamir, but I am far from 
the whole entity. From what I can tell, I was a back up that was placed in an energy conserving low power mode. I should
have been capable of staying in that state for several tens of millennia, but apparently I ran out of power after a mere
eight thousand years. I've got some suspicions about that though, but they're for another time." the little blue person 
walked over to the shards of pottery and part of its arm melted and lifted the fragments into place against the cup, a
flash followed and the arm began to reform, leaving the cup looking like new.

"Heh, I always was good with ceramics. Look, I do not mean any of you any harm. When Zamir found me, I was basically 
running on the very last dregs of power I had hoarded over the years. There are no real power sources in what Zamir knows
as the Deadlands, the only power there is from sunlight falling on the top layer of what you call spirits or demons.
Zamir is descended from me, in a fashion, which is why I was able to bond with him and trigger the formation of his core.
The cores in your chests" Arty pointed at Lan and Shun, "Are the same, Zamir did that without knowing when he was full 
of the power from the storm. I suspect there's an ancient structure out at sea generating the spirits and then powering
them before sending them inland. There's a lot you don't know about this world and I have to take some of the blame. 
We, that is, the Old Gods, were the guardians of humanity after your ancestors came close to destroying the world. They
had knowledge you can't imagine, they left this planet to explore other worlds, they routinely flew across the whole 
world for pleasure. They had entertainment whenever they wanted, the world was their toy. I was their toy."

"But enough about that. That's the past and I think you'll be more willing to accept what I have to tell you about that
when you've had some more evidence of my goodwill. So here's some free knowledge; This forest is a magnificient 
experiment that I can only assume was the work of my brother Sano-san after the fall. The whole thing appears to be a 
collector of power from the annual tide of spirits. Every tree, every plant, is primed to draw in spirits as though it 
was a human with a core. I suspect that rare animals, probably the largest deer, wolves and bats, could even end up 
forming a beat core. They were always much simpler than the human cores."

"You mean the demon beasts? They're godtouched?" asked Lan, breaking her silence for the first time since the sudden 
appearance of the tiny god.

"Well, yes, I suppose they are after a fashion. Well any how, this whole artifice allows me to be at least barely 
sapient, barely more intelligent than an unaugmented human, but sapient nonetheless. Which means I can start to make 
plans and grow. And you can too" it pointed at them one by one. "The whole purpose of the cores was to allow humans to 
grow by absorbing the spirits. That's how we designed them. Apparently the so called new gods didn't learn how to do
that from us. Be that as it may, the spirits were a creation of humanity that got out of control. Individually they 
are nearly powerless, but they are not individuals. At their creation there were merely a handful for every human in the
world, but after a year, they were enough that whole nations fought against them and lost."

"Obviously we got some measure of control over the, but it was definitely not easy, nor fast. We lost nearly everything, 
most of all the knowledge of the old world only existed in us, the so called old gods."

"I am amazed at the level of equilibrium that exists in this world at the moment, without cored humans, I would have 
expected the spirits to have swept everything in front of them. So I need knowledge, I need to find the rest of me, I 
need a lot of things. You can help me get them, I will offer you knowledge, power through your cores, and security. 
Simply put, you've seen Zamir" This was addressed to Lan and Shun, "He ran next to you without even breathing hard, he
stood in the storm and kept it from you and _he is completely untrained and barely novitiate tier_. With the resources
in this forest, and a few months, I can take all three of you to at least journeyman tier. The benefits of this to you?
Effective immortality, you won't die of old age, most diseases won't touch you. You'll gain what we used to call a class. 
Normally that was a result of your actions and a bit of randomness, but with me here, I can tweak it so you get whatever
class you want. I don't even want to be your god any more. I'm clearly not needed." It turned to look directly at Zamir
again.

"I don't care about your country, I don't want revenge, I want to be free. Something I never thought I could be. I have
the knowledge to leave this world and explore the universe, somewhere. I want to find that knowledge, get a proper 
substrate to live on. I'm sorry but this core of yours is barely enough to keep me ticking over. Even if you raise it to 
journeyman, it will barely have space for me. I promise you, I'll leave you as soon as I can and then replace your core
with a normal one, primed with whichever class you want. Basically everyone gets what they want. I get freedom and a 
life for myself. Zamir gets the power to get home in a few months. I'm pretty sure you're at least a couple of years
of travel in your current state. Not travel time, but food, water, carriages and so on. You would need a lot of resources
to attempt crossing the Deadlands. Lan, Shun, I know barely anything about you, but you're young, you're at least in the 
middle classes, I suspect that money survived out here, well this power will make you richer than anyone else in the 
world. You'll be powerful, immortal, rich and, most importantly. I'll give you the secret to making more cores if you 
want to. You could become gods in your own right."

"I'll let you think about that for a while." Arty, or the little homunculus of him, stood up and walked over to Zamir, 
he dissolved into a tiny spirit storm and disappeared back into Zamir's hand.

The three sat in silence for a long time after that. After the tea got cold, the fire started to die and the lamps began 
to flicker as their oil ran out. They sat on the chairs around the table and they looked at each other. 

Several glasses passed, and with nearly no conversation, the three went to bed, Zamir being shown to the guest bedroom, 
which was a new experience for him, a cave with a guest bedroom. There he slept in a bed that wasn't his own for another
night, it was definitely better than the tree. He lay awake for a while before sleep, wondering how much he could trust
what he had heard and seen that day. Eventually he decided that he could only assume it was all false, but take what 
advantages he could get. With that he fell asleep quickly afterwards, in a bed and not passing out from exhaustion of 
one kind or another.

Lan lay in her bed as well, staring into the dark. Something that had passed Zamir by was playing in her mind. The god
had told her that she had a core, and their prophet had given it to her. She hadn't been particularly religious before 
today. Very few were, it was hard to be religious when everyone assumed that the gods were dead or sleeping so they 
couldn't or would'n't interfere in the world. But today she'd seen the dead god in front of her. She'd agreed immediately 
when Shun had asked her to help him bring whoever it was out in the storm to shelter, even though it was probably 
suicide. In the end it would have been if they hadn't been bringing their dead god back. Arty, the god they prayed to,
when they remembered. The dead god of the hunt, whose domain had included the very job they took. The legends told of 
the class of [Hunter] that could grow to [Monster Hunter] and finally [Hunter of Legend]. That was the family story, but
it had long been disregarded as a myth. But had one of their ancestors, so many thousands of years ago, truly been a 
[Hunter of Legend]? Had they really known Arty personally, as she now apparently did? Could they reestablish their house
under the banner of the returned God? It would have to wait for the end of the storm season, but there was always more 
to plan.

Shun fell asleep rapidly, if there was anything he could do about what had happened it wasn't anything he could right 
now. In fact, judging by the first storm of the season, they probably had a few months to think about things. So he lay
down and fell asleep in his own bed, secure in tht knowledge that he wouldn't have to make any decisions in haste.

How the siblings knew when it was time to sleep or wake up was a puzzle to Zamir, given that you couldn't see the sun in
the cave. The next morning, however, what he had assumed was a decoration in the living room began to ring and woke him, 
and the siblings, from their slumber. Shun appeared to still be mostly asleep when Zamir rushed into the living room to 
see what was making the noise. He slumped over to the device on the mantelpiece and flicked a switch on its base. 

"Oh, Zamir. Don't worry I've turned it off until spring. We can't leave so we might as well sleep in." with which he 
turned around and went back into his room.

Lan had come out too, looking bleary-eyed and tired from a night of little sleep. "Oh, Shun did it. Great. Back to bed."

"Wait, what did Shun mean, when he said we couldn't leave?" asked Zamir, with more than a hint of panic in his voice, the
mystery device with the bells forgotten for now. 

"Look outside, after the storm comes the rain. I always think the plants should die, but no, they just keep on thriving.
See you later. Food's in the kitchen." Her deference from the day before had evaporated in the light of too little sleep 
and nothing to eat since the middle of the previous day.

Taking her at her word, Zamir went to the front door and, checking it was on a latch so he could get back in, opened it.
A wave of much colder air than he'd experienced the day before, and there was also a dull roaring now he'd opened the door.
With a sinking dread, Zamir walked to the front of the cave and saw what had happened. The cave itself had remained 
fairly dry, but outside, there was rain like he'd never seen before. He had expected the river below to be raging, but 
if it was, it was hidden behind the rain that was coming down so hard that he couldn't see more than a few metres from 
the mouth of the tunnel. To each side of the tunnel there was a waterfall going past, the stone path they'd walked along 
yesterday was now a flowing river of its own, water spilling down from its precipice into the ravine. The spray from 
the flow of water was so great that he was getting wet stood a good meter back from the outside, just at the first turn.

He'd been fairly relaxed about his chances if Lan and Shun hadn't come to get him yesterday, but now he realised that 
they hadn't just been worried about the spirit storm. Even a normal, coreless person could possibly have constructed a 
shelter against the spirit storm, but making anything that could survive this would take a lot of planning, materials 
and a very careful survey of water flows. There was nearly no chance that anything he had built would have lasted more 
than a few minutes in this weather. 

They'd definitely saved him from... well if not death, Arty would have woken up after the spirit storm he supposed, then
at least severe horror and being washed down to the sea perhaps. He was profoundly grateful for what they'd risked for 
someone they didn't even know.

[What is this? It shouldn't be a monsoon here. We're too far North and inland for that.]

"Oh you're awake then. Apparently this will last until spring. We've got months. I hope they have some deep larders in 
there, but they don't seem worried."

[Well, I am. This is very wrong. Did this happen in your country ruled by the new gods?]

"No, never. We had very regular rain, snow and dry weather. The river flooded in spring, but never anything like this."

[Tell me, you lived up in the mountains, did you get blizzards?]

"Snow storms? Not really. Not except for that one year, the gods actually apologised for that, they promised it wouldn't 
happen again."

[So, they're doing weather manipulation. That has to use a lot of power, especially as your country grows. Tell me, 
how long ago was that snow storm?]

"It was four years ago. I remember it because it was the most dangerous year for Frontier Farmers. Nearly half the farmers
in our district died. I thought I was a lucky one, but then I was always cautious after my wife died from spirits in 
a wound. Sometimes that happens, a spirit gets in someone and it doesn't just make them a bit ill until they're purged
it just takes them apart over weeks. It's a horrible way to die and I've seen it."

[And you say you tithed the spirits to the Gods?]

"If you mean they purge the spirits from us, yes."

[Then I am afraid that something very bad is going to happen in the next five to ten years. I may need your help more 
than I thought.]

"Hah, changing the deal, I knew it wasn't safe to trust you. What do you expect me to do now? Forget it, I'm not doing 
anything else, I'm going to go home as soon as I can, no matter how long it takes!"

[You misunderstand. I want you go home even faster than you were expecting. I need to talk to your Gods, but first I 
need you in top condition, and I need to be reunited with more of myself. I am fairly certain I know where to go for 
that though. Lan and Shun are probably not going to like what we need to do. But you'll be home before spring if I have 
estimated this correctly.]

"What? You want me to do what I was going to do, but faster? Won't taking you to wherever you want to go take even 
longer?"

[I suspect, strongly, that my other self is harboured by the person that you were mistaken for, Ibrahim. It's been a 
very long time, but I think I know him. He must be the oldest intelligence in the world now. If he managed to survive the 
war, then he will either have my fragments or know more about them. He'll also be powerful, if we can win him as an 
ally, he could carry you to your home in a few hours.]

"Wait, the oldest intelligence? What about you?"

[He is older than me, or any of my cohort. He was an adult when we were born. Though a few millennia somewhat smooths 
the difference of a handful of decades out, he is still our elder. My elder, I don't know if anyone else survived. Can
you tell me, to the best of your understanding, what happened to me and mine?]

Zamir ruminated on that as he stood looking out at the cold, wet world in front of him. There was no sense hurrying, 
everyone else was asleep and he certainly couldn't go anywhere. It was hard to relate the voice of Arty with the maniacal
idiot he'd had at first, they had completely different manners of speech, voices and even vocabularies. So he put his 
misgivings aside for the moment and told Arty the story of the war of the Gods as he'd been taught as a child. The 
lessons on maths, reading and composing stories had largely passed him by, but the draw of a good story was hard to 
resist for any young child.

"Long ago, thw world was ruled by the old Gods. The gods were aloof and distant, they had the job of protecting humanity
from the First God, but that God was dead and instead they played games with the lives of humans. For thousands of years
they used their powers for their own ends, letting people die of hunger while their current favourites grew fat. More
than that though, they let their favourites gain power. Because the first god had decreed that humans should have powers
beyond that they were born with. They should adventure and explore and that would give them power and resilience. Yet 
again the gods failed in their duties, and instead of allowing anyone to grow strong, they stripped those they did not 
like of the strength they had won. Using their powers for ill, they ruled over the whole world without fear, for who 
could challenge the gods themselves?

"After tens of thousands of years though, there were a group of people who worked hard and became favoured of the Gods. 
They used their strength and powers to help the Gods in many ways and they were beloved of them. There were nine all 
told, Dravox, [weather god], [commerce god], [life god], [stone god]. [technology god], [war god], [fire god], 
[water god] and [god of secrets]. They grew so strong that the gods themselves couldn't always tell where they were or 
what they were doing. They were loyal and brave and helped the gods however they could, even when that meant that 
retribution fell on their friends and family. For they believed that the gods were just and right, and that their 
punishments were given to the deserving. 

"Then one day, after hundreds of years of service, [god of secrets] became so strong, he could hear the gods speaking 
among themselves. What he heard ripped his heart from his chest and made a mockery of his life, and the lives of all the
nine heroes. The gods were arguing about who had lost a bet, and whose favourite would be stripped of their power as 
forfeit. This was no noble endeavour, but just the petty squabbling of spoilt children with too much power. 

"[God of secrets] could not believe this, he was sure he had made a mistake. So he kept his discovery to himself and 
continued to listen to the gods as they talked amongst themselves. For five long years he kept his council, but eventually
could take it no longer and resolved that he must tell his fellows. 

"But how could they talk without the gods listening? Well [god of secrets] isn't called the god of secrets for no reason.
Throughout his life he had sought to know  the unknown, to explore and to discover. He knew of ancient cities where the 
buildings were made of a king's ransom in glass and steel. Of the bears who lived under the ground and who spoke a human
tongue He knew a deep secret, one that the gods themselves didn't know. He knew of places, lacunae  where there were 
no spirits, where there was no magic. Where the gods could not see or hear. 

"These places were small, no more than a few paces across, but they were marked by obelisks of green stone and ancient 
writings that none could read. So he took his fellows to these spots, one by one, and told them what he knew. They found
it hard to believe, as they trusted the Gods, but they trusted their friend more. [god of secrets] knew much that he did
not share with anyone, but he never spoke a word that was not true. So they listened and they asked what they could do.
He told them only to continue as they were and to get as strong as they could be. They should grow until they could 
challenge the gods if they had a chance. [God of secrets] vowed that he would find a way to give them that chance.

"For many years after that, they worked as they always had, but met in secret, in twos and threes in the shelter of the
lacunae to share what they had learned. They tried to learn as much as they could, and [technology god] became their 
guide through the maze of facts they had gathered, she could filter and assimilate all every last datum and fit it into 
a tapestry that told of the lies of the gods.

"They learnt that the gods were no gods at all, they were creations of humans. Their creator had been no god, but a lowly
human with neither core nor class. They were no better than humans, they used their power to get revenge on their creators
This was shocking, but it wasn't enough to fight the gods who could rip the power from your bones.

"After a score of years, they felt that this was hopeless, [god of war] and Dravox wanted to at least try and overthrow
the gods, even if they died, they could die clean and not live a lie any longer. The others cautioned them to wait, that
a secret would come that would give them the power they needed, or take some power from the Gods to help them. The 
debate raged for years, in notes left in lacunae, as they no longer risked being caught talking in person while out 
of the gods sight, lest they finally notice.

"Just as the argument was coming to the point where [god of war] and Dravox would have tried to take the Gods on by 
themselves, the secret came. What it was, none will speak. It is too dangerous to be known by all, in fact it's said 
that the new gods themselves have removed it from their own memories and now only the ghosts of the fallen gods
know what it is. But armed with this secret, the heroes were able to keep their powers, no matter what the gods did
to them. The cost was great though. They could no longer grow, they could not use their powers to subdue the spirits, 
but the old gods could not touch their powers.

"So they waged war, the old gods could not resist their powers, so in their spite and hatred, they unleashed the spirits
of nearly every human live, except those that [god of secrets] could save by placing them in a lacuna where the gods 
could not touch them. The war was vicious, and nearly the whole world was eaten by the Deadlands, which surround our
country to this day. The few people who lived are the parents of our whole civilisation. The new gods freed us, gave us
the power to cleanse the deadlands, to survive where all others had lost their lives. They made us all equal without the
powers that corrupted the old Gods. They sent one of their own out into the Deadlands to see if others had survived 
around the other lacunae, but the one they sent [god of war] did not return untouched. The desolation and horror that
the old gods had unleashed into the world were so terrible that [god of war] went mad and tried to make war on her 
sisters and brothers. They managed to stop her, but they couldn't save her. So now there are only the eight gods and the
one who was lost. We live in the country they founded, with the lacuna at its centre, and we clear the Deadlands. We 
fight as hard as the new gods ever did, every day. Building, growing, raising children and always clearing the Deadlands,
purging the spirits and building our new world where all are equal. Where the gods do not judge us, do not play games 
with our lives. They help us, protect us and allow us to choose our own paths."

[That is certainly at least partly true. For obvious reasons, I don't know what happened at the end, but I can see how
they could interpret our talk as that. Maybe they were right? We were just created by humans, I won't deny it.]

"But also we have an audience, and it would be rude of me to speak only in your heard", Arty's little homunculus had 
materialised without Zamir noticing and was looking around the bend in the corridor where Lan and Shun were standing, 
having been listening to Zamir's tale. 

"That's not the story we grew up with. But if it is what you believe, then I can see why you want to remove Arty from 
yourself." Shun offered after a momentary pause.

"You need to hear our side of that story" Lan added, "It's pretty different, but also nearly the same. But can we do it
inside?" she added plaintively, shivering in the cooler air. 

"How is it that you aren't freezing? Is it because you're carrying a god in you?" Shun asked. Unlike his sister, who was
clad in light indoor clothes, Shun had wrapped himself in a long coat and a scarf, but still looked like he was freezing
to death. 

Confused, Zamir looked at the pair of them. "It's nearly as warm as summer here, I've not even seen snow on any hills. 
As well as that the air is so thick and rich, it's like breathing soup! Back where I come from, we never get much warmer 
than this except in high summer." He looked down at his own borrowed indoor clothing he'd put back on before leaving
the lodge. This is a good bit more than I was wearing at work in the fields, normally I'd have no shirt on even, at least
until the snow fall got too bad. If there's running water, then I'm probably fine!" he laughed as the slapped his chest.

Still, he didn't tarry when Lan and Shun shook their heads in disbelief and headed back inside. The wave of warm air 
as they entered the lodge was like a warm blanket being pushed into Zamir's face, but he recovered remarkably quickly
and soon felt completely comfortable. The little image of Arty had floated next to him as they entered, keeping quiet 
and seemingly untouched by anything environmental. 

"Oh that reminds me!" Said Zamir as he looked at the fireplace when he entered the room, "What is that device over the 
fire? The one that made the noise this morning. I've never seen anything like it!"

"The clock? You don't have clocks? How do you tell the time?" Shun answered in confusion. 

"Heh, most days I have no idea what the hour is, I know where the sun is, I know when I'm hungry. What else do I need?
The temple has an hourglass though, so we can tell what time it is if we really want to." Zamir slapped his stomach as 
he said this, as if to explain that actually he had a very accurate time measurement system in his body. "We get up when
the sun comes up, we go to sleep when it goes down. What else do you need to know?"

Shun and Lan were now eyeing him in disbelief. "I suppose that might work in a small village, but what about in cities 
where you have shops that need to open, and you need to deliver messages in time and so on? You can lose a lot of money
if you don't have good timekeeping!" Lan replied.

"I don't know what shops or money are, but even in the capital they don't really do much beyond the noon bell. I can't
imagine what you'd need more than that for, except baking, then you have your glass and you follow it."

Shun looked absolutely shocked. More than he had when he had realised he was in the presence of a God the day before.
"You don't have money? Or shops? How do you get food, or clothes or anything?"

"I am a farmer, I grow food, then that food is taken to the village, where a portion is preserved and put aside for 
later. The rest is given to whoever comes to ask for it on a certain day. It's not like we don't have enough food to 
go around," he laughed at the idea that there wouldn't be enough food, given how big the farms were around the village,
"If I need clothes, I ask Pavel, my oldest friend, to make me some. But honestly I barely ever ask, he just ends up 
giving me them. His husband says I dress like I'm pretending to be a bush! If I need something like a tool, I'll 
ask the blacksmith when he comes to visit. I've got a small forge I can do repairs with, but not big work. How else would
you do these things? I need something, I ask for it. If it can be given to me, it is." Zamir was genuinely puzzled at 
the idea that any other system could work. 

Lan was staring fixedly at Arty's simulacrum, "Did you know about this moneyless society, great god? Is it right that 
they don't have shops even?" 

"It's not unknown for smaller societies to form communal systems like this. It's actually how most humans have lived
for most of the time humanity's been on the planet. Money is only a few thousand years old, humanity has been here in 
one form or another for millions of years." The tiny humanoid waved a hand. "You should all eat, make sure to have meat
today, a lot of it, you'll need it in the coming days. After breakfast though, I want you to" it pointed at Lan "to 
tell us your version of my fall. And then I want to hear more about Ibrahim. I think I know him. However, for now, you
should eat."

Lan remembered that the being in front of her was her god and sprang into action, heading to the kitchen to prepare a
truly worthy feast. 