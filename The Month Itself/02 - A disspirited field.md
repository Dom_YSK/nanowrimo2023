# A Disspirited field. 

The walk back to the field he was cleansing was more than the two fields it had been when Udema was alive. In the 
years since, he had expanded the farm markedly, when his children left home, there was little else for him to do 
after harvest and before sowing. Only the depths of winter could slow him down. This meant his walk back to his little 
warded enclave in the mostly cleared field took him a while, long enough to remember that there was actually more to 
life than reclaiming the Deadlands all day. It might even be nice to spend a winter in town, just working on his cooking
perhaps, maybe purifying the well water for people. There were a host of things he could do that didn't require him to 
be lightly clothed in the freezing air at his age. As he divested himself of his outer layers again, and a particularly 
cold breeze came down off the mountains, smelling like the promise of snow. 

Pulling up his totems and carrying them in his arms like children, he made another octagon, this one covering about half
of what he had cleared so far, making sure he wasn't going to lose what he'd claimed so far. So it went all afternoon;
the totems, the mat, the blood, the care, the call.  This process was like a balm on his soul, he had an objective, he
knew how to reach it, he had a clear sign he had succeeded every time he completed a step. It was just complex enough to
occupy his mind, but not so taxing that he couldn't rest. It was a lovely state of thinking without really thinking, 
and time passed without his noticing as he pushed ahead. At this time of year the sun sank behind the mountains only a 
handful of hours after lunch, so he was surprised to find that he was facing the last corner of the field while the sun
still stood above the mountains. 

This part of the field was the closest to what looked like the ruins of an antebellum structure, they were dotted all 
through the Deadlands. They made cleansing a field more arduous, as the ruins were often thick with spirits, and 
uprooting the ancient stones and moving them back to the village was backbreaking labour. The benefit was that even
after thousands of years, the ancient stones were cut as sharp as a midsummer shadow, seemingly untouched by the spirits
or time. This field was no different, though the actual ruin itself was in an adjacent field, there were large blocks 
scattered over this corner, often the ruins looked like a giant had kicked the building down, scattering it over the 
nearby ground. Zamir paused for a moment, assessing his approach to this final corner. Had it been clear like the rest 
of the field, there would be two, perhaps three more cleansings needed. This wasn't going to be the case here though, the
blocks were positioned such that the normal layout of a regular octagon would be disrupted by them. It was also true 
that you never wanted more than one block in an octagon if you could avoid it, the spirits loved the blocks and they 
were often incredibly densely populated by spirits. Cleansing just a block and the ground immediately around it was 
preferable, even if it meant you then had to cleanse a larger octagon which included land you'd already cleansed. 

_Huh,_ he thought, _Maybe I do have some things to teach an apprentice._ He imagined having one of the villages young
teenagers following him here. He'd lift his hand up and point at the corner, showing them that, given the fact there 
were three blocks and the two in the far corner were very nearly touching each other, the first two patterns should be 
around those. The block closest to the rope marker for the Easternmost edge of the field, the farthest point from the 
village, should be cleansed first. He'd take his finger and draw in the cleansed soil around his feet, sketching out the 
patterns he'd use. Make sure the totems are aligned so the line between the two closest to the blocks does not touch 
either block. The last thing you wanted was to cleanse half a block and then have it be corrupted again moments later. 
The spirits loved those blocks and would call in others to fill in any block they were in if it wasn't already full to 
bursting. Next, you'd clear the matching block in that pair, allowing the whole corner to be cleansed as once later. 
The third block would be the same as the first two, and that would simply leave three normal cleansings. It wasn't so
bad, he'd certainly seen worse in the past, but even for him it was going to be a tight fit to get everything done before
sunset. So it was probably time to get started then. 

As he placed the first octagon to clear that farthest block, he noted that these blocks looked somewhat different to 
those he had seen before. Darker in colour, almost black instead of the light grey they normally were. Still, every 
ruin was different, and this was about as far into the Deadlands as anyone had ever been, in this direction. It was 
possible that the fishers in their warded boats had gone further into the Dead seas, but Zamir was fairly sure, 
especially given what had been said today, that he was now as deep into the Deadlands as any human had ever been.

Just about as far as it was possible to be from the capital and the Grand Temple ensconced there, was he really further 
from the Gods than anyone else? He pushed that thought to the back of his mind, he'd be in the local Temple tonight, 
in just a few hours and he'd have the blessing of the Gods then, when they removed the spirits from him and cleansed 
them. His hand drifted towards his sternum of its own volition as he thought that. The spirits which apparently made 
other frontier farmers cry out with pain, or have to be carried to the temple after each day's work, that left him feeling 
nothing but more hale and hearty than he ever did. He had often wondered, what it was that made him different to other
people in that way. He wasn't alone though, far from it. Many people in the village showed no signs of sickness before 
their weekly visit to the Temple to be purged of the spirits that had built up in them, drifting over the fences on the
wind, or from a handful of water taken directly from the river. 

Most of the village, most people, got a bit sicker, a bit weaker, as the spirits started to work on them, before they 
were drawn out and purified by the Gods in the Temple. But a substantial minority were like Zamir, they showed no ill 
effects from the spirits, indeed they seemed to thrive on them. No one suggested not getting cleansed though, every now 
and then the hunters would find some poor animal that had managed to wander out into the Deadlands and come back, they
were invariably half-dead and barely able to move around, clearly in incredible pain. Those were put down quickly, and 
a frontier farmer like Zamir was called in to cleanse the corpse and the ground it had passed over. 

He shook his head again, this was no time to get distracted, the ground was rich with spirits. If the octagon did not 
need to be as close to even as possible, he would have put it only around the block, but as it was, he had to... Zamir 
paused, pulled up the totems and began to place them next to the block, ready to cleanse the ground he would need to 
include when clearing the block. It was two extra ceremonies, but he'd promised Umeda to be as safe as possible. So 
he took the extra time, took care when re-opening the prick on his finger, which had healed quickly this afternoon. He 
was careful during the incantation and the call. Then he prepared himself for the first block. 

It wasn't that he'd never cleansed a block before, far from it, there were fields of his, now growing food for the 
village that had been nothing but blocks, whole floors of ruins. Those were tricky since you needed to find a way to 
keep the totems planted without them moving while the ceremony took place. Still this was a solved problem and Zamir had
carved wooden bases for his totems when he couldn't plant them in the ground. However those cleansings were not so 
challenging as this one. There were more spirits in each of these blocks than there were in any four cleansing rituals
he'd performed on a ruin before, each encompassing many blocks of this size. The sheer quantity of spirits in this block 
was making his hair stand on end as he got close to it. 

This was no time for half measures. He carefully placed the first totem at the vertex of the block, and then paced out 
the square which would make the first four points of the octagon. Technically it was possible to make the ritual with as
few as three totems, the invisible lines of power that split the world into inside and outside would work. However those
lines would be weak, easily overcome by a surge, or a too strong calling. Increasing the number to four would help, five
 was better, in fact the more totems you used, the stronger the walls were and the safer the ritual. The problem lay in 
the fact that you had to mark out a good, regular shape, and that simply carrying and placing many totems would slow 
you down. Also they were expensive and difficult for the Temple to make, they were durable and many had been passed down
from farmer to farmer over the centuries though. 

But Zamir did not espouse the use of fewer totems. No frontier farmer who lived more than a few years did. Honestly if 
he could, he would have gone out to a nonagon, but measuring that shape by hand was so much harder. Eight was the best
balance between ease of use and security. So he took his time, he paced out the shape, then he paced it out again. He 
even took the time to align the mat in a North/South direction when he laid it out, it was said that it was stronger
in that way and he would take any edge he could get tonight. Everything prepared, he took the knife, pricked a different
finger, just in case, placed the single drop at the centre of the mat and quickly staunched it, hid the cloth in the bag 
and wrapped hid finger in a bandage. Then he sat on the mat, spine straight, shoulders dropped, legs crossed, and began
the incantation. The call came swiftly and strongly, he could feel the spirits almost rushing toward him. He could feel 
them on his skin, more than he had ever felt before. Each heartbeat he was certain that the torrent must stop soon, he
could feel the weight and pressure inside his chest. But it kept happening. A normal cleansing was over in two or three 
heartbeats, but this was many tens of heartbeats in now. There wasn't a chance of him cleansing another block tonight, 
the pressure under his ribs was a pain now, a sharp, stabbing pain. His heartbeat began to falter. 

_This is it, Umeda. I'll see you soon_ he thought as his heart began to give out. This was always how he was going to 
die, he knew that now. Killed by his own hubris doing what he loved. Through all this, the flow of spirits never ended, 
each laboured beat of his heart was accompanied by a massive growth of the spirits inside him. As he started to fall 
unconscious he felt a solid object strike his skin, as the flood of spirits ended. 

There was a moment of stillness, a warmth on his arm, the object had hit him hard enough to draw blood. There wouldn't 
even be a body for his family to find. He only regretted that he had never got to meet his grandchild. Then the spirits
inside him vanished. Years later he would say that it had felt like they had collapsed in on themselves, leaving his 
chest as free and empty as it could ever be. There was a wave of weariness flowing over him as this happened and he fell
into unconsciousness, breath coming again along with the deep, slow heart beats of a man who despite his protestations
was still in his prime. 

Zamir came to what must have been mere moments later. The sun was still roughly where it had been when he started the
ritual, and at this time of year it set fast. He felt all over his body, and looked at his arms and legs as he stood up,
despite every instinct in his body telling him he should be dead, or at the very least grievously injured, he was 
completely unharmed. Even falling unconscious hadn't seemed to affect him adversely, he felt sharp, awake and clear. 
However he did not feel the pressure of the spirits within him. In fact he didn't feel the presence of them around him 
either. It was as though his ability to sense the spirits was gone. Looking around, however, told him a different tale.
The totems he'd placed so carefully were all knocked over, on the three sides of the field tha faced the octagon. 
Knocked inwards, lying on the ground with their points toward the Deadlands. The blocks that were in this corner of the 
field were also gone, depressions in the earth the only sign they were ever there. They must have been made of spirits
if such a thing were possible, which apparently it was, thought Zamir. These signs of a major spirit storm meant that 
anything at the centre of such a storm should have been utterly destroyed. Thoughts of this couldn't hold him for long 
though as it was the fields around him that caught his attention. 

The presence of spirits in the ground had certain tells, a darkening of the soil, the almost imperceptible haze over the
the surface, and of course the feeling deep in him that told him there were spirits nearby. None of these were present
in the surrounding fields, at least five or six in every direction. Somehow he'd cleansed an area the size of a medium  
farm in a few seconds. The soil was absolutely free of spirits around him. Normally a few were left, but one or two made
no real difference, but this was pristine, like they said the land was in the capital, around the Gods themselves.

The shock of this couldn't stop Zamir from being who he was. There was an unparalleled opportunity here to expand the 
land the village had for growing food in, and he was going to take it. He gathered up all his eight totems, realising 
somthing he should had noticed earlier. He was unharmed, but the reason he could examine his arms and legs was that 
his clothes had not been so lucky. He was as naked as the day he was born and the wind from the mountains chose that 
moment to make itself known again. Yes, completely naked.

Zamir rushed over to his mat for his outer layers and was relieved to find tha aside from a little wear on the farmward 
side of the mat, they were perfectly fine. There hadn't been enough spirits on this side of him to really affect the mat.
He unceremoniously dropped the totems to the clean, soft soil by the mat and began to slip on his clothes and boots. It 
was only a few moments later that he was buckling his totem belt back on before bending down and slipping his totems into 
their loops. 

Then he was off, running to the edges of the cleansed land, ready to plant the largest, but probably weakest octagon he
had ever made. A plan coalesced in his mind, this would need to be an octagon inside the cleansed area. There would be 
cleansed land lost back to the spirits, but the weakness of this barrier couldn't be ignored. The whole thing would 
likely serve as nothing more than a brief impediment to the tide of spirits that was sure to wash inwards, in fact was 
already moving in to the cleansed centre. Under normal circumstances, a field like the one he had set out to clear would
be safe for a few days at most, slightly less because of the ruin at one corner, but this was not normal circumstances, 
and the spirits were strong, this deep into the Deadlands.  He'd never seen the spirits be this strong, next to land that 
was so clean. He was pretty certain no one had seen this since the War of the Gods. So his plan was to lay out a large 
and rough octagon, then head to the Temple to get more totems and help. 

As he approached the edge of the pure zone, he could feel the presence of the spirits wash back over him. It wasn't 
a pleasant sensation or an unpleasant one for that matter, but the mere existence of it told him that he really hadn't 
lost his sense for them. He planted a totem at the farthest border of his current farm. The barrier would be stronger if
some of it was inviolable inside the strong barriers of his fences. Then he ran out to the next point, a good fifty 
paces inside the cleansed area, and planted the next totem. This would not be a work of mathematical precision and exact
measurements. Over these distances it was nearly impossible to be that precise, especially when running. The saving grace 
was that the cleansing appeared to have been roughly circular, so the points of the warding would follow that, more or 
less. The sun was below the line of the mountains as Zamir placed the last totem, deep inside his farm. It was harder 
here to measure the edge of the cleansed area, but close enough was the order of the day. He was nearly back at his 
house, but had to make the journey back to where he had left his temple made prayer mat. It wouldn't do for it to be 
damaged somehow if there was another spirit storm. 

In the fading twilight he couldn't see any damage to the mat from the previous storm, but honestly he couldn't see much
of anything. So without too much examination, he pulled the mat free of the ground and started back to the village, 
walking this time as he was almost out of breath as it was. As the sun dipped ever closer to the hidden horizon, the 
night air started to take on a bitter chill, reminding him why people around here normally wore several layers at this 
time of year. He didn't waste time cursing himself for not stopping to get more clothes from the farmhouse though. There
were much more pressing matters on his mind. What had happened to him today? How did he cleanse an entire ruin, and 
a whole farm's worth of ground, in a single ritual? Was he actually as hale as he seemed, or was the spirit sickness
just allowing him a night of normality before it killed him, as so many illnesses did? How was he going to explain this 
to the priests at the Temple?

What had happened to him?

Why wasn't he dead?

What.

Had.

Happened?

With these thoughts running through his head, the walk to the village passed in a haze, and waving off any attempts from 
people he knew to talk to him, Zamir headed straight to the temple. 

The priest was actually his sister-in-law, Sitara, and she was used to his coming in after dark on an autumn evening. 

"I thought you weren't coming tonight, brother! Come to the altar and Darox will draw the spirits from you and set them 
free." She waved a hand toward the low black stone that was the altar, brought here directly from the capital. Like a 
drowning man reaching for a life rope, Zamir placed his hand on the altar as he had so many times before. He was ready 
to tell his tale as soon as he had been cleansed. 

"Oh, that's odd, I thought you'd been clearing fields today?" muttered Sitara, "There's no spirits in you at all, nothing
to draw out."

Zamir looked at her in disbelief, then in a shaky voice he related his story of what had happened to him, from the 
morning until he arrived at the Temple. 

"We definitely need more totems to hold the area, we can't let the spirits retake it. This is a whole new farm for the
village.", Zamir kept repeating this as Sitara led him to the meeting hall attached to the shrine of the Temple. There
was always hot water there, ready to be made into a bolstering pot of tea. She spooned honey into one of the glazed, 
handleless cups and as soon as the pot was half brewed, poured it into his cup and handed it to him, pushing him gently 
towards one of the chairs with deep cushions on. 

"Brother, listen to me, you're in shock. Something clearly traumatic has happened. I will send out the other farmers to 
your farm with lanterns and a full set of totems each. They can be out there tonight. Please, stay here, I'll go 
and gather them now. "

Zamir took a sip of the hot, sweet tea and stared over the cup, through the steam. He must have slept sitting up, because
the next thing he knew was that Sitara was back,  and the tea in his hands was cold as the air. 

"Pavel's here, he's going to take you to his house to rest. You come back in the morning, and we'll see what happened to
you. I've got some news on that front actually, but it can wait until tomorrow. Go. Rest."

Suddenly strong hands on long arms were guiding him up, toward the house just over the street from the Temple where his
best friends in the world lived. He'd slept in their house more than once, before, often staying over at festivals and 
when the road back out to his house was impassable in the snow. The warm air, and the familiar scents of his friends' 
house soothed him, in mind as well as in body. Scant moments after he lay down on the trundle bed that Qodir had prepared
for him. Zamir was asleep. 

As he drifted off, Qodir looked down at his sleeping form and asked Pavel the same question Zamir had been asking himself
all evening.

"WHat happened to him?"