# A Festival fit for a god

THe morning dawned with an unseasonably early flurry of snow. Just enough to cover the land
and cover the many variations on red and brown sandstone that Zamir found personally quite 
beautiful. The snow held its own beauty though, and its own problems. This level of snow would 
most likely lead to the clouds burning off by mid-morning and turning the unpaved roads of the
village into a muddy morass by noon, when Dravox was set to arrive. 

Sitara was already going house to house and telling people to get their shovels out. The only 
solution was to clear the ground of snow before it could melt. Every able body in the the 
village was out and helping after a hearty breakfast that spoke well of the coming feast.

Soon enough with all hands helping, except some who had to be made to put down their shovels 
and instead rest, like Amira who had been trying to shovel snow in the cold with her
very pregnant self insisting she was fine. The combined distress of her husbands, father and 
aunt only serving to get her to move to bringing hot drinks to the shovellers. This was as far 
as she was willing to go however and the quartet of disapproval gave up after eking that level
of concession from her. Even the visiting wagoners were helping out, unfamiliar faces admist 
the crowds of villagers.

The village mostly cleared, the odd scattering of snow coming in late behind the main force
left a very thin layer of snow over the village, but not enough to shovel off.  The paved roads 
looked very festive as people began to place out bunting, stretch canopies across the narrow 
streets and place out braziers and long trestle tables. By the time the sun was at its zenith
there was a veritable feast weighing down the tables. As people stood around, one of the 
wagoners tapped Sitara on the shoulder "What are we waitng for?" She asked. 

Sitara turned, about to explain that they were waiting for the arrival of the God, but seeing
the face of the woman who had tapped her on the shoulder, she fell to the paving with a bow. 
"My God, I wasn't aware you'd arrived. Did no one greet you at the road?" 

"Oh I didn't use the roads, I flew here, there was a tail wind so it was a little faster than 
I was expecting. I expect that was [weather god] giving me a hand, he's thoughtful that way.
I got here and everyone was busy so I just helped. Been a long time since I did honest work!"
Dravox laughed at this and pointed at a large barrel of ale over by the wall. "I put that up, 
it's heavy, but when you've got thousands of years to work on your muscles, it's not so hard. 
Going to take more than a single human to lift it again though. So we better empty it eh?"

"Of course my God." Sitara looked around at the steadily growing circles of people falling to 
the ground in supplication as they realised that the God was in their midst. One young wagoner 
was looking almost sick as he fell to his knees and Sitara remembered that the youth had a 
habit of trying, and often succeeding, at talking any of the unattached young people in the 
village into bed. He couldn't have known of course and she didn't think that Dravox would hold 
it against him. That was until she saw Dravox catch his eye again and wink, with a slight 
gesture that promised more than a wink later. 

Dravox pulled Sitara up and leant in to her ear "Hey, a girl doesn't often get propositioned when 
there's eight thousand years between the partners, and he's a good looking young lad. We'll have
this feast, sort out Zamir and then I'm going to relax tonight. I've actually got [commerce god] 
to cover my regular duties, so I can even have a good time with no consequences. First holiday I've
had since that time we found the volcano." standing straight, she gestured in the air and spoke
so it sounded like it came from just behind the ear of everyone listening. "Yes, I'm Dravox. 
I'm honoured to have worked alongside you, the reason this country exists today. If it's not 
too bold of me, I'd like to dedicate this festival to you and yours. Without you, without the 
farmers, the tailors, the weavers, the dyers, the lumberjacks and woodworkers, the hunters...
there would be nothing and no one. You are the reason and the base of all that we care for. "
Thank you all! Now let's eat!"

With that said, Dravox offered a space at the head of the table that was closest to Sitara, then 
she gestured the young wagoner over and placed him on her left. "Zamir, where are you? Let's have
lunch then we can go to your farm and work out what you are!"

She sat down next to the young wagoner, whose name she didn't know but was determined to find out, 
and without ceremony dug into the food laid out on the tables. Zamir came over as soon as he heard
the shout and sat in the place she'd saved for him. Patting her young man on the leg, Dravox leant 
in and whispered something in his ear that had the poor young man blushing a brighter red than the
tomato sauce Dravox was dipping a slice of venison in. 

"Well met in the flesh!" met Zamir as he sat down. "Good to see you properly." Dravox took her hand
off a leg and put it on Zamir's shoulder, her eyes widening as her finger made contact with his 
skin. 

"I was going to take you out to the farm to make sure, but by the dead gods you've got the gift 
and strong!" Her grin was open and cheerful. "You don't know what it's like to not have any new 
peers in so long you build a civilisation waiting. We'll talk more later, but I am convinced. 
You've got the godgift." Her eyes went unfocused for a moment and then a cacophony issued from the
Temple. "And I just told my brothers and sisters! Here, get a bowl and come into the Temple with me.
Everyone, I am sorry but I need the use of the Temple for the next hour or so. Hope you don't mind."

Pausing only to fill four tankard with ale from the barrel she'd put out, Dravox led Zamir into 
the temple. 

Inside there were seven other figures, the Gods. Three other women, four men, all of them extremely 
exercised about something. 

"Are you sure he's got the gift? I thought we were never going to get another one." Said one 
man, who Zamir was fairly certain was [weather god], the patrician line to his nose, the lightly 
curly hair and the shirt which appeared to have clouds and wind running across it in constant 
motion were likely tells.

Next came an interjection from a woman with hair the colour of beaten copper, her eyes were green
of all things, like jade. Aside from Pavel's family, Zamir knew of precious few people who didn't
have black hair, brown eyes, and a healthy bronze to their skin. "How strong was it? Did you feel
his influence?"

"When I touched him, I nearly lost control over my finger. I had to actually try to maintain control!
And this was after only a day since his awakening. I'm taking him into the deep Deadlands later today."

"Is that truly wise? We don't know if his body can handle it yet. We should examine him properly." This was from an older
looking man dressed in what looked like a strange shirt which buttoned up the front and was even more garish than the 
shirts Pavel wore. It was a bright green with a strange yellow oval on it, with spiky leaves coming out of the top.
Noting his gaze, the man said "They're pineapples, and one day we'll grow them again and I will spend a year drinking 
pineapple juice. I'm [agriculture god] by the way. If you're joining the family, you'll get used to us all. Though I had
thought they taught the children about us still."

Blushing slightly under his bronzed skin, Zamir replied "Well I never really paid that much attention to lessons when I 
was a child. Too interested in being a hunter one day."

"Anyway, he survived what I'd classify as an A rank spirit storm the day before yesterday and today I watched him shovel
snow for an hour straight. He's definitely ready, he's the one who's been clearing more than any three other Frontier 
Farmers for the last 20 years. We hoped he was able to teach his methods to others, but I bet he's just been growing 
into his power more and more. I'll have to check the records, but I strongly suspect that we've been purging barely a 
third of his spirits each time he cleansed a field, and that storm pushed him over the edge. We might be looking at 
spontaneous core formation." Dravox lost her informal, nonchalant tone and suddenly appeared like a school teacher 
as she began to gesture in the air, causing lights to flicker over her brothers and sisters faces.

The conversation continued in a similar vein for several minutes, with each of the Gods introducing themselves to him
 and wishing him well on his journey into the Deadlands later that day. 