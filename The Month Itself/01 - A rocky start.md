# A Rocky Start

The valley Zamir lived in was perfect, or at the very least perfectly imperfect. The village was nestled partly up one
slope of the valley, the south facing side, the fields were there too, mostly. The dry stone walls separating the older
more established fields, and the imposing mass of the Temple stood above all of the low houses tucked into the walls
of the valley.

Zamir's house was not in that central cluster, his was the lot of the frontier farmer, converting the old fields of the
last age back into productive arable land. Out here, away from the village, there was nothing to shelter his house from
the chill wind that spoke to him of the coming of winter. As the sun rose, he sat on his porch, at least a storey above
the ground to prevent issues with flooding when it was first built, drank his tea and looked over his fields. Zamir had
the largest farm in the village, nearly half the youths of the village would descend upon his fields for harvest,
transforming the quiet and solitude he enjoyed most days into an echo of his own youth. He was very aware that at least
some of the teenagers who came to help him harvest had been conceived when their parents had been doing the very same
thing at the start of this farm.

This land was his now, all land belonged to the village as a whole of course, the council of the village saw to it
that everyone was fed, housed and had a job, but there was definitely a sense of ownership of this farm for Zamir. He 
stood up and stretched. While everyone else in the village would be preparing for the long winter months, when the fogs
around the deep places grew thickest, Zamir couldn't rest until the snow lay on the ground so deep that you needed a 
shovel to make way. This was the season when the spirits that haunted the battle sites of the war of the Gods were 
quietest, when frontier farmers like Zamir, and there were some others like him in every village around the country, 
could actually capture those old spirits and then take them to the Temple to be cleansed.

Zamir had spent more than twenty-five years clearing fields, ever since he was fourteen years old and had visited the 
Temple on his Ascension day and been told by the Goddess Dravox herself that his fate was to bring live to the dead 
lands that surrounded the villages of the world. 

It was dangerous work, most frontier farmers made a mistake, weren't able to contain the spirits, or just got unlucky 
and the weather caught them before they reached five years of clearing fields. Zamir had the knack for it though, He 
could almost see the spirits and how many there were. He had never placed the totems too far apart, trapping too many 
spirits inside their boundary. The Gods guided his hands as he pushed the eight totems he carried on his belt into 
the dead soil. This was a strong field, with many spirits, so he would be clearing it for months to come. 

_Well now, a whole fresh field to work_, thought Zamir to himself, _You would have been telling me to be careful not to
bite off more than I could chew Umeda._ The thought of his wife, dead nearly thirteen years, triggered him to remember 
to wash the flecks of dirt off his hands. There was something about the ground when the spirits were still in it, if 
it got into a wound it could cause an illness not even the gods could cure. That was what had happened to Umeda, all 
those years ago, Zamir had promised her as she lay dying that he would always be careful and that he'd see their 
children grown up into healthy adults. He'd managed to keep both parts of his promise, to the extent that his daughter
was expecting his first grandchild in the coming spring. Pulling the special prayer mat that the Temple had given him
from his back, he scuffed the soil mostly flat and laid it down. 

These mats were one of the expressions of the Gods' power in the world, a material smoother and stronger than anything a
human could make, they never stained, never wore out, and they allowed a frontier farmer to capture the spirits of the
Deadlands inside themselves.

Zamir straightened the edges of the mat, his calloused brown hand stark against the bright green and gold mazework 
pattern of the mat. Decades of compulsive hand washing would have left it dry and cracked, but long ago he'd learnt to 
rub a little raw wool over his hands after washing them, the oils sealing his hands and leaving the surprisingly smooth 
around the callouses. Even so, he could barely feel the faintly raised gold ridges over the green cloth with his 
fingertips. Giving the mat one last brush to remove any lingering dirt, he spread a second mat to the side of the first.
This was a plain, hand woven rug, and a new one at that. They rarely lasted more than a season when they were put on the
uncleansed ground of the Deadlands. The rough beige fibres did what they were meant to though and allowed him a place to
put the clothes he divested as he prepared for the cleansing ritual. First went his harness for the mat, then came his 
totem belt. The totems were again, gifts from the gods, but the belt he had made himself from the leather of a buck that
wandered into his very farmyard the first year he had been a farmer. Taking it as a gift from the gods, he'd used the 
whole of the animal. The meat had been a welcome addition to pots all over the village, supplementing their mostly 
vegetarian diets with something with a bit of texture. The bones had become knife handles, the antlers he'd carved into
statues of his children when they were born, to keep them safe. He wished he had carved one for Umeda as well, but it 
was said that the magic only worked from parent to child. The leather had become his belt, his harness, the grip on his 
knife, and the bag he kept his bandages in. 

Next went his shirt, his boots he took off and then knocked off against the fencepost to his last claimed field. 
Wouldn't be able to do that for most of this field. The walls around this field were made of the same stones as the 
houses of the village, worn smooth by the years and the spirits. Once cleansed, they would be taken away and replaced
by fences built by human hands, but blessed by the Gods to keep the spirits out. They did not have to be as powerful as 
the totems, but they could occasionally break and allow the spirits in. With the central fields, that was less of a 
problem, but out here on the edges, it could be life and death. 

Sitting on the mat, clad in only a light shirt and breeches, Zamir took his knife and pricked his finger, being careful 
to not left any blood touch the soil outside his mat. Blood was one sure way to drive the spirits into a frenzy, and 
part of the reason frontier farmers were so often short-lived. The one drop that oozed out of his finger was carefully 
placed at the centre of the prayer mat. Where his blood touched the mat, there was an even finer maze of gold and black, 
which thirstily drank his blood and started to spread power throughout the mat. With a speed born of much practice,
Zamir pulled a cloth out of his bag wiped his finger clean with it and then sealed it back in the bag. He then pulled a 
length of bandage out and quickly wrapped it tightly around his finger, the pressure staunching the blood before another 
drop could form. 

He carefully stepped onto the mat and dropped into a cross-legged position. Taking his pricked hand he touched the first
two fingers to his forehead, for the wisdom to know how to corral and contain the spirits, to his lips to give them 
the fluency needed to speak the dead words and communicate with the spirits. To the hollow under his ribcage, just below
his heart for the strength to continue when the pain of the spirits seemed too much to bear. That last had never been a 
problem for Zamir though. He had often heard other frontier farmers talking of the excruciating pain that bringing the 
spirits into them caused, but for him it almost felt like replacing something that was missing. Still he never let that
fool him. _The spirits will try anything to get into you and kill everyone you love._ He thought as he cast the notion
that maybe he didn't have to go to the temple tonight and give the spirits to the altar for cleansing, but could instead 
keep them for himself. 

That was his secret, the unspoken doubt that had lingered with him since he first took a spirit into himself and found 
that it felt right. That was why he was so successful as a frontier farmer, why he got up early every day in the season 
to spend every drop of daylight in the deadlands. Why Umeda was dead. 

So he began the chant and felt the power of the words amplified and changed by the mat under him spread out until it met
the invisible barrier of the octagon described by his totems. He felt the spirits leap to his will and come rushing at
him, like puppies seeing their master return. Second by second the flow grew, until his body was covered in a fine
coating of the dirt. This was where the danger truly lay, if the wound was not properly staunched, the blood would drive
the spirits into a frenzy and they would destroy the farmer, leaving nothing but a pristine mat and eight totems in the 
field. 

Then with a feeling that was like his soul being filled with the laughter of his wife again, the spirits entered him, 
right under his rib cage, where they would lie quiescent for the rest of the day. 

The ritual was over, the whole thing had taken perhaps a hundred heartbeats from start to finish and now this space, 
only a few paces across, was cleaned. This small patch of land could be used to grow food to help grow the village. It 
would be. But not today. The next thing to do was to pull up the totems and do the same thing on the next patch of land
The spirits would move back in, but slowly. Once started a field this size would need to be cleared and sealed in two 
to three days and sealed, with the octagons getting progressively bigger and bigger until the whole field was enclosed.

With that in mind, Zamir set to his work for the day, pricking his finger again when the flow of blood stopped, but 
otherwise barely pausing as he started to clear the field until the sun, hidden behind the grey clouds that had swept in 
from the lake in the distance at the end of the valley, was at its zenith.  

It was then that he very carefully walked back over to his discarded clothes and equipment and set up another ritual, 
a much smaller one, around these items and cleansed any lingering spirits that might've crept back into this ground 
since his cleansing more than five hours earlier. He hadn't broken his fast yet, and was starting to feel the hunger as
sharp pangs in his stomach, and in the hollow under his heart where the spirits slept. He always felt that they too were
hungry when they were inside him. It made sense, they devoured everything that wasn't warded or Temple made, but the
world was being cleaned of them, year by year. One day, there would be no need for frontier farmers.

Having cleansed his clothes, Zamir got dressed again and stepped back over the fence into his established fields. The 
totems he had left in the ground would keep that little part of the deadlands clean so he could prepare again after
breaking his fast. He'd managed to clear well over half of the field. He was always a little slower after lunch, but 
felt that he would have done the first pass by the end of the day. Zamir walked the few hundred paces to his house from 
the field. He'd started this farm spreading first to the village's farms, so he could get to and fro safely. Then down 
to the river for irrigation, the whole while only having two fields between his house and the Deadlands. Umeda was 
alawys nagging him to give them more of a buffer, time in case there was a spirit storm like the one that their 
grandparents talked of, where a stag had somehow managed to limp over the fences from the forest on the far side of the 
villages during a hunt and then climed into an uncleared field, dripping its blood as it went, sending the spirits into 
an absolute frenzy. The unusual thing was that they heard that the stag made it out of the fields alive, and in better
shape than it went in. The spirit storm, however had overwhelmed the wards, breaking through into a field of cows, and 
left nothing of them behind. That field was so dangerous, the Temple had spent a whole moon producing nothing but 
much stronger fences to surround it. 

Zamir had a feeling though that such an incursion into his space wouldn't be so bad, and the fences around the forest 
were much taller now as well. So he cleaned out, back toward the village. The fallow land here couldn't be described as 
the Deadlands, but the wind blew, and there was always some spirits coming into the village, even if no one noticed 
them. Only a properly warded fence could guarantee a clean field, and for much of the land inside the village, that 
wasn't done. The whole purpose of much of this fallow land was to give the town space to grow. The war of the Gods had
left very few people alive, but every year there were more and more of them, and one day the village would be a town 
like the ones in the heart of the country, where the Gods said that over ten thousand people lived inside one settlement.

So he cleaned a path to the village, which took him only a few days all told, building a road over to his farmyard was 
another story however, but with the cleaned path to the village, that was hardly an issue. Everyone pitched in, after 
all, cleaning the fields and having a warded house at the edge of the deadlands meant if another spirit storm rose up,
one of the really big ones from the stories, there was a chance they could get warning and shelter in the Temple. 

It was down this road that the village had built decades ago that Zamir saw a friend approaching. Pavel had been his 
friend since childhood, and had stood with him when he had gotten married. He'd stood with Pavel when he married too, 
because that's how things were. Pavel's family had come from the Western end of the country, where the Deadlands were
a dead sea, awash with spirits. His parents told stories of warded fishing boats and a frontier fisher on every one 
ready to cleanse the catch before people ate it. Technically Pavel had been to the capital, though he was only a year old 
at the time and remembered none of it. He still was called townie by his friends, who hadn't forgotten this, despite
the decades since. 

"Townie! What brings you out here? Whatever it is, it'll have to fit in a short visit, I'm half way through that new 
field." Zamir shouted down the road, receiving a wave from Pavel's shockingly pale hand on a tall and thin frame. Pavel's
family was not dark skinned and dark haired like everyone else in the village, but pale and almost washed out looking. 
He didn't have the shoulders and stocky build of a farmer either, his family were tailors, mostly. Pavel's clothes 
graced nearly all the village, and his attire today was a strong testament to his skills. The village was sited on the
edge of the habitable world, and a lot of strange relics were found when clearing fields. The Temple had to see each 
one, but often they were safe and not cursed or spirit drawing, and could be used. The village had one main export, 
aside from land safe for farming, which was dyes. It was the discovery of a dye making artefact that had caused the Gods
to send Pavel's family across the whole nation to set up shop. Half the fields around the village grew flax for linen to
feed the tailors and their clothes which were shipped across the country. Not everyone wore clothes like theirs, and 
they were definitely not the best clothes in the country. After all, nothing could compete with the Temple and the 
God fabric. But Pavel's clothes were tough, soft and well dyed and cut. His boots had been made by his husband, the 
cobbler, the trousers were a strong purple, held up with a sash of scarlet. The shirt, perfectly fitting his lithe form
was a pale yellow, and the short jacket he wore over it, a vivid green. Personally Zamir didn't think it looked very 
good, but he was definitely in the minority there. This fashion was very popular across the country it seemed, judging
by the amount of clothes the Temple had asked the tailors to produce for shipping these last few years. 

"Got news from the Temple for you!" called Pavel across the rapidly diminishing distance between them, "And lunch!".
He lifted his other hand then, showing a wrapped bundle of boxes, again it was wrapped in the gaudiest cloth that 
Zamir could imagine, still if it was lunch then it was probably cooked by Qodir, and the cobbler was possible even a 
better cook than Zamir himself. Zamir's lips twitched into a smile as he remembered the consternation on Umeda's face 
when she discovered that not only was her husband a better cook than even her own mother, but the tailor's husband was 
better than he himself. She'd consoled herself with the fact that she was the best shot in the village with the recurve 
bows that the temple issued to the very best hunters. 

Zamir waved again and stepped inside to make a pot of tea. The hills in the West were a lot lower, the climate milder
and the tea they grew was definitely the best in the world. The low fire in the grate was already keeping a pot of water 
hot, ready for his lunchtime tea, so shaving a few curls of tea from the brick and into the earthenware pot was the 
work of a moment, before he covered it with hot water and took it outside, setting it on the table of the porch with 
two glazed cups just as Pavel started up the steps. 

Without a word, Pavel sat the package on the table and began to unwrap it, taking out two tin boxes stacked atop each
other, and setting the bottom tin, which held some hot coals, down on the ground at the bottom of the steps. No one 
wanted a cold meal in this weather, but no one wanted to burn their house down either. The two tin boxes had clasps 
which held their lids on, and it was these that Zamir undid and placed one in front of each seat, along with a freshly 
filled cup of tea. This first wash would be weak and thirst quenching, leaving a stronger brew for after the meal. 
Inside the boxes was rice, potatoes, beans and thin slivers of what looked like venison in a thick sauce that caused 
Zamir to cough as he inhaled it. 

"Oh by the names of the eight Gods, it's not time for hot spices yet. It's not even started snowing." exclaimed Zamir, 
knowing full well that Qodir would disagree, and indeed had. 

"'The mountains are as white as they'll ever be, there'll be snow on the ground in days'" quoted Pavel in a surprisingly
good imitation of Qodir's deep voice. "You know how he is, he worries about you. You should have remarried, got someone
to watch your back out here. Gods alone know how you managed to raise two children and still grow the farm this much. 
You've done enough for the village, you know that. Qodir wants you to come and live in the village with us, pass this 
place on to the next generation. Which brings me to the Temple's news. They want you to take an apprentice." 

Zamir had not been idle during this monologue, despite his complaints, he actually loved the spiced food that Qodir 
made, the man had a genius touch for adding just the right amount of honey to take the edge off but keep the heat and 
flavour in a way that no one else in the village could match. So his mouth was full when he heard that he was expected
to take an apprentice. The ensuing coughing fit lasted a full minute and left him red-faced and gasping. 

"They want me to do what? Frontier farmers don't take apprentices, there's never been an apprentice or journeyman. You 
can either do it, or you can't. The Gods know who can. so why do they need me?", he eventually manged to get out. It 
wasn't that he hadn't taken apprentices on his farm before, but they had been his wife's, and he had just helped them 
learn the more mundane side of farming on the side. No one had taught him how to cleanse a field, beyond a few lessons 
in the Temple before they set him to cleaning a few fields inside the village.

"That's because no one else has lived as long as you as a frontier farmer and not died or quit. You have more experience
of this job than anyone else in history. The priests asked the God about it, and they confirmed it. Never, since the 
War, has anyone been a frontier farmer for more than twelve years. No one has ever cleared as much land as you. You do 
know you're a legend in the village? People from all over the frontier know about you. The Temple think that you cannot
be wasted, and you should pass your knowledge on. If you've started a field, you'll be in town tonight, right? So when
you go to the Temple, they'll talk to you more about it. I only know it because I overheard the priests talking when 
they were getting some new boots, so act surprised when they ask you."

"I will be surprised. I don't know what they expect me to teach anyone that they don't already. You learn the ritual,
you sit on the mat, everything works. Be careful, don't try to cleanse too much at once. spend time near the Deadlands
to feel how many spirits are in a field. There, I just taught you everything I know about my job. Congratulations 
apprentice Pavel, you are now Journeyman Pavel."

"You laugh, but I know there's whole layers of knowledge you have that you don't even know that you know. When someone 
starts asking you proper questions, from a place of some experience, you'll find gems being unearthed from the dense 
rock that is your brain." Pavel paused to refill both of their cups from the pot, noting with satisfaction that the 
steam had the astringent edge that spoke of a refreshing brew. "Anyway I need to get back to the shop, and you need to 
go and expand the world for normal humans more than any two people together never have in the whole history of the world.
You know, apparently you're only just short of having cleared more than the three people who were the top three until 
you came along."

"Fine," replied Zamir, his friend's chatter as he cleared the table putting him in a somewhat better frame of mind, "
But there better be some good food ready when I get out of the Temple this evening. You know how hungry that leaves me."

As they sipped the last of the tea, their conversation turned to other matters, like Amira's pregnancy, Zamir's daughter
was a hunter like her mother and was determined to keep hunting the forests as long as she could, just as her mother had
when she was pregnant with Amira. With Johan, though, Umeda had actually stopped a good month earlier, it was just more 
tiring having a child already to look after, she'd said. The fact Qodir had been coming out to the farm every few days 
to cook treats for Umeda hadn't hurt. He was even now promising to do the same for Amira. 

Soon enough though,the gossip finished, the cups were empty and Pavel was striding back to the village, a walk that 
would take him half the time of anyone else because of his long legs and his seemingly inexhaustible energy.  Zamir 
put the teapot aside, those leaves would be good for another pot at least this evening or tomorrow morning, rinsed out 
the cups with fresh well water that he cleansed as he drew it, a minor working that didn't require more than a thought 
these days. Most frontier farmers could do this without a mat eventually, but none could do it as quickly or as well 
as he could. The cups put away, he wiped down the table, catching any crumbs for the chickens, and set the chairs to  
right. 

Soon it was as though no one had been home since the morning, excepting the pot of cold tea leaves in the kitchen. With 
one last pass through the house with a brush, he pulled his boots back on and set off back to the field with a full 
stomach, and a troubled mind. 