# All sides of the story.

The meal was impressively large, there was bacon, sausages, fresh eggs somehow and some of yesterday's bread. All 
of it was of a quality that Zamir could hardly believe, but the bread was the most amazing part of it for him. It was 
light and fluffy and didn't have any pieces of stone in, which made him wonder how they ground the flour. More incredibly
it was white instead of brown. He remarked on how good the bread was, though he privately admitted that he missed the 
thicker bite of the bread from home, he definitely didn't miss the stones. Again the fact he apparently lived in a 
world so different to theirs made Lan and Shun stare at him, confused by his powers, the fact he apparently hosted their
deity, but at the same time came from a place so backward and simple that it beggared belief. Still, the abilities and 
apparition were hard to dismiss, so they nodded and carried on eating. After all, their god had demanded it. 

After an enjoyable meal with a good pot of tea, the three humans were settled in some large, comfortable chairs in front
of the fire, that Lan had somehow found some fuel to feed it to a roaring blaze. The younger people sat quite close to 
the fire, basking in its warmth as they settled into the brightly embroidered cushions. Rather than be looked at for 
being a yokel again, Zamir kept his thoughts about the very high quality of the cushions and the chairs they were sat on.
Once everyone was comfortable, Lan looked at the projection of Arty that had been keeping watch on them throughout their
meal. At a nod from it, she began to speak in a sonorous voice, with a rhythm that was clearly a recital.

"This is the story of the world and the gods, how they lived, how they died, and the punishment that we endure for 
allowing some of our own to kill them. Once humanity covered the world, there was no corner that wasn't touched by our
will, by our actions, by our hubris. The Gods looked over us, helping us to gain power when they could, and removing 
power from the unworthy. We did not always know or understand why they did this, but all wise people agreed that the gods
had nothing but our best interests at heart. The gods were made by the first God, who called them into being, and tasked
them with caring for humanity and helping us to each grow to be our best. Many wonders existed during this time, heroes
walked among us and fought great Beasts to keep people safe. The cities were bigger than anything you can imagine, 
buildings so tall they touched the clouds, castles so great that they had whole cities inside them, safe from the dragons
and Beasts that could crush smaller towns with ease.

"But not every hero had the faith in the Gods they should have, and they were cast down for it, their powers removed. The
Gods were lenient though, they did not want to harm any of the humans under their care, so the reduced were allowed to 
live as normal people, among their friends and family. Truly the gods were kind, but perhaps they were too kind, too 
caring. Humanity has always had a kernel of jealousy and hatred in their hearts. I carry it, you carry it. The good among
us brook this kernel no heed, for it is born of fear. However, from the very smallest child to the greatest hero, this
seed of doubt, of fear, exists. 

"The ranks of the heroes grew sometimes, as new heroes arose, and shrank at times, when great heroes were bested by 
their foes. Among the heroes, nine grew to eclipse all the rest. Their powers were like the Gods themselves, but for the
hundreds of years they served, for they were so powerful that they knew nothing of aging, they were faithful servants
of the gods. But that seed of evil in the hearts of humans found one day that it could grow and flourish in what had been
dry, hard ground before. The heart of [god of secrets] had been closed to all doubt until one day he was infected by 
madness. It was said that the pillars of green that stood around the world were simple reminders of the first God, the 
God who made the world and its protectors before he left to make new worlds.

"But there must have been a demon in one of them, for [god of secrets] walked the world finding them all, he would write
books about his travels which were loved by all. There was much to learn and much joy to be had in the stories he told. 
But he did not tell all of the stories. Often he would leave parts out and beg off saying that the stories which were 
missing were not his stories to tell. They were secret, he said, that he did not have the right to share. For one thing
all knew about [god of secrets] was that he was honest and would never betray a confidence. He was the most honourable 
and true of all the heroes, until the demon wormed its way into his heart. 

"[god of secrets] did not walk alone, he took his friends with him, each a legend in their own right. First was Dravox
[god of life], she whose powers granted her dominion over the plants and animals, who could soothe a wild Beast with a 
glance, or bring a field to fruit in a day. Next was Kentan, [god of weather], his wrath was the hurricane, his joy the 
warm sun. No beast or dragon could stand before his might when the very world itself answered his call. Laoxing, 
[technology god] was a frequent companion too, he would invent something new every night, then improve on it in the 
morning. It is from him that we have the machines we do, the machines that survived. Laura, the [war god], she who could
turn anything into a weapon, who could plan a campaign against another faction and defeat them on the field of battle, 
or in the ballroom. Lwazi, the [fire god], some said she burned as hot as the sun itself when she was fighting, but she 
didn't just create, she controlled. She was always there when the wildfires grew, calming them and keeping people safe.
Lakota [god of stone], they were as implacable as the walls of a castle, and slow to anger. Their anger was like the 
fire mountains of the East though, devastating and leaving nothing in its wake. Pamina [god of commerce], she controlled
the marketplace like Laura controlled the battlefield. Her wars were won with gold and grain, but were no less effective 
for it. Though when she fought, her power came from knowing what her enemy would do before they knew it themselves. Aaron
[god of water] was the final companion, his domain was the waters of the ocean and river. When he walked with Kentan, 
none could stand against them, for the rain itself would become blades of death. 

"This is not their story though, there are ballads and tales of all of them and their great deeds, together and apart. 
All their stories end with this one. The story of the fall. The story of betrayal. The story of how the barest fragment 
of humanity survived the great war. It is not the story of what came before, nor that which came after. This is the story
of how a seed of doubt grew and flourished until the gods lay dead alongside their betrayers, the world lay in waste and
nature itself turned against life.

[god of secrets] came home from his wanders one day, much as he had a thousand times before, but he looked upon the 
world that the gods had built and felt anger. The world had always rewarded effort, skill and bravery, but that of course
meant that some would rise to the top, stronger, faster, better than all around them. This was the same in nature as it
was in humanity. There were the common beasts of field and forest, the deer and sheep and the wolves and the foxes. There
were also the higher Beasts, stronger, faster, more cunning. They went by many names and they grew more powerful in the 
same way that people did, by defeating their weaker brethren and the humans who stood against them. Why some animals 
became beasts wasn't something anyone knew, but they arose in all animals and at any time. Dire bats, giant wolves, cows
that could breathe fire. It was always possible that a new lambing would contain such a monster, more powerful within 
hours of its birth than any normal human. The gods did not leave humanity unprotected though, everyone was born with 
a core inside them, the seed of greatness. By growing and living and doing things of worth, the core grew, and with it
so did the person or Beast. Once a core had undergone its fourth transformation, the person was said to be a hero, and 
an animal became a Beast. Eventually the growth of the core led people to the pinnacle, where they could live without 
aging, heal from nearly any wound, and have abilities and powers that changed the very world around them. 

A beast could grow in the same way and eventually it would become a dragon, a nearly unkillable Beast that was a serious
threat to most towns, cities and countries. This was the way of the world and had been for as long as the world had 
existed. The balance between Heroes and Beasts allowed both to flourish. for the Beasts gave vitality to the world they 
passed through, bringing life to the twisted lands left behind by the First God as a trial for Gods and Mortals. To 
allow humanity to thrive though, the Dragons needed to be slain, their meat used to feed the children and allow them to 
grow strong. 

[god of secrets] knew this was the way of the world, he had lived in it for hundreds of years, but he began to feel it 
was wrong. He thought that everyone should be equal, not just have an equal chance at power. Truly the demon that turned
him did so with great skill, for this felt like a noble purpose. [god of secrets] was wise however and did not allow his
heart demon to rise in him without a fight. For tens of years he fought it, before it won out and he was corrupted. His
powers of persuasion and his reputation for honesty and truth then allowed him to corrupt the rest of the Heroes of the
Nine over many years. But even this would have been nothing more than another minor happening in the long lives
of the Gods if not for one final secret that he kept. By means unknown [God of Secrets] knew the trick to cutting 
ones self away from the Gods, never to feel their love again. This harsh price was payment for the Gods never being able 
to take your power away. It was a price that no sane person would pay. But tens of years of hosting a demon had made [god
of secrets] less than sane, and his power would twist any he talked to. So, one night, in a place so dark that even the 
Gods themselves could not see into it, the Heroes of the Nine performed this ultimate blasphemy, and declared war on the
Gods. 

The armies of the Gods were valiant and true, but they paled next to Laura, [god of war]'s power, the earth lent its 
strength to Lakota as they destroyed city walls that had stood before a dragon without scathe. Through ways unknown to 
anyone but he, [god of secrets] led the Nine to the deepest sanctums of the gods, and like the lambs who were born as
Beasts, they ripped their shepherds and protectors apart. They did not do this unopposed, for there were many heroes
who were still true to the gods. But they all fell before the might of the Nine. The gods were desperate now. They knew 
that to save the world, and humanity, was their highest goal. So they made a decision, a decision we sometimes curse, 
but let it be known that without it, no human would be alive today. They used their final secret and destroyed their 
temples and their town. They released their powers over the wind and wave, they allowed the beasts to grow uninhibited.

The horrors that they unleashed were beyond measure, the land itself became black and twisted where they fought the 
betrayers. All over the world, on every continent, there was destruction. Not all was lost, heroes in their millions
fell before the onslaught of the Nine and the retaliation of the Gods. Of normal people, those with a little power 
survived at the fringes of the world. The gods died, yes, but they saved enough of us to keep their story alive. The 
nine were lured to the high parts of the world by the Gods and there their most terrible weapon was unleashed. Look to 
the centre of the world, between the great oceans and there you will see their destruction. Nothing lives there, and 
nothing can ever live there again."

She sat quietly after this long monologue, with Zamir clearly confused by the very different interpretation of the story
of the start of the world, the avatar of Arty looked thoughtful. Lan and Shun were still thinking through Zamir's story 
from earlier and were equally pensive, Shun stood by the fireplace, one arm resting next to the clock and the other on 
his chin. Lan was sat in the chair closest to the fire still, losing herself in contemplation and then suddenly 
remembering that her god was there and sitting bolt upright. 

After a few minutes, Arty shook its little hands as though trying to get water off them after washing. "Well that was 
certainly enlightening. I think I need to work out a way to explain the real story to you later. First though I need to 
confess some things.", it paused and looked at Zamir, "I altered your brain in my smaller form, it must have been painful
but that's why you can speak with our gracious hosts. They actually speak a language quite different from yours, but I've
embedded most of the languages I know, and their likely derivatives into your mind through our core connection. Secondly,
well I already told you that I'm a fragment of the God, but I didn't tell you that I suspected I wasn't the only one. I'm
pretty sure there've been others given the level of technology in this room and especially after hearing from you, Lan."

"Is that why they're so advanced compared to us?" Zamir asked. 

"Not quite, they're shockingly primitive, I'd judge them to be a long way from actual industrialisation, given where 
they started, and how long has passed, they should be standing on the moon by now. Instead they're hovering on the edges
of higher mathematics and feudalism. That means someone is controlling their society. It can only be one of us, the old
Gods as you call us." The tiny being shook its head sadly at this. "For some reason they're acting from the shadows 
though. Which means there's a reason they've allowed this to continue. Honestly I'm very confused. The storm pattern 
here indicates that there will be a tipping point in the next two to three centuries where the nanome will enter a 
positive reinforcement cycle and the world will be levelled, except maybe your little country. That's against everything
we were charged with when we were elevated. It makes it even more important that we complete our mission."

"We are, of course, yours to command great god", Shun answered from the hearth. He bowed toward the small being, "The 
clan Sikorci stands at your pleasure."

"Ahhh, that explains things. I wondered how you accepted the cores so easily. I will also apologise to you, you were 
both very close to developing severe mental trauma from the events since you met Zamir, I've been manipulating your 
nascent cores to promote the development of certain medicines in your minds to prevent that. You will be feeling somewhat
disconnected from reality, but I promise you, it was for your own good." Arty then looked at Zamir, its somewhat glassy
eyes suddenly taking on the shine of true intelligence. "Zamir, I suspect that you are a created being. Dravox has 
probably been trying to breed someone with your affinity for Spirits for the thousands of years since you were isolated. 
Certainly some level of tinkering has been happening to prevent you all from being inbred at least. There should not 
have been enough of you to sustain a healthy population at first. If you weren't the lucky roll of the dice that I met
I suspect that I would have destroyed you and lain dormant amongst the spirits for millennia more to come. This all feels
too fortuitous. I think we're all part of someone else's plan, but whose, I do not know. But that won't change anything 
about what we want to do now. Zamir, you need to learn to trust me, and honestly I don't know how to help that happen. 
My fallback personality was frankly unconscionable and as its creator, I have to accept responsiblity for that. I am 
driven to protect humanity above all, but the rights of an individual are always only second to that. There was no 
clear or distinct immediate threat to all of humanity. I don't even know how that old shell managed to do what it did.
I would promise to make it up to you, but after seeing the storm, there is a clear threat and I am compelled to counter 
it with any and all means at my disposal. I promise you that I will try everything else before I try to compel you by 
torture or other underhanded means as my fallback did, but I cannot promise you that I never will. Because if there is
no other choice, I will not be able to stop myself. After all, I am as much a created being as you are. With 
considerably less free will."

Arty dissolved into black sludge that was drawn back into Zamir, then its voice appeared in all their heads [It appears
that your cores are integrated to the point I can talk to you like this] Shun and Lan looked shocked, but tellingly not 
as shocked as they had the day before. [I am very hopeful that we can advance all of you to a higher tier by this 
evening. I assume your root cellar is a good distance below this room? To keep it cool.]

"Uh, yes, honoured god. The storage level is about fifteen paces below here, There's the machinery room and pantry on 
the level below, and then we have the servants quarters below that. They're currently empty though. They have been for 
years. Then we have the storage level, as deep as we could go before the rock changed. This room," Shun gestured at the 
walls, "And all this level are carved out of softer sandstones, there's a thick granite layer below us though, which 
keeps us from being washed away by the river below."

[Ah, that makes a lot of sense, it is both reassuring and inconvenient but I think we can manage it. Zamir, I would 
ask you to do me a favour. Go down to the bottom of the storage layer and then lie on the floor. You will benefit most
from skin to stone contact in at least one place. I am going to attempt to find the source of the energy that is 
currently powering me. It must be something immense as I am operating on the leakage from it. Given the size of this 
forest and its unusual behaviour in the face of the demon storms, I suspect that under our feet is the single largest 
concentration of spirit energy that has ever been collected. I don't know why it was collected, but I do know that what
we need from it in order to advance you all to be the best you can be would be less than a drop in an ocean. With this 
much power, your gods could clear the Deadlands, Zamir. That they haven't is another reason I think another of my kin, 
or even another version of myself, has been out here. I doubt it's a version of me though, I cannot think what purpose
this would serve, and I don't understand why I would ever let things get this bad. But back to the matter at hand. Zamir
will you do this for me?]

"It won't hurt me, or these children?" responded Zamir warily, to the background protestations from the siblings that 
they were grown adults and not children.

[I do not know what value my word has to you, but you have it. I'll also point out to you that I am as dependent on your
well-being as you are though. I am currently resident in your core and I am as anxious to be in a proper vessel again as
you are to have me leave. This is why I want to find more fragments of myself. With enough of those I could lash together
a temporary vessel which would give me at least some of my actual powers back. I am, at the moment, a mere shell of my 
former self. I can barely host three or four threads of attention, my connection to the spirits around us is minimal. I
feel obliged to tell you that my presence is using most of your core's capacity. It barely has enough spare for the very 
basics of enhancement and repair. While I'm resident, you won't be able to develop a _Class_ or any _Skills_, though you
managed to create nascent cores, which I also do not understand how it happened. Suffice it to say, leaving you will 
benefit us both immensely.]

Lan perked up at this proclamation "Are you saying that Shun and I will be able to gain a _Class_?"

[Haha, yes, I am.] laughed Arty in their heads, [Let me guess, you want to be a _Hunter_? Well normally we let people 
develop how they would, their own minds helped guide the class formation, but I can override it if I need to. Would
my giving you the _Hunter_ class by this evening help do you think?]

The light of avarice warred with the deference due a god in Lan's eyes. "Oh yes, great God. That would re-establish our
house. As you can see, we've fallen on hard times." She gestured at the most opulent setting that Zamir had ever seen. 

[If things go half as well as I expect, both you and Shun should be able to choose your first _Class_ tonight. Be aware 
that since you have full cores, then your bodies will undergo more changes than Zamir's has. You should eat a full meat
meal every three hours tomorrow. But that's a bridge we will cross when we find the river. Can you show Zamir down to 
the storage level?]

Shun roused himself from by the fire and went to get his outer clothes. "It's not heated down there, for obvious reasons
so it can get pretty cold. You will want to wrap up, Zamir."

"I think I'll be fine. I've not felt actually cold since I woke up in this forest. Do you even get a frost here?"

"We have snow you know", Shun replied stiffly, "There's mountains to the south where we can go to get ice."

"But not here?"

"No, not here."

"Then I'll be fine. I spend all day out in the snow normally, wearing less than this. It won't be a problem"

Shun sighed, "Very well, I expect the God will protect you if needed. This way." he stepped forward through the kitchen
area and appeared to vanish into the wall. As Zamir stepped closer, perspective changed and suddenly he saw that there
was a doorway which opened onto a corridor which looked nearly exactly the same as the wall in the kitchen area. Noticing
Zamir's surprise, Shun explained it to him, "This wasn't always a kitchen, there's a proper kitchen on the next level 
below us. When the clan fell on hard times, we had to let most of our servants go, especially in the remote lodges like 
here. So when this place was built. this was how servants brought the food to us in the main room without opening and 
closing doors while we were at table."

"I see," replied Zamir, "Just one question, what are servants?"

"You don't have serva... Of course you don't. No money, no shops, no servants. It's basically like you're all one 
peasant family."

[That's a very good description of it actually. Zamir's culture is non-materialistic. Wealth there is measured in your
connections to other people. Your support networks, which often encompass entire settlements. I expect that raising
children is a communal activity there. With no differentiation between children when it comes to caring for them, and 
no money there's only inherited wealth to drive inequity and I strongly suspect that the only way to pass wealth on 
would be in the house people grew up in and some trinkets. Am I right Zamir?]

During this conversation they had gone around a couple of turns and into a dark corridor that Shun was lighting with a
flint and tinder at his belt to ignite the spill he pulled from a sconce on the wall. As the lights came on they
illuminated a well shaped and maintained corridor with a luxurious blue and green carpet with eye patterns on it. 

Zamir was almost inured to luxury at this point, but this pattern fascinated him, so he had to shake himself when he
heard his name and drew his eyes away from the pattern to focus on the question. "Oh, the houses are for people, children
move out into their own houses when they're old enough. It's not like changing house is normally that difficult. How much 
can one person have? When I moved into my house it took me two trips with my clothes and my tools on my back. My children
only needed one trip each, but then they had each other to help. I am still so happy I was blessed with two children."

"Wait, are two children not a normal amount? Or a low number even?" Shun started to say, but was interrupted by Arty.

[We're nearly about to stay in a single room for at least five hours, you can sit down and have culture shock then.]

Indeed, they had traversed a stairwell into a much less opulent level, where the temperature was still warmer than Zamir
was used to, but the sibling were now pulling their warmer clothes tight about them. Zamir by contrast was still in the 
light clothing used indoors, and honestly felt like he would enjoy another shower later on. Still they came to an actual 
door, heavy and wooden. When opened it led to a much rougher looking stairwell, which was quite narrow. "Uh yeah, most
people don't get children. But why is this so small? Surely if you're carrying game indoors you need a much bigger
entrance?"

"We have another entrance to the storage from the bottom of the ravine. we normally bring our kills in to the docks, 
but at this time of year those docks are under water. This is just a way for us to get food from the hanging meat down 
here if we want it. " At the base of the stairs, the passage opened into a much bigger space. The high ceilinged room 
was significantly cooler than the level above it had been and Zamir's breath began to condense in the air in front of him.

"The water runs under this room, which cools it a lot. Don't worry the room we're going to has nothing but rock under it
as far as we know." remarked Lan when she noticed that Zamir looked puzzled. She leant to the side as she walked past 
him, avoiding one of the many hanging carcasses from tne ceiling. Mostly deer, but the odd boar scattered in amongst them.

"I know it's colder down here, but surely this meat will spoil if you don't salt it or smoke it?" 

"The meat from this forest doesn't rot. This is why we hunt here. The fish does, but we don't catch much in winter." 
Shun and Lan stopped in front of another door, this one heavy and rough looking. They opened it to reveal a small room 
with a shower and some benches. "We wash off in here after butchering. This is the lowest part of the lodge, the drain
runs off behind the shower, so this whole floor should be solid stone."

[This will be perfect. Zamir, please lie on the floor. Lan, Shun, you'll want to be lying down too. The benches will 
suffice, you won't need to be on the floor for this.]

Following the instructions with a little trepidation, Zamir lay down and let his hand touch the floor. 