# Who knows what happeened? 

Zamir woke up and stretched, his hands hitting a wall where there shouldn't be one. The smell of cooking reached his 
nose too, in fact he was fair sure it was this that had awakened him. Oh there was definitely the maize porridge with, 
oh yes sausages. Umeda didn't cook often. but when she did, it was this hunter's breafast. Dried foods and water with
a little salt and... Wait, Umeda wouldn't put hotspice in her breakfast. Suddenly coming to full consciousness, Zamir
remembered where he was. He cracked an eye open and looked around. He was still fully dressed, but there was a basin of 
hot water and a fresh set of his clothes on a table next to the trundle bed. 

"I know you#'re awake, get washed and changed quickly," shouted Qodir from the kitchen, "You've got to get to the temple
by noon and you've slept most of the morning already. How are you feeling?"

Zamir checked himself over. Still he felt fine, there was no sign of his explosive encounter yesterday. He quickly stood
and shed the surprisingly clean clothes he wore from the night before. The water was still hot and there was a bar of
soap and a cloth. He availed himself of these while the smells from the kitchen continued to tantalise him. He realised
that if it was really nearly noon, he hadn't eaten in half a day. Half a day, where he had done the largest cleansing 
that anyone had ever done, nearly died, ran around a farm, ran from his farm to the village, didn't manage to get purged
of the spirits he'd taken into himself and then passed out on the trundle bed in his friend's house. He looked again at
his arm, where he was certain the mysterious object had hit him and drawn blood. The blood was still there, a spot or
two of a brown stain that wiped clean off with the hot soapy water, but the skin was unmarked. The scent of food once
again awakened him to his body's needs and he quickly finished getting washed, before grabbing the towel, drying off and 
getting dressed. He stepped into the kitchen and then through it as Qodir said, "About time you got up, go to the toilet
and your breakfast will be waiting for you when you get back."

Zamir complied, quickly stepping through the frosty air as he made his way to the outhouse. Unlike most people in the
village, Pavel's family could never get used to the chamber pots everyone else used, so the path to their outhouse
was covered and partially fenced in. Even in the deepest winter, you could get a reasonably clear path to the toilet. He 
moved along it with an urgency that was making itself known. With that matter dealt with, his passage back to the kitchen
was as swift. The urgency this time deriving not only from a pressing biological need, one that was making itself known
with a rumble from his stomach that echoed in the small walled in garden, but also a metaphorical hunger. What had 
Sitara meant when she said she had news? What had the farmers found at his house? They'd clearly been there and back and 
brought back his clothes. 

"Has the Priest told you what happened? Did Pavel know?" Were the first words out of his mouth as he sat down at the 
table in the cozy kitchen. The beams were studded with hooks and the peppers and roots that were ground together to make
hotspice were hung from every hook, coming down low enough that Kamir knew that watching Pavel navigate the depths of 
this maze of flavours was an amusing sight indeed. Qodir pushed a filled bowl of porridge and sausages and, wonder of 
wonders, some of his festival sauce. Rich and thick, made with dried lentils boiled with , hotspice and tomatoes 
and precious sugar and salt to preserve it, this was a welcome topping to any meal for Zamir.

"Eat up, don't talk and listen to me." 

Qodir sat down and began to pour the tea.

"I don't know how you did it, but yes, that whole area was cleansed. It's the biggest expansion of growing area that 
anyone, and I mean that to include the Gods too, has ever heard of. There's uproar at the temple, they're making fences
as fast as they can, but the current idea is to dismantle some of your interior fences, replace those with totems for 
the moment, and use those interior fences to secure as much of this as we can. You've cleansed enough ground that we'll
be getting more settlers next year for certain, probably as many as three families. Everyone is very, very excited about
this, even if you can't ever do it again, this puts us years at the least, maybe decades ahead on the expansion of the #
village. If you can do it again, we're expecting that just two more times would be needed for us to have the land to get
a proper named Temple and a real road back to the West.

But the other side is, no one knows how you did it. Around you, the spirits are just... gone. Pavel worried about it after
he brought you back last night, carried you back really.."


Through a mouth full of some of the best food he'd ever eaten, Zamir interjected "Thank you, and him, for that. You're 
damn good people. I don't know what I'd have done without you both."

"Zamir, you're family. Which is why I sent Pavel yesterday, you should be here, with us, enjoying the shelter and company
of the village. You can still go out to some remote hut when you need solitude, but... But I guess that can't happen now.
You're too mysterious now, hurry up and finish by the way, Pavel will be back to take you to the Temple in a moment. 
Where was I? Oh yes, after carrying you back, Pavel went back to the Temple to be cleansed, just in case, please don't
take offence."

Scraping the last of the porridge out of the bowl, Zamir replied "None taken, I don't understand what happened either.
Very sensible. You know how I am about spirits since Umeda."

"Exactly. Well anyway he went back to the Temple, but just like you, he was completely cleaned of spirits. Absolutely 
without any spirits at all. It's unheard of. If you can just walk through the Deadlands and they're cleansed, I'll make 
you a new pair of boots every day to do it in."


At that moment the front door opened and the sound of a pair of those self same boots being taken off and knocked out 
against the boot scraper in the porch came through. 

"Is he ready yet?", called Pavel from the door, "I know he's up, he's never managed to sleep through the smell of you 
cooking!"

"I'm ready," replied Zamir, standing up, "Just let me come and get some boots on."

He stepped around the table, the spices clearing his head with a finger's breadth to spare, Qodir used the spices like a
fence to keep those who couldn't cook out of his kitchen. Zamir wasn't quite as good as Qodir in the kitchen, but he 
wasn't far off. He embraced Qodir, who looked somewhat flustered at this treatment, as he always did. 

"Thank you." The two words were simple, but they carried a sincerity that meant Qodir knew they encompassed more than 
the admittedly rather good breakfast.

Zamir stepped away and in moments was at the door with Pavel. 

"Why did you take your boots off?" he asked the man who was as close as a brother to him, curiously.

"Oh Qodir would skin me alive if I wore my everyday boots today, you've got a visitation from one of the Gods today.
Can't let a God see me in my ordinary, exceptionally good, comfortable boots can I? No I have to wear my special boots 
for festivals. So do you." Pavel gestured at the boot rack where the shiny and master crafted festival boots from his 
house stood. Having a master cobbler as a soi-disant brother-in-law was not without its benefits after all. 

It was then that Zamir realised that his clothes were probably the best he had, his mind was definitely still catching 
up with everything that had happened to him in the last day. As he sat down to pull those boots on, he felt a tugging at 
his hair, as Qodir began to run it through with a brush, roughly working out any tangles. 

"You two will not embarrass me in front of a God by being anything but perfectly dressed." he said, as his strong, 
calloused fingers pulled the few knots in Zamir's fine black hair loose.

"Don't worry, my heart," replied Pavel, leaning over to plant a kiss on Qodir's cheek, "All eyes will be on the great 
hero's new abilities."

A few seconds later, and satisfied that the two men in his life, his husband and his partner in cookery crime, were as 
well turned out as they could be. 

"I'll tell you all about it," said Pavel as they got to the door. "You know, you could come with us, there's no list of
people allowed in. It's our Temple after all."

"No, it's the Gods temple, and they're going to want to hear from people who've seen the scene, who can help explain 
what happened. All I can tell them is that he snores and can eat enough to beggar a poorer village. Everyone knows that.
No, you two go now, and I'll work on that new last."

Conversation complete, Zamir and Pavel stepped out into the bright and crisp day, the sun was really just about to hit 
its peak, but luckily the Temple was but a few steps over the main paved street in the village. Their festival boots 
clicking as the barely worn hobnails in their soles struck the cobbles. The temple was an imposing building, made of 
black stone brought from the centre of the country instead of the rough orange sandstone that the local mountains, and 
all the houses, were made of. The doors were local though, made from the wood of the tall pine trees that dominated its 
centre. The pale yellow, almost white doors, stood in stark contrast to the surrounding stones, but somehow the stark 
aesthetic was welcoming instead of imposing. The Temple was a place of safety, of community and of home. The only thing 
the temple asked of most people was that they come to get purged of spirits regularly so they didn't get ill, or pass
sickness on to others. 

They opened the door and stepped into the shrine. In the room were all three of the other Frontier Farmers, their haggard 
eyes and weary stances provoking a stab of guilt in Zamir. While he had been sleeping, they had been doing the work to 
secure the new farmland on his patch. Noticing his expression, Antonov smiled at him, the other frontier farmer had 
once suffered from his own soul storm, and Zamir had been the one to pull him out. The realisation that he would have 
done the same for any one of them without a second thought calmed him somewhat. At the altar stood Samira walking to
a woman that Zamir did not recognise, it was only when she waved a hand while talking and it went _through_ the altar
that he realised this was one of the Gods, as close to in person as they would ever get in a remote village like this. 

The God turned and smiled at them as they entered. 

"Ah you're here, and two minutes early at that. Excellent. Zamir, my child, I am Dravox, and I oversee the expansion of
land for us all. You are apparently my star pupil!" Her voice was light and airy, but seemed to come from everywhere in 
the temple at once. "Tell me, in your own words, exactly what happened here. Can we get a chair for him please? I have 
a feeling this might take some time."

For the second time in a day, Zamir sat in the temple and explained everything that had happened the day before.

Throughout his story, Dravox sat on a chair which didn't exist until she sat down. She nodded and appeared to be
listening carefully to everything he said. When he mentioned the level of spirits in the pieces of the ruin by the field
her attention, already clear, became sharp focus. "So it felt almost like the spirits were the entirety of the ruin?", 
she asked. 

"Yes! Now you say that yes! But how can that be? How can the spirits be stone?" He answered, the feeling that the God 
was exactly right, sinking into him. 

"It can happen, sometimes. Before the war, the spirits were tasked with maintaining the temples of the old Gods. Normally
that just meant cleaning them and helping to repair very minor damage. But there were so many spirits used, created, 
and controlled during the war that, sometimes they just went a little too far. Holding together dust in the shape of the
block it was when the temples were destroyed. Please, continue."

Zamir then recounted how he'd segmented the final corner, earning an approving nod from Dravox "Things didn't turn out
how you expected them to, but that was absolutely no fault of yours. That was exactly the right approach to take. You 
really should take an apprentice, I doubt there's anyone who knows this job as well as you do. Please continue."

Zamir's recollection of the moments immediately following the start of the fateful ritual was somewhat shaky, but he 
related what he recalled as well as he could. 

Dravox wasn't the only person in attendance who was listening though, and while she accepted everything he said with 
quiet equanimity, there were shocked faces all around, none more so than the other Frontier Farmers, who knew first hand
what it meant when he said that an entire ruin had been cleansed in seconds. None disturbed his narrative, nor gainsaid 
his story. They'd all see the evidence of it with their own eyes, even if they hadn't actually fully digested what the
depressions in the cleansed earth had meant. This testimony was making it clear, and combined with what they had seen, 
seemed incontrovertible. 

When he mentioned the object hitting his skin and, cutting him, there was an audible gasp around the room, though 
Dravox herself didn't seem shocked, only thoughtful at this. 

"Was this object that hit you there when you came to?" She asked as his tale came to a conclusion. 

"I didn't look, I'm sorry. The wound it caused had healed up though."

"That's perfectly normal, the spirits can have a restorative effect on some constitutions, I'm sure you've noticed that
yourself. I remember when I was like you, young and out in the world, fighting the spirits. I'd often take some terrible
wound that would heal in hours after the fight." Dravox laughed, a light and airy sound. "You know what? It's been too
long, far too damn long actually, since I was out on the frontier. I'm going to visit your town, We'll have to name it.
I'll be there in oh, noon tomorrow?"

Sitara's face was a mixture of reverence, horror and shock. "My God, you're coming to us? For this man? I swear he was 
not doing anything malicious, he reveres you and all the Gods. He came here straight away and has never done anything 
that wasn't for the good of the village!"

For the first time in any of their lives, the villagers saw a God walk among them, and be completely gobsmacked.

"You thought I was... That I thought he was... That there would be punishment?", Dravox was shocked and gabbling, but 
she quickly pulled herself together. 

"Oh no, nothing like that!" She exclaimed quickly, "There's only been seven other cases of something like this happening
before!" She gestured at the icons of the Gods around the walls of the shrine.

"We don't punish anyone. Ever. We're not here to judge you, we're not here to rule you. The Old Gods did that, and it 
was worse than you can possibly imagine. They would grant their favoured people power, but take it from those who 
disagreed with them. I had friends that were stripped of the powers they earnt. They earnt them saving the world, but 
because our so-called gods", the sarcasm dripped heavily from her words, Sitara was desperately writing this 
pronouncement down in the corner, "didn't agree with why they did it, they were left powerless. They were left to die.
The reason we killed the old gods, was because of how they treated people. I know you all call these our Temples, you 
call us Gods, and for thousands of years we've tried to stop that, but something in humans cries out for a God I think.
We're not gods, we never were. We were people like you, we just found a way to advance a little further. This is what I 
think is happening to you." Here she looked directly at Zamir. 

"I'm not certain yet, but it looks like you're on the brink of passing beyond the purely human, and becoming something 
more." She smiled a brilliant smile, all perfect white teeth and a crinkle in the corner of her eyes. "I can't wait to 
meet you, to see if you're going to be my brother. See you all tomorrow!"

With that she vanished, and the room seemed darker and less welcoming than it had a moment before.

Without words, a line began to form in front of Zamir. His neighbours, his friends, were queuing before him, as if to 
offer supplication. The thought of everyone trying to worship him before the god... before Dravox arrived tomorrow was 
almost more than Zamir could bear. Before that mortifying ordeal could come to pass though, he was saved by Pavel, his
oldest and closest friend raised his voice and with a laugh in his voice said, "Hah! Wait until I tell Qodir he cooks 
better than an apprentice God. I wonder if you'll ever manage to make hotspice porridge as well as he can? You better 
start practicing, if you are a God, you've only got a few thousand years to get it right. I might be biased, but I think
you'll need longer than that to beat my man!"

Almost sagging with relief, Zamir retorted almost automatically, "He needs to be that good, because you can burn water.
he's using all the talent you don't have!"

With that familiar back and forth, the tensions in the room were broken and the line of supplicants suddenly remembered
that they'd known this man for all of their or his life. 

Antonov stepped forward and asked in a speculative voice "I understand you can cleanse people with a touch. I know you've
been through a lot, but if we can go back to our farms and have an edge like being completely purified. I'd like it.
I mean not all of us can just make a new farm in a day. Some of us need to actually do work."

There was laughter at this, as the whole room relaxed and began to scatter out of the Temple. Zamir did touch those who 
came to him, hand on the shoulder, this would, he hoped, cleanse them as it had Pavel. Soon enough only Pavel and Sitara
were left in the Temple with him, the latter looking somewhat shaken by the day's events. Pavel noticed this and quickly 
went through to the meeting hall to get some sweet tea for her.

"A god was here, in this room. She appeared, and then she told me that she was coming to visit. Tomorrow. Here." Sitara 
was babbling to herself, the revelations having shocked her in a way that Zamir's cleansing of a whole farm hadn't. 
Pavel came back with the tea, concern showing in his strangely light eyes. "Here, drink this and we'll go and have lunch, 
Qodir always has space for one more."

"Lunch? I don't have time for lunch! I need to clean the Temple, no I need to clean the whole village!" Sitara almost 
shouted. "We have to have this place worthy of Dravox's visit!". She was pacing nervously back and forth, her slender 
fingers picking at the strands of black hair which fell across her face. The ceremonial garb of the priests, a green and
gold filigreed pattern, like that of the prayer mat used in the cleansing ritual, was being alternately plucked at and 
smoothed as Sitara's body let out the stresses that were building up in her mind. 

Much reassuring, reasoning and the threat of Zamir simply going back to his farm and making Dravox come to him if Sitara
didn't calm down worked to make her realise that cleaning the Temple was perhaps enough and that Dravox wasn't going to 
smite them all, in fact she'd been at pains to point out that they never punished anyone. 

Later over another cup of tea after Qodir's magnificent lunch, Zamir broached the question that had puzzled him.

"So the Gods, aren't Gods? And you knew that, because you're not worried about it, but you are worried about meeting Dravox." He asked. 

"Well it's not a secret, but we don't go around advertising it. No, the Gods refuse to admit that they are Gods. They 
were humans like you and me, during the Age of the Old Gods. It's said that Dravox was over three hundred years old 
before she and her brothers and sisters, not literal brothers and sisters, discovered the secrets of the old Gods and 
their power and waged war on them to free us all from their tyranny. It's always been the case that they maintain that
anyone could become powerful like them. In the first millennium after the war, that was a central tenet of the Temple. We
largely stopped it after that though, because no one did. And in the millennia since no one has." The recounting of 
familiar stories had been calming Sitara until now, but with these last words the tension returned to her body and she
pinned Zamir with an intense gaze. "Until now. Until you. Will you be the first human since the War to join the 
Pantheon?"

She laughed as the absurdity of the situation hit her. "Here I am, someone who's dedicated their whole life to the Gods,
who's believed in the doctrine of ascension. Now I'm getting upset, not because it could be real, but because the person
closest to a God is my wife's husband. Somehow I never considered that the awkward teenager who hung around my house 
when I was a kid would end up being a God. That's exactly the kind of thing I should have expected though, a person like
anyone else, but a little better." She closed her eyes and rested her head on her hand. 

Pavel had moved from the table with Qodir, and they'd both gone back to work. For the next day or two, this house was
 Zamir's as well. He reached over to his sister-in-law and gripped her hand, "I'm still me. I've not changed. Honestly
I'm probably more confused by this than you are. You, of all people, should know that I _never_ paid any attention in 
lessons when we were children!"

Sitara fixed him with yet another sharp gaze, she was getting a lot of practice at those recently, "You were terrible at
everything that wasn't going to make you the best hunter you could be. It was hilarious when you were chosen as a Frontier
farmer. Your whole life's learnings, suddenly not relevant. The Gods were right though, of course they were. You're 
the best Frontier Farmer that we've ever had, in all of recorded history. Did you know that? We were going to ask you to
take an apprentice. Just two or three more like you in the next generation would have been an incredible boon. If they 
could take apprentices as soon as possible, then in three generations, we'd have a veritable army of Frontier Farmers 
who were like you. We'd be cleansing literally hundreds of times more land than we are now. Humanity would have space to 
grow without restriction." Sitara's eyes were bright and her voice became passionate. This was what she lived for, the 
furthering of humanity. The growth of the country. Not a return to the world that came before the war, but a growth into
a better world. Larger farms made for more efficient production of food, which meant more people could be trained in the
arts, in production of goods, in building techniques. Which would lead to people having better quality of life and 
better health, which would mean they could have more children... and the cycle of societal improvement would continue.

She sighed, as the reality of the day came back to her. There was little to no chance that Zamir would be taking an 
apprentice now, nor that the apprentice would be anything like Zamir. His was an in born talent, one among thousands of 
thousands. Unheard of since the start of this age. _Still_, she thought to herself, _This is the world we live in, and 
a lot needs to be done before tomorrow_

The rest of the afternoon passed back at the Temple, Zamir helping Sitara to tidy up the Temple, grabbing a bucket and 
cloths to wipe down every statue, nook and cranny in the Shrine and the whole floor. He stopped occasionally to cleanse 
anyone who asked him to. She stopped regularly to confirm to curious villagers that yes, the rumours were true, and a 
god would arrive tomorrow. The meeting hall was soon appropriated for a whole village meeting, and all nearly two 
hundred adult and their children were gathered together. The sun was about to set and the chill winds were blowing from 
the mountains again, but the meeting hall was hot and humid. Very rarely would everyone be gathered together like this, 
especially not indoors. Weddings, funerals and the odd festival would sometimes call everyone together, but they were
nearly always at least partially outside. 

The crowd was dressed in two camps, the garish clothes favoured by Pavel, and the rougher, more drab clothes of those 
who worked the land, or hunted. In a community this small, no one was really too far away from the land though. A bare 
moon ago, even Pavel and Qodir had been in the fields, helping to get the harvest in. The same harvest that was going to 
be cut into a little early. It was decided that the next day would be a festival, and that the coming of Dravox would be
feted as was appropriate. 

That was fairly uncontroversial. The Gods left their home very rarely indeed, even from a distance as had happened 
yesterday. A god coming to a town in the flesh was something that happened once or twice a millennium for the largest of
towns. This was a village so small and new that it had no official name, travel to and from it was so infrequent that 
there as no real need for a name, other than the Eastern Frontier Village. So this was absolutely unheard of. There was
no question that there had to be a festival day really. 

The controversy lay in the incipient godhood of their local farmer. 

People knew Zamir, they'd known him for as long as they could remember in many cases. He wasn't old, but he had been 
here for the last forty years. The older among them had known him as a toddler, though his teenage years. They'd seen 
him in the throes of young love and the frantic confusion of young parenthood. They'd been with him in his grief. 

Now, no one was saying he wasn't a good man. A very good man even, but a God? Sure he was the best Frontier Farmer they'd
ever heard of, and they were proud of him for it, but there was quite a step from being good at your job, to being the
highest being. 

Still the fact of the God's arrival was the more immediate problem, and it could end up that this was all a 
misunderstanding. That was why the God was coming after all. There was a general consensus reached where by the village 
as a whole would magnanimously await the judgement of the literal God who was arriving tomorrow, before declaring that
Zamir might actually be what the God had said he might be. 

Once agreement was reached, everything started to happen. People were sent out to check the pit traps on the edge of the 
forest for game. Warehouses were opened and the stored food kept against a dark time was pulled out, ready for the feast
that accompanied every festival.  

For Zamir though, there was little to do now. He headed back to Pavel and Qodir's house before he prepared to have
dinner with his daughter and son. 

Zamir's children were fully grown now, His daughter had her own child on the way, her husbands were a lumberjack and a 
woodworker, so naturally their house was lumber framed and ornately decorated, with really solid furniture. The coming 
child would definitely have more than enough wooden toys to play with, thanks to their dads. 

Then there would be an early night to be had, no one wanted to meet the God with half a night's sleep. Well not at Zamir's 
age anyway. 

Amira's house was large by the standards of the village, and she always asked her father to stay with them when 
he was in town, sometimes he did, and he certainly had planned to stay for as long though the winter as he could, to 
help with the new baby. He remembered his life being upended when Amira was born, and again a few years later when 
Johan was born. They'd been allotted two children, and produced them right on schedule. With the extra land he'd opened 
up to farming in the last few years though, there was definitely going to be a boom in the children in the village. It 
was probably only a matter of years before the country would build a new village on the far edge of his farm, and the 
Eastern Frontier Village would become just another village. 

"Dad!", called a heavily pregnant Amira as he walked through the door of her house, she stepped toward him, somehow
an amalgamation of his own mother and his dead wife. Like both she had the habit of holding a hand on her stomach as she 
walked around. He'd never been entirely certain why they did that, it certainly wasn't for support, they put their hand 
on the top of the curve of the belly. Still, he couldn't help feel that it was right, and that she looked absolutely 
perfect standing there. She had the full lips and laughing eyes of his wife, with the little ridge between them on her 
nose, like he'd gotten from his mother. Her long black hair was tied up in a braid and looked as lush and rich as it 
ever had. That had been the best part of his wife's pregnancies, her hair had stopped shedding and for a few short 
months the house didn't look like a hair monster had exploded in it. The love he felt must've showed in his face, 
because Amira looked at him with a suddenly soft smile and with a tear in her eye simply said, "I'm so happy you're
here." 

He wiped a tear from his treacherous eye and replied "If Umeda could see you now, well she'd be as proud of you as I am.
You're going to be an amazing mother. And I'll get to be a granddad." He reached out his arms, inviting her into a hug.

She snorted softly as she accepted the invitation, as well as she was able with the bulge in front of her. "From what
auntie was saying, you'll be too busy being a God to pay any attention to the likes of us."

"I have no idea what I am when it comes to god and spirits, but I am your father and I love you, and that will never 
change."

"I love you too dad."

The door opened and Johan stepped in, seeing the embrace happening, he just hung up his coat and stepped into that too, 
only to be met with a bat across the chest from his sister and a pointed look at his muddy shoes. Chastened he took 
a step back to the door and took the rough work shoes he wore off. He was the very mirror of his sister, a little taller, 
a lot thinner at the moment, but they had the same eyes and the same lips. The clatter and rattle of two men cooking 
coming from the kitchen died down as it appeared dinner was ready. The rest of the evening passed in the warmth of a 
well made house filled with family and chatter on a cold autumn evening. The food wasn't as good as Zamir would have 
made, the rice being slightly over done and the venison in a black bean sauce a mite too salty, but it was far from bad.

It was probably too late when he said his goodbyes, hugged everyone and walked across the village to Pavel's house, 
accompanied by his son. One topic that hadn't come up during dinner was the arrival of the God the next day. Almost like
they were conspiring to keep everything as it was by not talking about it, or what might happen to Zamir when the God 
had a proper look at him.

They said their goodbyes and Zamir went in to sleep again at his best friend's house. Perhaps for the last time, 
but hopefully not.