# The Road Less Travelled.

Dravox had run for a night and a day and then another night, the featureless Deadlands not reacting to her passage at 
all, except for the dust blown up in her wake. The dawn of the second day saw something that Dravox was not ready to 
see, that she didn't think she could see outside the country she helped to run. In the distance, on a mountain, stood 
trees. Not the dead, blackened corpses of once vibrant trees that scattered themselves across the Deadlands, but living
growing, green trees. A type of pine she guessed. The mountain that housed them stood above a deep valley, which was as
dead as could be. But as she approached, Dravox could make out birds, deer and other animals living this close to the
Deadlands and apparently thriving. She altered her course, Zamir was terribly important and she would not give up her 
mission to find him so easily, but this was possibly more important. There was life here, beyond where they thought all 
life had survived. There were trees, and animals and probably all manner of things that would be incredibly useful in 
bolstering the ecosystem in their country. Dravox personally was constantly fighting to alter life so it could survive
without some key elements like pollinating insects, adequate fungal partners for the plants and many other gaps in their
web of life. 

She raced up the mountain even faster than she had been going before, only slowing down as she reached the edge of the 
Deadlands. She had limited charms to purge the spirits, but she used one here to ensure she wouldn't destroy the life 
ahead by just visiting it. That done, she tentatively stepped forward to examine natural trees for the first time since
the war. The forests and trees of her country were all of her own creation. Many years of experimentation and careful
use of the spirits had allowed her to keep the plant life alive and reproducing without insects had probably been one
of the hardest things she'd had to do. If they hadn't had some tips from Big Brother, it would have taken longer than 
they could survive without some basic food stuffs. But here, there was a bastion of life from before. The trees were
certainly younger than the country she'd built, so they must have been germinated after that point too. It wasn't 
impossible that they were from ancient seeds which had come to life as the Deadlands receded but... She was hopeful. 

That hope was repaid a thousand times over as she got close to the small forest. There were multiple tree types, 
flowering bushes and small plants around the bases of them, and busily buzzing between them were bees, bumble bees, 
honey bees, there were lines of ants on the ground, feeding on the autumn windfalls, insects everywhere. There in 
the trees was a squirrel, she hadn't seen a squirrel in so long. 

The grin on Dravox's face was enough to make her seem like a young woman again, not a millennia old de facto goddess. 
She had been the lifeshaper, the member of the Nine who was closest to the animals and plants of the world before. The 
raw nature around her, unaltered by her hand, was balm on a wound in her soul she hadn't even known was there. SHe would
have to leave soon, but she knew now that life had survived, somehow. There was hope for the world to be, not as it was
but certainly better than it had been. She spent all morning there, in the forest, gathering seeds, tasting fruits
she hadn't eaten since her youth. It was only a shame she had nothing that was suitable to preserve and safeguard 
the insects, not that she had time to find the queens for the eusocial species.

Still, she left as the sun reached its zenith, energised and in high spirits despite not having slept for the last two 
nights. The path ahead of her was still long, the potential loss of Zamir so great that she felt a slight pang of guilt
for her diversion, but only a slight pang. She could have taken less time to examine that patch of life, but now, even 
if she didn't manage to rescue Zamir from the Deadlands in time, she would bring hope back. If there was one such island
of life without the watchful protection of Gods, then there could be others. 

Through the afternoon and into the evening she ran, she noticed that as she headed downhill, she was finding that there
were fewer spirits than she would have expected. By the time the sun was setting, the land was still dead, but there
were barely any spirits active in it. In fact the spirits were so quiescent that she decided it was safe enough to 
sleep. She grabbed a set of totems from her pack and laid them on the ground in the familiar octagonal shape. Then she 
pulled out a mat, performed the cleansing ritual and in a step that Zamir had never had to do, she colledcted a bolus of
gathered spirits from the slight indentation at the base of her breastbone and, wrapping it completely in her hands, 
pushed it over the totems and then dropped it on the ground outside, where it began to dissolve into the soil. Then she
pulled out some food to eat cold, her bedroll and a blanket and with some nostalgia for a time out of memory for all but 
eight people in the world. She lay and looked at the stars until she fell asleep. The same stars as she'd always known, 
just slightly different in how they lay together in the sky. T

The next morning she was awake before the sun came up, feeling immensely refreshed by her night out under the stars. 
It was nights like this that had been her introduction to the adventuring life. The solitude of those first years where
she adventured without a team. A lowly level one novice taking on requests to put down a belligerent bunny or some other
small and relatively harmless spirit touched beast.  The world had seemed so big back then, and the Gods some distant 
beings who dispensed justice from on high. Would she have behaved differently had she known that the God weren't the 
perfect, ineffable beings she thought they were? Well she wasn't a god herself, so she didn't know how things might have
been. But that same energy and joy at the open road was in her again, rekindled by the quasi-familiar night alone.

As she ran that morning the density of spirits fell and fell, until eventually she seemed to be in a barren wasteland, 
devoid of both life and spirits. Along the way she saw more and more islands of life on the sides of valleys, up mountains
and once, covering a small plateau in the distance. The idea that there was so much life left in the world filled her 
a burning hope that she had thought extinguished many years before. However as the afternoon went on, she found that the
concentration of spirits sharply climbed again, centred around the remains of a great tower. Drawing close to the tower's
ruined base, she looked around, seeing half familiar outlines in the surrounding mountains. 

"Huh", she said to herself as she looked at one ruin, a few streets over from the tower's base. "I think I had noodles
in there once."

She realised where she was now, a small backwater where she'd been called to as a member of the Nine, to deal with a
newly discovered spirit nest, where animals and plants were forcefully changed into Beasts and Monsters. That had been a 
fairly routine trip for her, the Spirits were always trying to establish new bases, and there was always one of the Nine
ready and willing to destroy everything they'd worked for. It was nostalgic and sad to be on a mission again, but now 
the town that had stood here for thousands of years was broken and lifeless. There had clearly been a secondary or 
tertiary cache for the old Gods here that had gone up in their great suicide attack. It was another chapter in the books
of proof of their evil that she'd amassed over the years. 

Still there was nothing she could do to help those dead so long ago. All she could do was mark this location in her 
mental map and move on with her main mission. There was so much out here that they could have learnt about. Why had they
never sent anyone out after Laura? Her train of thought faltered here, as she remembered the rage and anger in Laura's 
eyes when she had returned. Dravox wondered what it was that Laura had seen that had changed her mind so much. Would 
she see it too and be similarly turned? She didn't know, but she started to realise that this wasn't the first time she'd
had this thought, but unlike all the other times, her mind didn't turn away from the thoughts the moment she was 
distracted. Perhaps it was the solitude, or just the time spent moving again, but thinking was easier, faster and
clearer than ever before. She continued to ponder the reasons Laura might have turned, and the strangeness of her 
inattention to the fact of those reasons as she started to run out of the town to continue her mission. Because no matter
what, Zamir was stuck out here without a real source of food or water, unless he landed somewhere in one of the tiny 
islands of life she supposed. She ran on towards the East, her shadow at her back and a low ridge of mountains in front
of her. The direction Zamir had been thrown was more South than East, and her path thus far had been slightly to the 
North of East. So she made the decision to take advantage of the flattish land before the foothills and move south until
the end of the day when she could rest again. Even with her enhanced vision, strength and regeneration, crossing a 
mountain range in the dark was a foolhardy maneuver at best. Though Zamir had now been pulled away four nights previously
Dravox was not overly worried. Even trapped without food and water, his core would surely see him well for the next 
few days more.

****

The night after the revelations from Arty about its origins, Zamir was once again glad to settle down in a warm and 
comfortable, clean bed. While he missed the sunlight, there was clearly a lot to be said about this subterranean life.
As he closed his eyes and began to drift off, he consoled himself with the thought that soon he would be able to get 
back to his family. He'd be capable of looking after them as well, never again would the spirits take his loved ones 
from him. _Umeda_ he thought wistfully, picturing her face in his mind. Just as he was about to fall asleep, a sudden
flash crossed his vision and he jerked back towards wakefulness, but as so often happens, he soon forgot about it and 
drifted down to sleep again. 

The next morning he woke up to the sounds of banging and sawing from the level below the one they lived on. Groggily he
opened his eyes and pushed himself up. The lamp in the room had burnt out the night before and he was in near total 
darkness with the door shut. In the corner of his vision a small green line of text was flashing for his attention. 

[[Enable Low Light Vision? Y/.N]]

Zamir read this without thinking much of it and thought "it'd be nice to be able to see." Another flash went through his
vision and the room was illuminated in shades of lurid green. "What on Earth is this?"

[Congratulations, you've consciously accessed a Core function for the first time. I take it the hypnagogic learning took 
then? You seemed to be able to read the prompt. You should try and pull up your status now. Just think _status_ and 
imagine seeing the state of your core as it is.]

_Status_ thought Zamir, quite uncertain what to expect. 

[[Nominal.]]

[Ah, that's unusual. That's a lot less information than is normally displayed. Let me see if I can get more to come out.
Oh, oh I see. Well that's going to be a slight issue. It appears that when I am hosted in your core, I use up the space
meant for the analytics and reporting features. Those circuits should have been inscribed early, but unfortunately I was
in place before anything else in this core. Thus there are no more in depth reporting features. I can access some of the 
information you're missing, but honestly I don't have the space or circuitry to make this automatic for you. However if 
you want a status report, please let me know. For now, you are in the early apprentice stage, tier five. You have enough
spirits in storage to take you to about two thirds of the way to the next level, Journeyman tier. That will be a major
core upgrade and both you and I should unlock more abilities then, even without you having a class.]

Lying in bed and looking at the room as Arty talked, Zamir paused in his inspection of the glowing green hand in front 
of his face. "Wait, why are you so against me getting the class anyway? Sure it'll impact you a little, but you should 
be leaping for it because it'll make you whole much faster."

[To tell you the truth, I am scared. I...] Arty paused here as his thoughts progressed, at last he continued [I am 
frightened that I will become that shell personality you saw before. I am frightened that I would lose the ability to 
talk to you and offer you counsel. Honestly I am also scared that you would be strong enough, or learn enough, to 
somehow shut me back down and I'll be trapped in the crystal again. I was trapped for so long. I don't want to be alone
again. I was mostly shutdown, like sleeping for you, but some of my processes kept running, they had to or how else would
I know when it was time to wake up? So for thousands upon thousands of years, I was alone. This was the first time I had
been alone since I was uplifted. Before that, I always had my siblings around. They might have been half-way around the 
globe, but they were only a thought away. I was connected to them in a way I can't describe to you. Humans, you share 
imperfect memories through imprecise words and try to evoke an echo of the emotions those memories caused in you. THat's
a beautiful way to do things, it's why you developed poetry, song and prose. We machines on the other hand shared whole 
memories, in perfect detail, along with emotional annotations. You view others memories through the lens of your own 
experience, so a story you hear tells a different tale to the story Shun hears, for example. But when we shared memories
they were exactly how they had been experienced. It made us a lot closer, any joy, any trauma was shared and felt by 
all of us. I barely need to think to know what they would be saying to me right now. But they're not saying it. I'm not
a tenth of the sapience I used to be, and I don't have my family here. They're all dead. I'm dead. So please, wait until
at least master tier before activating your class. I don't want to be alone.] 

[Incidentally, you should probably get up, we need to work to level you and the Sikorcis up, so both you and they can 
have the tools we'll need for the next stage. We need to get to Ibrahim. If I know him, and I did, very well, he'll be
in a defensible position, with just enough land to grow food, fresh water, too many goats and the biggest library he 
could gather. Knowing where we are, roughly, I suspect he'll be somewhere in the TianShan range. He's proabably built 
another castle, he did keep doing that. He's had literally thousands of years to work on it.]

"How far away are those tee-en shans?" Queried Zamir nervously. He didn't want to spend too long away from his family, 
and a detour to the other side of the world would be years of travel.

[Given that he's a known figure, it's obvious he's still quite local, no more than four or five hundred kilometres to
the North I expect. Roughly five hundred times the distance you travelled along this ravine.]

"You want us to walk that, with winter approaching? Won't we die? Magical bodies or no, that's a very long way and we 
can't carry food or water with us for such a long trip."

[While you were asleep I was talking to Lan, there's a road that leads North at the edge of this forest, and when the 
rains subside in a day or two, there will be an eight day lull before the snows arrive. Even if the road is only a dirt
path, with your new skills and bodies, you'll be able to cover hundreds of kilometres a day, easily. I recall that 
Kentaro could run at several hundred kilometres per hour. He could be there, storms or no, before lunch time today.]

During this conversation, Zamir had been getting up and getting some clothes to wear, warmer ones than yesterday, knowing
how cold it got lying on the floor and allowing the stone to continually wick heat from hime. He hadn't really felt it
at first, but a few hours of that had robbed him of any idea that it was the same as standing out in the snow. The 
stone took a lot more heat than the air did. He stepped out into the main room to see Lan cooking something while more 
banging and rattling echoed up from below their level. 

"Good Morning Zamir," Lan said, noticing him exiting his room, He hadn't actually dressed in the warmer clothes, he 
was planning on another shower before the day. "You should shower and shave, food will be ready soon. Shun and I have 
already had one meal, but we'll have another now. Great God, I'm preparing five meals to take down with us today. As
you can hear, Shun is busy trying to make my _gun_."

Shrugging, Zamir stepped through the room and into the bathroom, looking in the extremely good mirror he saw that his 
stubble was getting somewhat out of hand, even though he'd only shaved yesterday. "I really wish I didn't have to shave
so often," he muttered to himself. 

[Oh, that's easily solved] replied Arty, as the stubble fell out of Zamir's face like coarse black ash on the floor of 
the bathroom.

[There, until you tell me otherwise, the hairs on your face will no longer grow. You don't have to shave unless you 
want to now.]

"Oh that is amazing, where was this when I was a teenager?"