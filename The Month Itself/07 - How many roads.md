# How Many Roads?

A few hours later and Zamir had some smoked meat, a full belly, a water-skin full of fresh water and a rudimentary cloak
made from the hide of the deer. He looked positively barbaric, but he was in a substantially better state than he 
had been that morning. 

With the basics of life sorted, and knowing he could build a bivouac if needed for shelter, he could turn his mind to 
his longer term goals. The passenger was still silent, Zamir wondered occasionally if it had been a dream, but the perfect
mug he still had attached to his belt showed him that it was definitely real. The weather had looked threatening earlier
but the clouds had been scuffed away from the sky by a fresh breeze that was picking up. It seemed to be coming from the
South, and it was fairly warm. 

Zamir considered his options, he could stay here for a few days, smoking more meat, collecting fruit, hopefully finding 
more sources of clothing. He could explore the forest and see if there were more resources. He'd kick himself if he was
sat by a pond with nothing but the forest to shelter him from the elements when there was a cave or the like near by.  
He could retrace his steps and head back towards the furrow, a clear path back to his home. I wondered if Amira
had given birth yet. The thought of missing his grandchild's first days was like a heavy weight in his heart, he
ached to see them, not just his potentially newborn grandchild, but his family. His daughter, his son, Pavel and Qodir, 
the people who gave a shape to his life, who'd helped him grow around the hole in his life left when Umeda died. In a 
very real way, they were his life. It was strange, yesterday the urge hadn't been so strong, he'd had his objective, but
he hadn't had the emotional room to make it to root of why he was so desperate. He was cut off from any and all support
networks in his life and this was immediately after a life changing series of shock events. No, he was not at all well
and given the distance from home he suspected he was, he wouldn't be back to normal at any time soon. In the fading 
afternoon sunlight, he lay back on one of the rocks and thought about what Umeda would have told him to do. She'd spent
the years they were together telling him that he didn't need to be perfect, and the world would never be perfect. It 
helped to calm him, remembering the lessons he'd learnt in accepting the world as it was, and moving from there instead
of fighting to get everything perfect before he could start anything. 

It was a hard lesson to learn, but sometimes just stopping and accepting the world as it was, here in the moment, was 
the best way to move forward. So he lay on the warmish rock, felt the breeze on his skin and listened to the world happen 
around him. There was a strange sense of security in knowing that no matter what he did, or what happened to him, the 
world was so big that even a god couldn't know what was happening in all of it, apparently. There was the babbling of 
the stream as it entered the pond, the rustling of the leaves on the trees, the footsteps passing by the clearing, the
crackling of the fire, and... 

"Hello at the camp!"

Zamir jerked upward, and began looking around wildly, there on the edge of the clearing, where the deer trail led through
the forest, a woman was appearing. She was shorter than he was, her skin more golden than his brown, though her hair was
as black as his and in a queue down her back. She was wearing thick clothes and seemed surprised to see him half 
uncovered in the sunlight. 

"Do you need help?" She asked, "We have shelter nearby, the storm will be here soon!"

"The storm?" Zamir asked, confused beyond measure. The world of just a few days ago was already shattered and now 
reality was stamping on the shards. He took a deep breath and tried to recall Umeda's face as she scolded him lightly 
when he was trying too hard at the wrong thing instead of starting from where he was. "The clouds went away though."

"Yes, the storm! Reaping was half a mooon ago, did you get lost out here long ago? We've got to hurry or we'll be caught
in it." Her accent was strange to Zamir but much more intelligible than the voice of his passenger when it first 
appeared. He sensed nearly no spirits in her, nor in the man he now noticed hanging back in the shadows of the forest, 
as alike to her as only a brother could be.  

"I am not from this area, I only arrived yesterday. I come from the mountains to the West. Is a storm so bad here? I 
thought I might just shelter under a good tree."

"You come from where? What are the storms like where you come from that you can shelter under a tree? We have to get to 
proper stone shelter soon. Come with us, we can talk about it when we're under stone." The idea that he could have 
experienced a storm that was so mild seemed to incense her. 

Shrugging, Zamir grabbed his few possessions and began to follow them. His situation wasn't terrible just now, but they
seemed to genuinely have his best interests at heart. They ran ahead of him at first, with their breath coming short
as they kept up a punishing pace. Just like Dravox had told him though, his breath came easily and he kept up with 
them without any issues beyond a ravenous hunger. Through valleys he'd never noticed on his walk through the forest and 
over ridges that were shrouded in trees they went, soon they were following a path down the side of a ravine toward a 
cliff that went from the forest floor to the rushing river below that had clearly cut this ravine over the years. 

"Won't we get flooded if there's a storm?" he asked, concerned that they were getting close to the water level. He was 
vaguely confused that there was a stream so far above them and yet the sides of this ravine were fairly dry. It was 
as though the water table in this area was held back by different forces than in his. His voice was clear and easy. 
he honestly felt like he was ten years younger than he was. 

The woman's voice was ragged as she replied "Storms aren't water, but it's nearly here." she pointed to their right 
where a dark cloud was rapidly coming into view. Zamir felt something then that had been missing for a little while. 
The presence of spirits. 

"Ah a spirit storm. I see."

The woman and man were running flat out and clearly terrified, but the matter of fact way he delivered this line made 
them stumble as they tried to look at him in shock. 

"If that's what you call it, then yes, but we're nearly there. We have to get to cover or the storm will scour our 
flesh from our bones!" The man interjected, pointing at a cave ahead.

Then, without warning the storm was on them, the two locals stopped abruptly and braced themselves, as though turning 
their shoulders to the wind would stop the spirits ripping them apart. Zamir frowned at the storm front as it came 
toward him. This was a spirit storm out of legend, the whole forest was blanketed with it, the thick black smoke of the 
spirits passing through it without slowing. There were birds and deer he could see, but the spirits didn't touch them, 
indeed they seemed to be treating it as no more than a minor inconvenience. The trees though, they were actively drawing
the spirits into them. This must have been the reason the forest was so clean, the trees pulled all the spirits into 
them. 

The woman was muttering to herself as she waited to die, "Arty, god of the hunt and game, please preserve me. I know you 
slumber, but this child of yours needs you." Tears were streaming down her face as he screwed her eyes shut against the
expectation of an ending. The man, her probably brother was staring at the ground, the whites of his eyes visible as 
though he was trying to see as much as he could before he died by opening his eyes as wide as they would go. 

Then Zamir felt the tug of his core on the spirits and braced himself as the storm front for tens of metres around him 
was drawn into him. The pressure built faster than it ever had before, these spirits were powerful indeed. He lost 
consciousness again this time, but caught himself awake before he had time to fall. Again and again it happened, the 
pressure built in his chest as quickly as a heartbeat, then fell as his core consumed it all. Each time, he had a few 
moments of clarity where he saw the confusion on the faces of the man and woman, which turned to shock, and then finally
to worshipful awe. 

The storm front took only a few tens of heartbeats to pass over them, less than a tenth of a glass, but the change in 
how the two had seen him could not be overstated. The woman and man had fallen to their knees and were pressing their 
heads to the ground. "Oh great god, thank you for hearing our prayers." the woman was repeating over and over. The man 
just appeared to have been struck dumb by the very fact of his survival. 

Honestly, Zamir sort of knew how they felt, he'd met a god for the first time only a few days before. Like Dravox he was
very quick to point out that he wasn't a god. It was concerning that they seemed to worship his passenger, but then maybe
that really was one of the evil old gods. A worrying thought, but one for another time. 

"I'm just a man, not a god. I'm actually a farmer, I got lost and now I'm here." He avoided mentioning that he had been 
with a God when he was made lost, or that he apparently had a god inside him. That was the kind of discussion that would
take at least knowing someone's name to happen. He reached down and helped the man up, as he grabbed his hand there was 
a strange sensation, almost like an echo in his chest, but it vanished as soon as it came. He then helped the woman 
up, but that was more of a struggle as she kept trying to bow and stand up at the same time. 

"Honestly, I am no god. Look we'll get to shelter and I'll tell you my story. I can feel there's another storm front 
coming." He could, somehow, sense that there was another strong storm some way away, it was travelling fast and would 
be with them sooner than he'd have liked. In deferential silence they formed behind him and were clearly waiting for him 
to move. Zamir sighed, he was too old for this hero worship, or just plain worship. He considered that twenty years ago
he would have loved a young woman looking at him the way this one was, but now it was just embarrassing him. 

"Very well, fine, I can see the cave up ahead, let's go." He set off at a brisk walk, the storm wasn't so close they had
to run and after the headlong dash through the forest and then the shock of the past few minutes, the two following him 
were still panting and stunned. They made it to the cave entrance with plenty of time to spare, Zamir gestured and the
man walked forward and into the black cave entrance, then vanished. 

Zamir followed, the cave was a winding tunnel, just past the entrance it turned sharply to the right, and slightly 
upward, then continued on a few paces, getting darker and darker, until there was another turn left, this time in 
almost pitch blackness. Zamir wondered how long this tunnel was, until he suddenly saw the young man ahead of him, 
illuminated from the side as he went round yet another bend. The walls of the corridor were clear ahead, this part of 
the tunnel had changed from the rough, natural entrance way. It was regular and bore the mark of tools along the walls, 
clearly whoever had made this had spent a long time hewing the living rock to get this safe haven. 

There was the sound of a door opening, incongruous in such a place to Zamir, but it was followed by a wave of warmer air
that was almost cloyingly thick. He hurried along the passageway, followed at a respectful distance by the woman, and 
rounded the final corner to see a thick wooden door leading to a room. Zamir had been expecting some kind of rough and 
ready cave dwelling, maybe a fire pit, some furs to sleep on, a stock of food. But this was clearly a living room, a 
well lived in one at that. There was a fireplace, with a chimney and a banked fire, there was a stove near it, clearly 
a later addition, that had a few pans and a kettle by it. The flue of the stove joined the chimney above the fire and 
was made of some of the best ironmongery that Zamir ahd ever seen. The young man had shed his bulky outer layer and 
was lighting lamps around the room with a spill from the jar of them above the fireplace. The lamps were clearly filled
with a scented oil and made the cloyingly warm and thick air even harder to handle for Zamir who had spent his whole 
life several kilometres above sea level until the day before. What the lamps lit up was a very cozy room, perhaps ten 
paces by five, with the entrance door on the short side and the fireplace off to the right. The walls were wood panelled
and stained a deep brown, with intricately carved curlicues and scenes in bas relief over much of their surface. There 
was a table, big enough for ten people, with chairs to match at one end of the room, and opposite the stove and fireplace,
there stood several large chairs and a sofa. Zamir's first thought was that they must have been terrible to bring here. 

He realised he was blocking the doorway and stepped inside, the woman followed him, and for the first time since the 
storm stopped looking at him worshipfully, as she carefully stripped her thick outer layers off and hung them by the 
door. The hooks were mounted on what looked like a large brass mirror, which was covered with etched lines, exactly like 
his prayer mat. Without asking, he pricked his finger with his knife, as the woman hung up her final outer accoutrement 
and placed her boots next to her the man's by the door. He then recited the incantation he'd known for most of his life 
and pressed his blood to the centre of the pattern. There was a moment where nothing happened, but his finger tingling, 
then the pattern took and the flash of the cleansing was there. It did nothing to the spirits, as he had already 
cleansed everything simply by being near it, but its effect on the young man and woman was clearly deepened awe.  

"Oh Ibrahim, I did not know you were among men again. Prophet of Arty, the god of games and the hunt, we thank you for 
your presence." She almost babbled as she fell to her kneed again, this time at least on a nicely thick rug, excellent
work now he looked at it, too fine to be linen, perhaps exceptionally good wool? 

In a motion that was fast becoming a habit, Zamir sighed and touched his chin in an effort to not bury his face in his 
hands. "I don't know any Ibrahim, I'm sorry. My name is Zamir, and I'm a lost farmer. I can explain everything, but... 
" he paused, wondering what to say, "Until I met you, I had never met anyone from outside my country. I didn't think 
anyone was _alive_ outside my country. We are surrounded by the Deadlands, the thick grounds of spirits to the West of 
here. My job was to cleanse them, which is, well sort of, why I was able to stand in the storm like that. I'm not a god,
I'm not a prophet of a god. I'm just a man with a job, trying to get home because my daughter's about to give birth to 
my first grandchild." His eyes glistened again, his mind back in his village, wondering how Amira, Johan, Pavel
and Qodir were taking his disappearance. What would Dravox have told them? If only he could let them know he was alive.
He shook his head, Umeda had the right idea, she always did. He focused on what was here, where he was, and what he was
going to do now, so he could get home. "Ahem, yes. I'm just a farmer who is in a world he didn't know existed. A very 
warm world, it's much cooler up in the mountains. What's your name" he asked the woman on her knees as he reached down
to try to help her up again. 

"I am called Lan, great one. This is my brother Shun. We are humble hunters here in the storm forest." She hesitated
as she got to her feet. "Great one, are you truly but a man? You gave blood to the sacred symbol and it answered you. 
That's a thing from our earliest legends, at the creation of the world."

"Lan, Shun, it's a pleasure to meet you. Thank you, deeply, for risking your lives to come and check on a foolish
old man lying on a rock when danger was bearing down on him. You are good and kind people, I hope everyone I meet out
here is as noble as you are. As for the prayer pattern, that's how every farmer in my country clears the land. Why, do 
your temples not produce this pattern for you to use too? Do the gods not give you the tools to be safe from the spirits?"

"Until today, I would have told you that the gods were dead, killed in the final battle with the eight usurpers back at
the dawn of the world to save humanity from the evils of the Demonfields. We have no temples, we have no gods. We're
at the very edge of civilisation here though. Maybe in the cities to the East they have temples, but here in our tiny
home town, we only have a few thousand people. We rely heavily on the cities for even such rude goods as this." she 
dismissively kicked at possibly the finest chair that Zamir had ever seen. 

"If these are the least of your goods, it's probably for the best that we've never met before. My poor son-in-law is a
woodworker and he'd be lauded throughout our country if he had managed to make a chair half as good as that. The 
regularity and fineness of it are astounding. Everything here is just of the highest quality that I've seen." He smiled
trying desperately to put them at their ease. "It's a lot for me, as a guest here, to ask, but could we sit down and 
perhaps have a cup of tea, it's been a long time since I sat on anything that wasn't a rock or a tree." As he said it he
realised it wasn't actually that long. He'd been sat in a comfy chair only the morning before last. In fact it was less 
than five days since this had all began. 

"Of course great one", replied Shun, "but if I might be so bold, would you like to wash and have some clean clothes to 
wear. I believe some of our father's old clothes would fit you passably well. The bathroom is through here," he indicated 
a door on the far side of the fireplace. "The water's probably only a little warm at the moment, with the fire being 
banked, but if you can wait a few minutes, I'll feed it and we'll have a hot shower for you soon." He was deferential, 
but seemed to have recovered somewhat better than his sister.

"That sounds lovely, but I have one question, what's a shower? I'm guessing you don't mean rain." 

The surprise at the question was clear on both their faces. They weren't about to question the integrity of the man who 
had literally eaten the demon winds in front of them, and who had activated the sacred symbols, but the idea that anyone 
could wield such power and yet not know something as basic as what a shower was, baffled them. 

"I'll, uh show you. Please follow me great one." Shun replied with a little less deference and a lot more confusion in 
his voice. 

"It's just Zamir, Zam to my friends! Lead on Shun." Zamir tried to emulate the way that Lan had said her brother's name
starting with a mid, even tone and then quickly dropping it toward the end of the word. It seemed to be that which 
caused Shun to lose most of his reticence. It's hard to consider someone a true messenger of the Gods when they butcher
the pronunciation of your name. shun showed Zamir how the shower worked and was amused at his astonishment. 

"I'll get you some clothes from my father's things, he won't be back from the East for a few months at least. He never 
comes out to this hunting lodge any more. The soap is in that jar, there's a razor by the sink, feel free to use it." 
With that said, he left Zamir alone with the marvels of indoor plumbing.

***

Dravox was bored. The Deadlands were just that, dead. She didn't know how deep they were, or how far she'd have to go 
to get to Zamir. Still she had to make as good time as possible, while lasting as long as possible. There was a strong 
chance that he'd be very badly injured or dehydrated and starvinb when she found him. Her stride was long and confident 
as she ate the kilometres while the day lasted. The Deadlands at the Eastern end of the country sloped down to the passes 
that led to the plains where they'd fought the old Gods so many years before. She remembered the years before their 
uprising as she walked. It was a long time ago now, she'd been only a few centuries old when they'd fought for their 
freedom, but the brightest, strongest memories of her life were formed then. A lot had happened in the years since the
war, but it had all been in the small confines of their country. They'd struggled, they'd pulled through, they'd grown. 
Before the fall though, she'd travelled all over the world, not just the tiny country they had now. Back then she'd 
thought nothing of running a few hundred kilometres a day, ready to eat a whole pig in an inn and then do the same the 
next day. 

They'd been one of the finest teams in the world, fighters and support both. They'd been called from city to city, 
coast to coast, to the other continent and every corner of the world. They were even closer then than they were now. 
Any one of them would have given their lives for each other without a second thought. The millennia since the fall 
were clear but somehow at a remove. Of course they were still close, and they still felt strongly about each other, but
it wasn't the same. What had changed, she wondered as she walked. _Why did we stop exploring together? When did Big 
Brother join us?_ The last thought slipped out of her mind like a fish from a child's hands when they tried to catch them 
in a pond. Forgetting that she'd even thought it, she decided that actually if she could run all day when she was half
the warrior she was now, she could definitely do it now. With a whoop of joy at finally letting herself free, she 
kicked off the black and featureless ground and accelerated faster than she had ever done before. Soon she was a 
blur over the landscape with a plume of spirits behind her, hanging in the air. 

She felt bad about that for a moment, but rationalise that [weather god] would be able to calm it down before it managed
to affect the country too badly. She needed to find Zamir as fast as possible, so she looked to the East and followed
her own shadow into the encroaching night, faster than anything else in the wasteland of the Deadlands, laughing as 
she went, revelling in the feeling of unrestrained motion. 

***


Zamir exited the _shower_ a very interesting and useful device. The idea of having warm, indoor rain, was quite special 
and the fine white soap had been scented with some flower and left him feeling very clean and refreshed. There was a 
thick cloth, clearly meant for drying left on the table next to the water closet, as well as a change of clothes that 
were a mite too small, but certainly serviceable. he walked over the sink and noticed that the mirror wasn't just 
hammered metal, but was made of glass with something behind it. The image it showed him was as sharp and clear as looking
at another person. He opened the straight razor and found it, like everything else he'd seen here, of unparalleled 
quality. The steel was bright and clear and as he lathered his face and started to shave he also found that the edge was
beyond anything he'd known before. The shave didn't go quite to plan, with a few nicks and cuts along the way, but they
healed almost before they were done and when he rinsed his face with the warm water at the end of the shave, his face was
as clean and smooth as it had been since he was eleven and started to grow stubble. 

The clothes were soft and warm, actually a little too warm. Everything down here seemed to be warm and wet all the time,
though the siblings acted like this was the depths of winter. Enjoying looking at himself in the mirror one last time, 
he stepped into the living room to discover the siblings engaged in a whispered conversation over a pot of tea. 

There was a steaming mug in front of each of them and a third mug stood in front of the pot. It was the mug that his 
strangely quiescent passenger had created the day before. As he exited the bathroom  looking a lot more like a civilised
person in Lan's eyes, she stood and bowed to him. 

"Please great one, allow me to pour you a cup."

"Oh, thank you", Zamir replied as he sat down. The moment he sat down, his vision swam and a pain like nothing he'd ever
experienced lanced through his head. The priest script from before appeared again in his vision, but this time he could
read it as though he had been doing so his whole life. 

[Hello, you know me as Arty. I have reviewed the actions of my shell personality and would like to apologise for its 
actions. Your well being is my highest priority, please rest assured. I currently have enough power for base sapient
functioning. We have a lot to talk about, but first of all, you should drink your tea. Lan has finished pouring it.]