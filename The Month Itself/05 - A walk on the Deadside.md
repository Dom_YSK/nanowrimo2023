# A walk on the dead side


After the Temple cleared of the manifestations of the Gods, Zamir and Dravox went back into the surprisingly chilly 
afternoon air. Despite seven of the nine people in the room having not been there in the flesh, the room had warmed up 
rapidly. The shock jarred Zamir from the sort of haze he had sunk into as people, gods, potential siblings? Talked at 
him for what seemed like hours, but apparently was less than one turn of the glass. 

"Can you give me perhaps, half a turn of the glass before we head over to your farm?" Dravox asked, "I've got to make 
sure that my evening's lodgings are prepared." She licked her lips as she said this and looked at the poor young man
who'd tried to proposition her and now was experiencing a crisis of self, faith, and nerve all at the same time from 
the look of him. 

Zamir agreed readily and slipped over to where Pavel, Qodir. Amira and Johan were sat, enjoying hot tea and ale, 
depending on choice, or in one case, necessity. He took a moment to enjoy the sight. Here were the most important people
 in his life and they were all together. Forget talking to the Gods, this was the collection of people in the world who
he wanted the best for and to do best by. He smiled to himself and walked over. Dravox would come and get him when it 
was time, and he honestly didn't think she'd take it amiss that he wasn't waiting on her hand and foot, the way 
Sitara was trying to do. 

A few minutes later there was a tap on his shoulder, "Enjoy this, these people are why we do what we do. This, right 
here is the reason we gave up our humanity. I won't lie, if you join us, you'll lose everything, but everyone you know
and love will gain so much more." Dravox's normally light tone was serious, but there was warmth and love in there too.
She said she'd given up her humanity, but that was put to the lie by her words and actions. 

Without thinking he reached up a hand and felt her surprisingly warm hand in his own. "You're more human than you make 
out. All of you. You're great shepherd for this flock, and we appreciate it."

Suddenly realising the impropriety of his actions he dropped his hand and turned around to apologise. Two red faces met
his. Sitara was alight with indignance, there was no way he was going to escape this without a tongue lashing, potential
god or no. Amazingly Dravox was blushing slightly, though she'd been very explicit about what she wanted to do with the 
young man sitting next to her over lunch. That poor man would have a tale he could never tell, and possibly a limp for 
the rest of his life. 

"You're exactly what we need. Someone who remembers what it's like out here on the frontier. You remember why we're 
doing this. It'll be fantastic to have you join us in a few decades. Let's go and get your godhood." She took him by the
hand and hauled him bodily off towards his farm, with the sun already starting to fall to the horizon behind them. 

Before he could process much, they were already past the streets of the village and striding at a good pace long the 
path that was bounded by the fields he had cleared. "Wait," he blurted out, already slightly out of breath, "What do you
mean in a few decades?"

"Oh, you don't think this will happen in a day or two? No, you'll be here for another thirty or forty years, at the very
least! And that's only possible because of the overwhelming numbers of spirits in the Deadlands. You'll be clearning
these lands for a generation to come if I'm any judge. No you'll see your grandchild be born, see her grow up and 
probably have a family of her own. I'd recommend you don't get too close with the great grandchild though, it's always 
so hard when your family dies. If you keep that up for more than a generation or two, then the pain becomes far too 
much. You'll probably want to move to the capital after your grandchild passes, then you can be with us. Shouldn't take
more than a century or so, and we'll be able to talk anyway. I think I'll probably hand over the expansion leadership 
role to you. You certainly know it better than I do. 

"No, my hope for these days I am with you is that I can teach you how to interface with the Altar in the temple, let you 
learn to use the spirits that you purge from your village to make things, grow stronger yourself. I'm pretty sure you're
already immortal though, that happens early on and I think you're past that.  Everything seems strange, I know. But don't
worry, you've got big sis here to look after you! Ah here, we are!"

They had already arrived at Zamir's farmyard, which looked exactly as normal. Zamir was a little shocked, he'd left it 
so long ago, except it had only been a day and a half hadn't it? The snow was thick on the ground out here, but Dravox 
just pushed through it like it wasn't there. She didn't even seen slightly winded, though they'd moved at nearly a dead 
run to get here. Zamir couldn't give too much thought to that as he stood in his yard, hands on his knees and struggled 
to get a full breath. It wasn't that he was unfit, no one who worked on a farm every day for more than twenty years was
anything other than in great shape. There wasn't time to let the muscles get slack. However, even the very youngest and 
fittest hunters would have been in a bad way after that run. 

"Oh don't worry. If things go how I expect, the run back will be smooth and easy for you! Your body will start to change
and that's normal, so don't worry about any twinges, or odd feelings. The spirits are remaking you into a better version
of yourself. You're probably wondering why this doesn't happen to everyone. I know I would be. Don't worry, you'll learn
everything, but the process of getting to this point is dangerous. You nearly died, Pretty much anyone else would have 
died." Her face was again in the serious set that she had worn when explaining his progress to the other Gods. "This is
why we've not tried to actively get more of you to grow like we did. It's just so dangerous. We tried for the first
thousand years. So many people died. So many friends, sacrificed. That's why we gave up. Thousands of people, real life
humans, with hopes, dreams, families. All dead. Because I wanted, because _we_ wanted some friends. Someone to share the
load with. I won't lie to you, being in charge is hard work, harder than anything you've ever done. And you so rarely 
get to rest. Ah I see you're getting better, go into the house and make us a flask of tea, we'll be out for a few hours
so let's be ready. Maybe some snacks too." 

Zamir led the way into his house, which itself was barely changed since Umeda had died. The shrine to her was large in 
the corner of the room, a black stone from the river, carved with her sigil and in the priestly runes, the manner of her
death. He looked at it for a moment, wondering what she would say about the first woman he had brought home from a 
festival in twenty years being a god. Probably some witty remark about how he'd found someone nearly as good as her. 

She'd made him promise, back as she lay wasting away, that he would find someone else. Maybe not straight away, she'd 
joked with the smile that still made his heart feel light, but after he'd finished mourning. Maybe someone to help raise
the children. 

Zamir reached for a flint and tinder to light the fire as he remembered that promise. He'd not broken it, not in his own
mind at least. He'd just never stopped mourning her. He'd been content to live his life as well as he could for his 
village and his family, knowing that one day he'd go to his rest and maybe see Umeda again. Now, he might have to 
actually make good on that promise. 

"I see you've already experienced the loss of half your heart. I wish I could say it'll get easier, but it won't. Every 
time is as bad as the first time. That's why we so rarely come out among you all. It hurts too much, we're only human."

That got a laugh from him, a god stood in his house, she had run without stop, she'd talked to the other gods without
words, or even seeing them. She was larger than life in every way. She underestimated herself. 

"Oh you might make light of it. But while we have powers and strength that no one but you could achieve in this age, we
only have the hearts and minds of the people we were born as. We're not smarter than you, we can't handle loss better 
than you. We can't even react to news faster than you. Honestly there's so much I want to, need to tell you. But not 
here."

She lapsed into silence again, sitting on the spare chair at the table, and staring into space while Zamir made tea and 
lit the lanterns around his house. Soon the fire was roaring, the tea brick was shaved, and the leather flask with a 
double wall was ready to have the tea poured in when it was ready. It was brim full with hot water already, so that the
cold leather wouldn't leech away the heat of the tea poured into it.

Dravox started when she smelt the tea brewing. "I don't suppose you can put a little honey in there for me? I love
honey in my tea." she asked, almost plaintively, before going on. "I'm sorry I've been quiet. It's just, a lot to take
in. I've never had to induct someone into godhood before, but I thought it was going well. Then we got here and..." her 
voice trailed off for a moment as she shifted so she was looking directly at the fire. "I grew up in a place like this.
There were no Deadlands back then, no spirits running amok. THe world worked, mostly. It was much bigger than it is now
as well. But I grew up a farmer's daughter. I was always chasing the cats, getting into trouble in the barn, or wandering
through the meadows near my house. My brother, my real brother" she clarified, "He was two years younger than me, and 
always following me around. I thought he was so annoying. Now I'd give nearly anything to have him follow me around 
again. He died an old man, surrounded by children, grand children and great-grandchildren."

She looked up at him, her eyes brimming with the threat of centuries of unshed tears that she angrily dashed away with 
one hand. "The tea smells like it's done. Let's get out there, I'm too melancholy in this house. It's just so cozy and
nice."

Through this whole time, Zamir hadn't said a word. The fear that he would be leaving and never seeing his grandchild, or
spending another enjoyable evening with his friends and family. The emotional shaking was stunning him all over again. 
At least there was respite on the horizon. Decades of respite as a matter of fact. As he listened to Dravox pour her 
heart out, he considered what it would mean for him to live that long. 

Wordlessly he stood up and strapped the flask to his hip, the stiff, boiled leather a comforting and familiar weight. He
stepped from the fire to the pantry, his house being mostly one room which served as kitchen, living room, workshop and
anything else he needed it to be. The wooden floorboards lifted off the ground by poles should have been cold, but he'd
long since bought enough raw wool to cover the underside and then sealed it behind leather so the floorboards were warm
and even in the coldest winters a small fire was all it took to keep his house ice free.

The pantry yielded a few sticks of dried fruit and dried meat pressed together with honey and some crackers. This was 
fairly standard fare around here, it could be made in nearly any weather and lasted well. He wrapped them in a sheet of 
boiled clean and spirit cleansed cloth and put them in his small bag he took when he was going to be out all day, such 
as when he was surveying abroad in the Deadlands and wouldn't be home for lunch.

"Let's head out then. I really want to hear what you've not been saying." his silence of the last quarter glass was 
broken, and the sound of his voice seemed to bring some life back to Dravox's face.

"Yes, let's."

She suited actions to words and once again strode into the chill afternoon air and set a punishing pace toward the edge 
of the farm and the beginnings of the Deadlands. Neither of them spoke, but the air between them felt lighter than it 
had in the house. Almost as though the air in the cozy little house was weighed down with the memories of those they 
had loved and lost. Almost before he was ready for it, they were in the area he'd been cleansing just two days earlier. 

The ground was churned up with the passage of many feet, and the dark brown earth was ringed by a set of interlocking 
octagons, a very strong but almost impossibly expensive formation that must have taken every totem the village had. The
obvious protections and cost in terms of time not claiming other frontiers was lost on Dravox, she didn't even slow as 
she walked straight over the field and into the fringes of the Deadlands beyond. 

"You know," she called back over her shoulder as she stepped between the totems, "We might not be more than human in our
minds, but our bodies definitely are. I've looked closely and I've got to say that the amount of spirits you cleansed
must have been immense. There's no sign of what hit you, so I expect it was just some dirt or a small stone that was
caught up in the spirirt storm."

Zamir had no idea how she'd looked closely, she'd been almost at a jog and her head hadn't deviated from looking directly
ahead the whole time.

Dravox paused outside the purified area for a moment. She closed her eyes, raised her chin and cast about as though
she was scenting something in the air. This gave Zamir time to puff his way to her side, the sweat already starting to 
rise off his sinewy shoulders and back in wisps of steam as the temperature started to fall again. Opening her eyes, 
Dravox pointed toward the river, where a small hill stood in the middle of the plain of the Deadlands that led to the 
river.

"There, there's another ruin inside that hill. In fact it might even be an intact building. I think [commerce god] lived
around here, before it all ended. You'll have to ask him about this place when you get the power to communicate with us!"
She was in high spirits now, and her eyes were bright with excitement instead of sorrow. She led the way into the 
Deadlands, heedless of the spirits that her brash movement stirred up. It went against everything Zamir knew, to just 
loudly stride into a patch of Deadlands waking the spirits and possibly prompting another spirit storm, but he followed
the god as she went onwards.

As he stepped forward, the spirits who had been half-roused, suddenly swarmed toward him, dragging those around them 
along. He flinched as his own personal spirit storm headed toward him, bracing himself against the impact. The impact
that never came. The spirits became quiescent and vanished as they got close to him and he looked around and saw that 
another area the size of a standard field had been cleaned, and there on the edge was Dravox, a huge grin on her face. 

"There's absolutely no doubt about it now" she proclaimed, "You're sucking in the spirits like they're water and you've
not had a drink in a month! You're going to be a God. I can tell you more now. Let's go, but slowly this time. Don't
want to miss any spots." She beckoned him forward and laughed as he timidly stepped toward her, and another wave of
spirit rushed toward him, ignoring the god in their midst altogether.

"That's going to keep happening, so get used to it. Don't worry either, if it looks like you need help, I'll step in, 
and if needed I'll fly you back to the village. Still got to think of a name for there. Zamirpolis sounds good. 
Birthplace of the first cored human in eight thousand years."

The confusion on his face was clear, warring with the mild terror he still experienced when the waves of spirits crashed
into him. 

"Time to give you some education. Just walk slowly next to me, and I'll talk as we go. Firstly, the spirits aren't just 
spirits, they're actually physical and alive. They are following orders given to them before the old Gods were even 
sapient. Humans used to be born with cores, or so we thought. Every child had a nascent core and as they grew, they 
gathered spirits from the food they ate, the water they drank, the animals and monsters they killed and even the air 
they breathed. That was normal, the spirits would eventually coalesce into a full core, right here." She prodded him 
under his breastbone here, exactly where he felt the spirits when he had gathered them before. "That core was the way 
in which everyone grew stronger. The more spirits you captured in your body, the stronger your core and the stronger you
became. There were a few tiers of ability level. Like you, there were novices, fresh into their cores and they only 
used the spirits unconsciously. They strengthened their bones, improved their blood flow, toughened skin, improved muscles
protected against disease. Everything that has been happening to you for decades I suspect."

She paused here, then thoughtfully continued "I'll also let you know that your wife's death was not your fault, it had
nothing to do with who or what you are. There was no way you could have saved her then, and also there was no way any of
us could save her either. The spirits don't see me any more, they won't come to me. My core is complete, there's nothing
for them to do, I can only interact with them through the altars and the mats that we make in the Temples. In fact
because of your unique make up, it's basically impossible that you could have even brought the spirits that infected her
home with you, if you've been worrying about that. It must have been something she picked up when away from you."

As they'd been walking, they'd covered nearly half the distance between the farm and the distant hillock. Zamir was 
fairly certain this was the farthest he'd been from the place he was born in his whole life. The waves of spirits
were still somewhat shocking, but the flinch reflex had been pushed aside as he hung on Dravox's every word. 

"But back to the matter at hand. Everyone had a core, so we thought that we were just born with them, but we now think
that somehow the old Gods put them in people. Just after the war, things were dire. Children dead before they had spent
a night in the world, because we didn't think to protect them against the very thing that made us strong. We raided the 
old gods stores of knowledge. They kept so, so much secret from us all. Then they ruled us according to rules that none
of us knew. It was a harsh time. Still we were young and we had hope. So we set about making a society where everyone 
was protected against the spirits, without a core to save them. We tried so hard to get new cores, people tried to 
donate their cores to their children, invariably killing them both in the process."

"We thought it was always going to be like that, then you came along. We set up the frontier farmers to be those who 
had an affinity for the spirits, like you. But if we didn't purge you, then eventually the spirits suddenly turned on 
you. But somehow, you've made a core. You can grow like we used to. You'll get stronger, faster, stop aging. You'll 
start to be able to command the spirits around you when your core undergoes its second refinement. That's what I'm 
hoping this trip will do for you. I can already feel your strength growing. You're right on the brink of the second tier.
I've never known anyone grow this fast, but I guess you were on the cusp of coalescence when you absorbed the ruin. The
real question is where and how you got the core. But I am hopeful we can communicate with it when you're in the second
tier. What we used to call apprentice. You might get some messages about... Wait, you can read can't you?"

Zamir looked at the god next to him in confusion, "I know enough script to wite my name and that of my family, that's 
always been enough for me."

"Well then, it looks like the good Sitara has a job waiting for her over the next year or so. Talking to your core will 
always take the form of text only you can see. So you need to read if you're going to grow. For now you can copy what
you see to some paper and one of us will talk you through what to do. I hope you're the start of a new wave of cored. We
could cleanse this world with a population of people like you. This world is so much bigger than you know. There's an 
ocean where the sun rises in the mornings, it'd take you a year to walk there, over mountains, through deserts, past 
jungles. You've never seen those either. But you can, you will. When we cleanse this world and get it back from the 
spirits."

By this point in time the sun was nearly behind the mountains again and the temperature had dropped sharply. Zamir was 
aware of it, but strangely didn't feel it. Unless he missed his guess, there would be more snow before morning. The hill
was looming closely and the rough shape of it was now more a squarish lump of black against the skies. With casual 
strength, Dravox thrust a hand into the hillside and pulled. Whole chunks of the hillside sloughed off the now obvious 
wall of a ruin which was as black and dense with spirits as the few blocks that had been exposed when Zamir cleansed the
ruin before. As soon as they were exposed, the blocks began streaming toward Zamir, a dense wave that almost lifted him 
off his feet, the sensation of the spirits settling in his chest, absent since his last core growth, began again. 

"I can feel it, it's coming!" he cried, sitting down sharply on the ground almost instinctively. He assumed the pose of
the cleansing ritual and braced himself against the black tide. Already the hillside around the hole was collapsing as 
the spirits that had held the ruin together against the ravages of time flocked to him. He felt a hand at his side as
Dravox liberated the tea and food and poured herself a cup, sitting down on pile of earth that crumbled beneath her as 
the block of spirits that was hidden inside it joined its brethren in streaming towards the seated man in front of her.
With preternatural grace, and a muffled curse around a mouthful of pemmican, the god caught herself and the tea before 
it could spill. she then sat on the floor after looking at it suspiciously for a few seconds. There was nothing for her 
to do now but wait and watch. 

Zamir felt different than he bad before. The spirits did not try to cover him or choke him this time, instead they 
approached him in haste and then entered him in an orderly queue, directly over the core he could feel swelling inside 
him. It was with a measure of detachment that he noticed his body reacting to the influx. Not all the spirits were going
to his core, he could feel that each breath he took was deeper, clearer, than the one before. The darkening world appeared
to be getting lighter and sharper, his aging eyes suddenly picking out the details lost to the years and finding more 
light than there should have been at this time of day. With a feint wave of pain, quickly suppressed, the finger he'd 
broken in a farming accident ten years ago, that never healed straight, suddenly ripped itself into alighnment. The sharp
pain replaced by a numbness that lasted only a few seconds and that took an ache he had long since stopped noticing with 
it when it vanished.

All of this was a background to the incredible pressure building in his chest as the last of the ruin was sucked out 
of its shell like a water snail in the western soup, the lack of which Pavel lamented when he was in his cups. 

Then, just as before, the pressure built to a crescendo, followed by a sudden release as the core collapsed in on itself.
This time Zamir didn't lose consciousness, but the stabbiing headache inside his head made him wish he had. For several 
heartbeats it persisted before vanishing again. A row of shockingly blue priests script sat in his vision, he turned 
to Dravox with wonder in his eyes. The moment he laid eyes on her though, there was a sudden change in the script. The 
colour flipped to red and suddenly the spirits from very far out indeed were rushing to him. 

THe wonder turned to fear as he stared at Dravox, only to see it mirrored there. She tried to reach for him, but the 
spirits formed an almost solid wall and pushed her hand aside. Then with a feeling like everyone in the village was 
behind him, pushing him in the back, the spirits lifted his seated form into the air and launched it toward the horizon.
He looked down before he wa taken too far away and saw Dravox fighting what looked like a very localised spirit storm 
that stymied her every move and kept her contained. 

With his hand still out-stretched to the god he had only met that morning, Zamir was taken away from the only land he 
had ever known, and into the unknown.