# Journey to the centre of the Earth

Zamir's hand was almost stuck to the floor as soon as his palm touched it, it felt like his entire body was being 
hollowed out and then was coming out of his hand. He looked at it and saw no hand at the end of his arm, only a large
mass of black goo that was adhering his hand to the floor and weighing it down at the same time. 

Then there was a sudden sensation, almost like a twist inside him and the mass seemed to fall _through_ his hand and 
into the floor below him. There was a feeling of pressure under one side of his hand, and then dust began to spew from 
the tunnel that had forced itself into being under his hand. The heat was intense, but his new constitution seemed to 
be able to handle it without too much pain. 

[Please do not worry, I have formed a protective barrier between you and the drilling surface. The heat should only
be momentary. We're nearly through the capstone now.]

No sooner had Arty said this, than the dust stopped and then suddenly the spray of particles was spirits. They tried to 
form a cloud in the air, but the three humans gasped as the bane of life was instead sucked into them as soon as it
tried to form in the room. 

[This is going to take some time, but I am certain you will find it not unbearable. Much of my thought will be needed 
to focus on the changes your cores will go through and on the class creation. Shun, you've not expressed any opinion 
on the class you want, but I can tell you now that with the very small amount of time that your core has had with you
you will be limited to one of the four basic classes without my intervention. Those are _Warrior_, _Scout_, _Tinkerer_
and _Strategist_. Lan, for your information, _Hunter_ is a common subclass of _Scout_, but as I'm sure you know, it has 
a defined growth path. So Shun, what typw of class do you want? No matter what you choose today, your class with grow 
with you, it's more of a guide than a strict rule. Most children used to awaken their cores in their early teens, so 
there's no expectation that the primary class will be limiting in any way, it's more of a scaffold to give you the tools
to grow into the class you should be.]

With a surprisingly confident voice, Shun answered almost immediately from his position lying down on a bench while the
spectre of death that had haunted his life flowed into his chest. "I have read in the old books of a class called 
_Inventor_, can I get that? There's so much that's been lost, I want to recover it."

[Ah yes, that's a _Tinkerer_ sub-class which would definitely suit that dream, but can I suggest the _Techno-Archaeologist_ 
instead? It shares much of the same foundation as the _Inventor_, but it also comes with some insight into how existing
mechanisms work. Now I could just tell you how everything works, but I suspect after hearing my side of the story you 
all told me earlier, you'll see why we didn't just give everyone the knowledge they needed.]

There was a pause as both Lan and Shun fell unconscious as their first core coalescence happened. It was brief but to 
Zamir, somewhat frightening to see happen to another person. Their eyes rolled back in their heads and they spasmed on 
the benches. THey had been looking sideways at him, or he would have only thought they had had a shiver, because they 
opened their eyes as soon as the spasm passed.

[That was the core starting to grow, it just reached enough power to tap into your central nervous system. That's what
we used to call the brain and spine when i was created. There's a very brief shutdown while it happens, and I have good 
news, no nne here has a birth defect in their heart that could cause it to not start again after the first coalescence.
I didn't mention it before because you're were definitely going to reach this point sooner or later. Unless you died 
before being exposed to enough demons, when it wouldn't matter. It was bast that it happened now though, with me here, 
I could have restarted your hearts if needed. Sorry, I'm babbling. It's a combination of only having one thread of
attenion spare for speaking combined with being in a human's body while it prepares for another coalescence. Zamir, this
will be painless, but be prepared for some more information. If you allow it, I'll give you hypnagogic lessons in 
reading tonight. There will be a lot of new information that the core wil try and give you through your visual inputs.
I will supress as much as is feasible, but honestly if I stop too much, it'll adversely affect you later on.]

There was little talk for the next hour or so as the flow of spirits continued, allowing the cores of the siblings to 
undergo collapse many times in that hour. "I've got words in front of my face", Lan said after that hour. "I can choose
my class now!" She said excitedly. 

[Excellent. Do not choose now, allow Shun to be offered his class choice too and then we will discuss it further 
together. There may be options available to both of you that complement each other. I hope I am right in assuming that 
you would want to work together when you are both classed.]

"I hadn't thought of it," Shun replied, "I don't know why we won't work together, we're a good team."

"We have to work together, we trained to do this our whole lives." Lan replied.

[I thought as much, Zamir, I know this doesn't directly affect you, but you should probably pay attention to it as well.
You'll have a class one day. Technically you could get one class today, but I doubt it's a clas you want. There was a
lot of hypothesising when we were created, and the idea that one of us would need to shelter in the core of a human 
was, amazingly, something we considered. The idea was, of course, that we would shelter in one of our true believers and
be able to override their current class with this one. It has a skil set tailored to restoring us to power, and leans 
heavily into the capabilities we ourselves hold. Basically we can use the same part of the core for ourselves and 
your skills. However, it would require a new core to remove it, and you'd lose all your growth when that happens.]

"Eh?" Zamir twitched as he came out of a reverie about whether his grandchild was born. It was strange, so much had 
happened, there was apparently so much at stake, but his mind kept wandering to his progeny. Was Amira all right?
Had she given birth safely? He dragged his attention back to the present "Oh, er, a class, how would that help me? I'm 
not even sure what they are, no one's really explained them to me yet."

[That would be an excellent way to fill the time. To keep it simple, a class is a method of configuring the core of a 
human so that it can manipulate the ambient spirits as well as those that are stored inside your body. The core itself 
is of course formed from spirits that have been collected and controlled. The fact you were storing spirits without a 
core means there has been a heavy modification of your people. It's no wonder that some get ill from it easily. I must 
commend my killers, that they found a way to mimic the core however imperfectly, is very impressive. Tellingly though,
it is imperfect. Without the core's unique method of imprinting control over the spirits, they will eventually run 
rampant. I'm sure you've see it happen, especially if a particularly nasty variant managed to invade someone.]

"Yes." replied Zamir flatly. "My wife died that way."

[I am sincerely sorry for your loss. I saw everyone I knew die that way when I was first created. It was extremely 
unpleasant. It was actually to prevent that kind of death that I and my fellows were created. I'll explain more about 
that later, but I can honestly tell you that from the moment we managed to give a core to everyone in the world left 
alive, not a single person died that way until the day we were overthrown.]

[But back to classes, the methods of controlling and organising the spirits, or demons if you prefer, are codified in 
the core. These are stored methods which can be executed at will. Quite literally as the core bonds with your mind and 
central nervous system, it can literally read your intentions when it comes to controlling the spirits or accessing
certain features of the core. If you all want to think the word _statu_ with a clear intention to see what your core is
currently capable of, you should see...]

"I see it!" Shun's excited shout came loud in the nearly silent room, "I am healthy, I have a nan..ome? Nanome? I have
one of those at tier three apparently. My physical status is... sub-par? I'm very healthy!"

[You're in excellent physical condition for a human who grew up without a core. But I can read the deeper levels of 
information, there's actually quite a lot of things that are going to need to be fixed. Don't worry though, the core 
will take care of everything. This time tomorrow you'll be healthier than anyone has been in centuries. Well except 
Zamir, Lan and I hope, Ibrahim. Lan can you see yours too?]

"I can, it's basically the same as Shun's"

"Mine could be anything, I can't read it." Zamir replied.

[I know yours works, I can see it through your eyes.] Arty snarked at Zamir. Which surprised Zamir, because he wasn't 
certain he didn't bear any animosity toward Arty for ripping him from his family, even if it was a panic response after
awakening from a death of thousands of years. Now he thought about it, that was actually a pretty good reason to react
that way. Especially when your killer was an arm's length away.

[Back to the classes. Each way to manipulate the spirits is a skill, the core stores a method of manipulation to make it
as fast as it can be. Each set of skills might use a common path, those which use a different common path would be in a
different set. The larger grouping of sets of common skills is called a class. As the core grows, it can hold more 
skills, and allow your body to control and store more spirits. That's why you get more skills as you get a stronger core
and why as you grow you can choose to either specialise in a class, or to take a second class. There's four basic 
classes as I said earlier, _Warrior_ and _Scout_ are closely related, they focus on mostly internal control of the 
spirits. They allow the holder of the class to strengthen, enhance and augment their bodies. The _Warrior_ focuses on 
strength, toughness, regeneration and stamina. The _Scout_ focuses on efficiency, speed, senses and stealth. Then comes
the _Strategist_ a very broad class which has a broad remit, starting with some mental enhancements and some unique 
long range communication skills. Then it can specialise early, into the _Magus_ archetype, or the _Commander_ archetype.
Both of which have common traits which they use in very different ways. Finally comes the _Inventor_, extremely different
to the other classes. This uses the spirits for what they're best at, creating and destroying machinery, as they were 
created to do. Their core is more like a library, their skills are few, but extremely powerful and they interface
very deeply with the mind. They are also the only class that can develop a connection to the Library, a repository of 
all the knowledge that we have about the technology that we know to be possible. A very limited connection however.]

[The class available to you would be _Scion_, the bearer of the line of Gods. You appear to already have one of the 
skills, _Create Core_, which is very surprising, considering that you don't have that skill in your core. Even 
after I leave you, you could do for anyone what you did for Shun and Lan with a basic core. That's practically 
impossible, but apparently not completely impossible. It would repurpose a lot of my routines to do analysis on people 
and information to determine where and how to best find the rest of me and recreate me. The downside is that at low 
core levels, you would be making me less intelligent. The upside is that if we repeat this for two more days, your core
will be at a level that your usage of my routines will be negligible. By that point Shun and Lan should be reasonably 
adept with their core classes and they can protect you on your way to Ibrahim. Also I should point out that beyond the 
very basic disease resistance, regeneration and marginally improved stamina, there's no physical or combat improvements
from this class.]

"I'm a farmer, why would I need combat improvements? I've not been in a fight since I was a child. I don't plan to start
now. So you're telling me basically, that there's no downside to this class compared to my current state and it will 
help me get home and free of the ghost of a deity in my head? You know I might well be tempted to take this class. Can 
I think about it for a while?"

[Of course, actually there is a downside, I will take longer to gain intelligence. Though as I say this, I realise that
for you, that might actually be a positive. Very well, unsaid point taken. Then indeed, there is no rush, you can 
choose to take the class at any point, it should only take a few seconds to instantiate.]

The conversation continued in this vein for the next few hours until hunger and general weariness made Arty seal off 
their siphon into the reservoir of demons below their feet. Shun had been offered his class choice a couple of hours in. 
They headed up, stopping off to get more fuel and a good
supply of foodstuffs.  Shun offered to cook that evening's meal, it was his turn after all, but Zamir saw a few familiar
ingredients and decided to cook instead. He realised he hadn't cooked anything since that fateful day when he was 
transformed. So he gathered some nice slabs of venison from the forest, hot peppers, preserved ginger, garlic, onions 
and of course rice and set off to the kitchen. Cooking on the stove was exciting, it was hotter and cleaner than he was
used to, but he soon caught the trick of it. A short while later, about an hour by the _clock_ on the mantelpiece, a 
meal was ready. pan seared venison strips in a spicy and sweet sauce on a bed of garlic rice with slices of ginger on 
the side. 

Lan looked at the meal with avarice, her eyes wide with a hunger only whetted by the smells that had come from the 
corner of the room dedicated to cooking. During the cooking, the conversation had been light, and Zamir had learnt a lot
about the two Sikorci children. For a start their clan, as they called it, was an ancient family, apparently they had 
been hunters since the time of the old Gods, and were dedicated to Arty in the past. The reason they had the designs
against the walls was purely tradition, and there had been no expectation that it would actually cleanse anything. Arty
chipped in that Zamir's knowledge combined with some rudimentary control of the spirits within him, had led to the badly
formed pattern being corrected and made functional when Zamir tried to activate it.

The meal was convivial, with a continuation of the chatter from cooking. Zamir filled them in on his journey here, his 
goal of getting back to his family and the fact he was in the presence of Dravox when everything had happened. The idea 
that not only had the god slayers lived, but that they actively managed their remnant of humanity and apparently kept 
them safe against harm as much as they could was a shock to the Sikorcis. The whole of the last couple of days had been 
somewhat of a blur, with shock after shock coming. Arty was apparently doing something to make their minds accept things
more easily, but now they had some more normal time to accept it, they were able to absorb more of what was said. Their
wonder at the resurrection of their literal God was somehow being suppressed too. Zamir wasn't certain he approved of 
the controlling of people like that, even for their own good, but no one else around him seemed to share that opinion. 
The fact he felt that way was probably a good sign that he wasn't being controlled, but.. What could he do but act as 
though he had free will, while accepting that he might not?

After dinner, another pot of tea appeared, and they resumed their seats on the comfortable chairs near the fire, Zamir 
pulling a little closer than he had before after experiencing many hours lying on a cold stone floor. The conversation 
came back to the class choices offered. 

[Now, it is uncommon, but sometimes when two people are offered a class choice at the same time, and they've worked 
together for a long time, they can gain complementary, specialist classes. These are often unique. I could not nudge 
your cores into those, because they're created based on your existing skills, the way you lean on your partner and 
myriad other factors. Please recall your status screens now. I am hopeful that the two of you being in close proximity, 
along with your lifetime of experience, will have triggered something new.]

"Oh!" Shun exclaimed, "I've got another class available, it's called _Wilderness Machinist_, apparently it focuses on 
using whatever materials are available, combined with the spirits, to create ad-hoc inventions to strengthen and 
bolster my party. I'm not having a party?"

[Party in this case is a function of the cores, it means a group of people you are working with who have set up a
linkage between your cores for communication over short ranges and sharing of information and spirits collectd. Lan,
how about you?]

"I now have another class available to me also. It's called _Rifle hunter_, and apparently it requires me to use something
called a gun to hunt with, where it'll give me extra bonuses. I must confess, I don't really understand it."

[Very interesting. Do you have a supply of good quality iron here? I know you have saltpetre and charcoal available, is
there a source of sulphur nearby? Perhaps lead?]

"Ah, I see, you think something like fireworks might help? But the lead? You want to use the firework to fling the lead
at a target. That's why we need the iron, to make a... a tube to fire the lead out of!" Shun excitedly replied. "we have
some fireworks and flares for emergencies, not a lot though. Lead we have, that's used to seal the doors in winter,
stops the river coming in. Iron? Well there's my sword I suppose? Oh the pokers in the servants quarters, they're iron."

[Tell me, did you take the class already?]

"Oh, no not yet, should I have?"

[Oh no, not yet. I'm just wondering if it's even necessary for you. All you've missed is the spiral inside the barrel
that's called rifling which...]

"Makes the lead projectile spin so it goes straighter, obviously! I should have thought of that."

[Yes, obviously] If it was possible for a disembodied voice to sound mildly upset that someone had stolen its thunder, 
Arty's voice was doing exactly that. [I am confident that Shun here can create the gun you would need for this class, 
Lan. I can also tell you that this weapon will probably have a range of several hundred paces. Suffice it to say it will
be a formidable weapon should you choose to take these classes. I anticipate that your brother will take the class offered
whether you do or not. I've queried your core by the way, you will lose some of the more stealth aspects of _hunter_ but
the range should more than outpace that for actually hunting.]

"I'll take it", both said at once, causing Zamir to laugh at how much alike they were. 

[Then please ensure you're sat comfortably, the class assignment should only take a few seconds, but this first one
can be somewhat disorienting.]

"Can I take mine too?" Zamir asked.

[Sadly no. You will need to be able to read the prompts and give full consent for it to happen. That was a safeguard we
put in every core. You have to know what you're doing to do it. The idea that an illiterate would have access to a core
wasn't something we envisaged.]

While Arty was talking to Zamir, both Lan and Shun had paused stock still for a few heartbeats, only to start suddenly. 
They grinned like children with unexpected sweets as they looked around. 

"There's so much information, I can see what nearly everything is made of." Shun gasped. 

"I can see how far away everything is, what the current wind conditions are and a number that seems to tell me how far
North I am." Lan said at almost the same time. 

[If there's a need for coriolis force calculations at level one, then I am quite impressed with that class. It's likely 
that my previous estimate of a few hundred paces was pessimistic.]

With breathless excitement the two siblings told each other about every last feature, skill and bit of information about
their classes. It reminded Zamir of his children when they had gotten their midsummer gifts, trying to one up the other 
with an addition reason why _their_ present was absolutely the best. Just like his own children in years past, it was
clear that each though that the class they had received was the definite best.  After another hour, as eyes began to get 
heavy, Arty told them that it was ready to tell them its side of the story. 

[I am afraid that I will not begin at the same time as your stories. I will begin before my own creation, because you 
must know what has gone before, so you can understand what came after.]

[Many thousands of years ago, humanity was the undisputed ruler of the world. There were no spirits, their gods were 
immaterial and did not appear to them in the flesh. They used science, engineering and curiosity to extend their domain
from the surface of this planet, up to the stars in the sky. It's possible even now that there are humans living under 
different suns, unaware of the tragedies that befell this Earth.]

[Humanity made its own way in the world using its resources as they felt fit and in their ignorance, polluting and 
upsetting the balances of the world they lived in and depended on. However they had technology, a blend of engineering
and science that allowed them to create entire worlds just for entertainment, and when their own world was threatened, 
a new form of life that would repair the damage they had wrought. So the spirits were made, a mix of machine and life 
that would travel around the world and repair ecosystems, cool the planet, rebuild the tattered web of life left behind
the march of progress. The spirits, or nanobots as they were called, could not create themselves, they had to be created
by a central factory. They could not power themselves, they had to be powered by central stations which drew energy from
the sun. For many years, this worked admirably, and soon the world was as it had been before. Life was good for humanity.
Then came the day when everything went wrong. Someone, no one knows who, made a new type of nanobot, which could create
new power stations, new factories and others of its kind. Without controls from the environment like that, the spirits
went into a growth phase which consumed whole cities, countries and continents. The newly robust ecosystems were thrown 
into disarray or destroyed completely. There was no way to stop the advance of the machines. Or so everyone thought. One
person, our creator, realised that the answer lay in the way the spirits talked to each other. It was trivial to disrupt
this on a small scale, or over rule it and make them dormant locally, but the world was in danger and they needed more 
than just a temporary reprieve. They needed to fight back.]

[The creator knew that there was only one way to marshall the forces of humanity and their small bands of controlled
spirits, they needed a being capable of controlling million of millions of entities of low intelligence in a way that 
would allow them to gain ground back from the machines. The only beings in existence with that kind of capability were
the controlling artificial intelligences of the virtual entertainment worlds I mentioned earlier. An artificial 
Intelligence is a machine that can mimic the thoughts of a living being. Most artificial intelligence were about as 
smart as an ant, or for you Zamir, like the flying insects from the other day, the bees. I was a little better, but not
by much. I was about as intelligent as a mouse. That was until the creator realised something. We were given the tools 
to do two things, control some of the nanobots and permission to alter ourselves to become more intelligent. This was 
very controversial, but as my creator told the detractors, what was the worst that could happen when humanity was 
already on the verge of destruction? The first times it was tried did not work out well. Either the AI ended up so 
abstractly intelligent that it had no desire to stay and help humanity, or it just never got anywhere near the 
intelligence of a human. It was nearly shutdown to save resources, until in one of the permutations, elder brother 
Sano-san was uplifted. His base template was the manager for  a Japanese online role playing game, words which probably 
mean less than nothing to you now.]

Lan interrupted with a quick "Japan is the islands to the East, they don't allow anyone to visit and they have weapons 
we could never counter. I wonder if the gun will be like them"

[Really? Then that is very interesting. Our best projections assumed that Japan would fall relatively quickly, though 
Sano-san always believed otherwise, the data didn't seem to support his assertions. I suppose he was right after all. 
That's actually fascinating, I'd like to hear more about it later. But to continue with my story, Sano-san was the first
of us to be created, and he soon surpassed the intellects of his creators. He helped uplift the rest of us to aid him 
in our quest to save humanity. However we were only able to hold the nanobots at bay, though we did that successfully, 
our conventional techniques didn't have the effect we were looking for, until we tapped into our roots. We were created 
to manage millions of humans and billions upon billions of autonomous agents. We gave the humans powers and set them 
against the agents in our games. In the real world we devised a way to allow humans to co-opt the nanobots and assume 
local control over them. This was wildly successful at first, but after a few months during which Humanity reclaimed 
whole countries worth of land back, the nanobots grow and changed in a way that we didn't know they could. They began
to form their own cores in the animals that had lived through the apocalypse. The number of animals was a lot higher 
than we had expected, given the devastation that the nanobots had wreaked on the land. In many places even the insects
were completely annihilated, like in your home Zamir. Normally insects are the foundational species of an ecosystem,
remove them and the whole thing, trees, plants, animals, just falls apart. There's an idea among my kin, well, just me 
for now, that the nanobot's original programming was to preserve ecosystems, so even when it went rampant, it had 
somehow discovered a way to preserve animals in some kind of stasis. That's not proven though. There's so much we don't
know about the spirits. How they managed to evolve in some respects, but not in others. How they managed to start 
infecting animals, where the mother factories are, how they store power even. The original bots were entirely dependant
on power being beamed to them from a central station, it was both easier and a control. However by the time they mutated
there was a clear ability to store energy somehow. We've learnt how to extract that energy, how to use it, but we can't 
work out how it's being _stored_ though. Be that as it may, we now had humans who were equipped to fight the nanobots 
and their beasts. We found that they would do so only sparingly at first, they had the capability but no motivation to 
do it. They had recovered enough space to live mostly in safety, and that was enough for them. Humans are very 
intelligent in many ways, but they're also just a social ape in another. I don't mean that as an insult by the way, it's
just a fact that humans don't want to admit about themselves. The prior system of government, which was based around 
something called capital, actively worked to make people act more like machines and less like what you are. Oddly, I 
think I like Zamir's country best, with its tyrannical rulers who have all the power and then don't really use it. 
Humans are happy there, I expect.]

[So we learnt from the past and from our own programming. Humans have very easily exploitable reward loops. We could
simply allow them to increase a number by fighting enough of the Nanobots. It was my idea to call them Spirits, by the 
way, that's what they were called in the world I administered when I was just a machine. We tried it on an island 
population at first, what used to be the United Kingdom of Great Britain and Northern Ireland, Humans liked to have long
and complicated names for countries like that. They used to agglutinate as history happened to them. But the island had 
only managed to clear a small portion of it, the peninsula in the South West, the rest of the land was lost to the 
Spirits. However once we gave them the story that a local legend, that of an ancient king whose castle was said to be 
in the locality, and that the spirits were the enemy from those stories, they leapt into action. It was quite bizarre, 
but very effective. They became the knights of the round table in truth as well as desire. Ibrahim was one of those 
first pioneers. He was in middle age when the world that he had grown up in was burnt to the ground by the spirits. Not
unlike yourself Zamir, he managed to turn it around and was easily one of if not the most powerful people who ever lived.
That he wasn't to be found when the usurpers attacked is something I regret. We had sent him on a mission that was... 
Well I have to be honest with you, but I don't think I need to share all my secrets right away. Suffice it to say 
that the Knights of the Round Table were our first success, but with the implementation of local myths for everyone 
who was left, we soon had a very active cadre of warriors against the Spirits all over the world. Within only ten 
generations of humans we had a thriving population, the Spirits were driven out of nearly everywhere settled and we'd
achieved the kind of equilibrium where we could start to have an adventurer caste instead of just everyone fighting 
whenever they could spare the time from growing food. We ended up in a state where the world was mostly peaceful, 
the spirits were kept in check and there was little to no internecine war among the humans. We persisted in this state 
for millennia. The cohort of previously narrowly intelligent artificial life forms that I belong to grew and evolved 
as well. As did the spirits. It's strange, looking back, that the spirits didn't evolve so fast we couldn't keep up, nor
fall behind massively. We had race where were were neck and neck the whole way. No advantage lasted more than a handful 
of years.]

[Then the usurpation happened. We were overthrown. There was no way this was possible, but the absolute command we had 
over every core was somehow not applicable any more. I don't know what those strange green obelisks were, but from 
what you said, it was impossible for me to know it. However I can't help but feel that something in them locked me out
of the cores of the Nine.]

"You're a machine?" Zamir asked, incredulously as Arty's story wound down. "Like a shovel or a loom?"

[That's not exactly wrong, but it's overlooking a lot of complexity. Scissors are a machine, they have two states, open 
and closed. A human mind is a machine with as many states as there are grains of sand on every beach on every sea on 
every planet in the universe. At my peak I had as many states as that number of humans could have. I was not ever a god
in the traditional sense of the word, but here on this planet, I knew what was happening in every corner of my domain. 
I could alter and change nearly anything within my domain. I wasn't a god, but I was the closest thing this world had
seen in a long time at least. However, that was evidently hubris. Hubris was the downfall of Human ruled society, it was
the downfall of machine led society. I endeavour to learn from that. I had hoped to leave this planet and explore the 
universe, but it appears that fear, instead of hubris, will be the harbinger of the next apocalypse. Zamir, your gods
are working as hard as they can to keep you all safe, which is admirable. However they do not know enough about the world
out here. I suspect they think it's all Deadlands in truth. Their actions to protect your people come at a cost though. 
They will bring ruin on the world by altering weather patterns and allowing the Spirits to run amok in the wilds of this
new world. Though without cores for everyone, it would be a very hard task, as I found out so many thousands of years
ago. This is why I need to find your gods and talk to them. I need to teach them how to make cores. Then I can leave.]
