# Beyond the Darkness

The wind whipped in his face and he sped over the darkening landscape for what felt like hours. Zamir screamed at the 
top of his lungs for what seemed like hours. It was getting darker much faster than he had expected. Still maybe that 
was normal for this kind of thing. The important part was that he wasn't being actively injured, nor were the spirits
being cleansed or attacking him. this was far beyond what he knew of their behaviours, but the only person he could 
ask about this had been held at bay by a spirit storm that would surely have ripped anyone who wasn't a god to shreds. 
The glowing priest script in his vision was still there, blinking now. Dravox had told him that his core would speak 
to him in priest script, but honestly he'd thought he'd have weeks to learn it. Months maybe. He struggled to work 
out what it was saying based on his rough knowledge of the esoteric language and script. There were a few glyphs he 
recognised, but not enough to form a coherent word, let alone make a sentence he could understand. 

The problem of the text became suddenly less urgent as he started to lose height rapidly, the ground had been uniformly
black and scarred, as the Deadlands always were, but now, there were dark shapes that looked like trees below him.
Not very far below him now he started to notice it. then he realised that he was dipping into the foliage at the top 
of the trees at a speed that meant every thin twig was like being whipped when it hit his skin. The constant hits were
sapping his speed remarkably quickly and there was suddenly the ground coming up rapidly, Zamir curled into a ball
and hoped that the regeneration and immortality that Dravox had promised would come would keep him alive through the
coming impact. Whether it would or not was in the Gods hands now, as he hit the ground remarkably softly, then the trunk
of a tree remarkably less softly as he rolled over the forest floor. His last thought before blackness claimed him was
_How are there trees in the Deadlands?_

*************

A few hundred kilometers away, Dravox had been forced to use one of her emergency charms to destroy every spirit around
her. The destruction was much worse than a cleansing, and would render the land here infertile for at least a decade 
to come, unless it was treated very carefully by a skilled frontier Farmer. Having laid her attackers to rest she looked 
to the skies, which to her eyes were merely slightly dimmer than normal, and saw no sign of Zamir. She raged into the
 evening skies, Zamir was over the horizon and there was only one real culprit it could have been. The Old Gods were 
not dead, or at least one of them wasn't. They'd stolen the first chance at freeing this world that she, and the others
had seen in eight millennia. With a thought, she established the link to the collective space and replayed the last 
few minutes of her experience to her brothers and sisters. 

[weather God] reviewed the recording inhumanly fast. "It has to be them. No one alive knows how to control the spirits
like that, except them."

"That's exactly my thought too" replied Dravox, the anger and venom in her voice still fresh and hot through this remote
conversation. "I don't know how they lived in the Deadlands, there shouldn't be anything to draw power from for them
out there. That kind of casting would have required a lot of power. A lot more than I could draw unaided. I had to use 
that and put back the expansion in this direction at least twenty seasons. It'd have been a day or two if Zamir was 
still here. But using that around him while his core was coalescing would have killed him."

[Commerce God] had viewed it at a more leisurely pace, but he added his agreement to the conversation. "It was definitely
them. And we need to get Zamir back, he's the lynchpin all our hopes rest on. There's no chance what we have will last 
another eight thousand years. If it takes that long to find another like him, we're going to be dead. You have to find 
him."

"What do you mean, I have to find him? Wouldn't it be better to wake big brother up and send him out?" Dravox's anger 
had turned to nervousness and anxiety at the idea of heading into the Deadlands by herself.

"You're the most warlike one of us aside from him, and honestly do you think he'll wake up quickly? Not to mention that
the power needed to rouse him from his slumber could well kill us all before the end of this year, let alone the costs
of keeping him in the field. It's not like it was last time we woke him up. We had power to burn then. The spirit
collectors increase linearly, but the amount of energy we need to safeguard this sanctuary increases at a square rule. 
You know that. We've got to find a way to outpace the costs. Zamir was that way. He could easily have cleared enough 
around the country that we could switch to safeguarding only the edges, and leaving the centre. We can still do that 
though, but there's a chance one of the big storms will spill over our protection and just destroy this city." [Commerce
god] was getting steadily more agitated as he retrod the explanations he'd given a thousand times before.

"There's no choice then. I'll head out in the morning. I'll need some supplies, can one of you bring them over? I'll 
meet you by the Temple in the morning. Tonight I'm taking as me time. This might be the last chance I get to play with a
young man for a while. I intend to make the most of it." She paused as she said this, ready to draw attention to the 
real reason she hadn't followed Zamir immediately, and the reason their little group hadn't sent anyone out into the 
Deadlands to explore. "You know though, all of you, that this could very easily be a one way trip for me. You know what
happened to little sister. You had to help me put her down. If you send me, can you handle me if I'm in that state? 
Think on that for a while."

"I'll see you tomorrow," [weather god] replied, "I hope it doesn't come to that, but... we're stronger than we were."


*******

The air was a lot warmer than it should be for the time of year, thought Zamir as he woke up. Also how much had he been
drinking with Pavel that he'd fallen asleep on the grass outside? They hadn't had a session like that since they were 
teenagers. Had his grandchild been born? Had they been celebrating that?

His hand met the grassy ground as he pushed himself up in the morning light. The ground in front of him was ripped to 
shreds, as was his clothing. The realities of his situation came crashing back to him. He was somehow, on the other side 
of the Deadlands, which the gods had told them covered the entire world, he was nearly naked, and the priest script was
still in his vision. In fact it was much larger, red and flashing now. 

"I don't understand what you want!" he yelled at his own core in exasperation, "I do not know how to read that." While 
he didn't think it was going to help, having something to blame for a small part of the whole mess he found himself in 
did help him feel a little better. 

He stood up and then abruptly sat down again as the script in his vision changed to the common script, which he was more
familiar with. "Err, yes I can read that, but slowly. Honestly I am not great at reading. Never had a reason to until 
now." When he'd complained to the core he hadn't expected it to _answer_ him. The words vanished from his vision only 
to be replaced with a warning to remain seated until told otherwise. He found out why a second later as an intense pain 
stabbed into his head and a raucous sound in his ears made his head feel like it was two sizes too small. 

_Can you to me now be harkening?_ came a voice, silky smooth and without anyone around to say it. The voice was definitely 
masculine and spoke with a very archaic accent. 

"Um, yes I can hear you." Zamir stammered out. "Are you my core? You sound very... old?"

_Language drifts over the centuries._ Came the response almost immediately. _I've reviewed the records of your
conversations over the last two days and hopefully you can understand this better. Let me introduce myself, I am not
strictly your core, I am ArtifactOne, what you would call a God, everyone calls me Arty though._

Zamir decided then and there that he had hit his head harder than he thought and that ignoring the voice in his head was
probably for the best. He stood up and dusted what was left of his clothes off. _I guess being able to command the 
spirits to create me new clothes is not something I've got yet._ he thought to himself. Then without further ado, he 
set off along the track he'd left through the forest. It was as likely a direction to find water and food as anywhere 
and he wouldn't get lost. 

_So, you do know about the gods? There was a rebel right next to you when I woke up, but I'm pretty sure we took care of
her for the moment. You should take me to the nearest shrine to me, so I can get back to myself_ The voice in his head 
was insistent and kept up a litany of similar demands, request and comments for at least the next glass. Zamir refused 
to reply though, which seemed to make the voice confused at first. After Zamir had found a stream he'd crashed through 
on his way through the forest, he bend down and cupped his hands to take a sip, ignoring the constant complaints in his 
ears. That was until just as he was about to have a drink there was a sound like the earth opening and his headache came
back in full force. He clapped his hands to his ears, but to no avail, the water he was so desperate for spilt back into 
the stream it had come from. 

_Oh, so you can hear that then? How dare you ignore me, I am your god! Now take me to my temple! I saved you from the 
rebel earlier! You should be thanking me, you illiterate farmer._

"You are not my god. You are the result of a head injury. I shouldn't even be talking to you. I'm going to have a drink 
now." replied Zamir and put word to action and bent down to get another cupped hand of water. However as he reached for 
the water, spirits poured out of his outstretched hand and formed a cup from the surrounding clay. A brief but intense
flash of heat and suddenly, sat on the ground, was a well-formed cup with a handle, made of what looked like the kind 
of pottery they imported from the capital back in the village.

_See, I'm real. I live in your core and I am a God_ came the voice, smugly from inside his head. _So accept this 
largess from me_

Dumbfounded, Zamir looked at the cup, inspecting it in wonder. It was perfect in all respects, though a little warm to 
the touch. His wonder was caught short by the desperate thirst he found in himself. Without conscious bidding his hand
dipped the cup into the water and pulled it out, taking a long, deep drink of it. Though it was just a stream in a forest
and it largely tasted of mud and rotting leaves, it was the finest drink he'd ever had. Qodir's best soups couldn't 
compare to this muddy water after who knows how much time unconscious.

_So, now you've managed that aspect of your biology, you can tell me how you found the echo of a god, young man_ Arty 
commented in his head. 

"Young? I'm in my fortieth year. I have no clue how I found you, I was cleansing a field, there was a spirit storm, I 
was fine and then a god told me that I could become a god too. Then she helped me and then you appeared."

_What do you mean calling that rebel a god? When you get to civilisation, you're going to have a lot to explain to the 
local clergy about your apostasy. And you can't be that old, people's cores are activated with puberty, that's when they 
start to gather nanobots and coalesce. You can't have children running around with super strength or the ability to 
melt walls. How old are you really? Also, what year is it?_

"Firstly, Dravox is one of the only gods I've ever known. She, along with her brothers and sisters, defeated the old 
gods eight thousand years ago. Everyone, all of humanity, lives in the country they set up, surrounded by the Deadlands
where spirits will destroy anyone who ventures in there without the proper protection, except me for some reason. 
According to the gods, the real gods, there's not been a human with a core since the old gods died."

There was a long silence, which Zamir took to meant that the delusion in his head was mulling things over, _a useful 
delusion though_, he thought as he took another drink of water from his admittedly excellent cup. 

_You must be mistaken, the rebels could never have defeated us. They were just cored humans, we could take control of 
their nanomes at will if we wanted. They were never a serious threat, we only allowed them to continue to exist because
Sano-san insisted that a rebel unit would hit a great story beat, and honestly we were all just happy that he was out
of his depression. He really thought humanity should have been over after the last story arc. But you don't need to know
that so I'll just wipe the last few minutes from your memory and..._

The voice in his head had trailed off, but Zamir remembered everything that had been said, in fact he was pretty clear 
that even if he wasn't delusional, the voice in his head was.

"Yeah, I've not forgotten anything. But I will be walking back home now. Whatever it was you did, seems to have sent me
in a straight line, so I can just follow the setting sun home. Then Dravox can shut you up and I can have a normal core."

He scooped up another cup of water and drained it in one movement. He started to walk further along the trough he'd cut
through the forest when his ears were assaulted again by the idiot in his core. He'd been expecting that though, so 
it was annoying, and painful, but the shock value was gone. 

_You can't go back, I'll kill you if you try!_

"Do it, if I can't get back to my family and friends, what's the point in living? And if I die, then you're trapped. You
don't have a lot of power in this. You can't affect my mind, apparently, you can only annoy me." He stumbled as his 
vision was flooded with white light, so bright he couldn't see anything else. From the light a being coalesced in front
of him, an unnaturally tall being made of pure light.

_I am your god, and you will obey me! Wait, stop don't walk through me! Stop at once! I command you! You can't walk away
from me! I am in your head! You're not going to sleep until you listen to me._

This kept up for the next few hours as the sun set and Zamir walked doggedly on. There were fruits he recognised and 
he ate them as he found them. His progress was slowed by the constant interruptions and complaints from his passenger. 

As he walked, he looked around the forest when he could. The air was warm, and strangely thick. The forest was strangely 
familiar, the trees were very similar to those he knew from the forests of his home. He saw flowers and bushes that were
familiar too. The forest was devoid of larger life though, he didn't see signs of animal trails, or even spoor. 
There were a lot of smaller animals in the air though, like birds, but very much smaller. As he looked closer, he saw
that they had extra legs, and strange eyes. 

_Why do you keep looking at insects? They're everywhere, the basis of life everywhere on land. But if you want to know 
more about them, I have some basic knowledge I'll happily share with you, if only you head over toward that hill._ A 
hitherto unseen hillock was outlined in a green more violent than anything that Pavel could have worn. _After all, I 
won't be able to change your mind it seems, so I might as well make myself more useful to you._

"Nice about face, but I don't believe you. Why would I go up the hill? The last hill I went to turned out to make you 
happen to me. I'm not a child, I've been around.", Zamir kept walking onward, following the setting sun. 

****

The morning dawned bright and cold, Dravox smiled at the young man next to her, he'd definitely helped lift her mood. 
There was no point putting it off, so she strode out of the wayhouse and over to the temple. 

"You could have at least gotten dressed", [weather god] said to her. "Here's your gear anyway." he indicated a pile on 
the floor in front of the temple. 

"I'm happy with my body, I don't get cold, why should I get dressed?" She asked, putting the lie to her words by 
immediately pulling out a set of strange clothing from the bundle in front of her. "Ah my old armour, look at this stuff!"
They certainly don't make it like that any more. Take a lightning bolt without leaving a mark, that will!" she said 
proudly as she flicked the jet black plates with a finger nail. Putting the armour to one side, she dug deeper in the 
pack until she triumphantly pulled out what looked like a large black cloth. As she pulled it over herself it became 
apparent to the waiting crowd that had grown around the two gods, that it was in fact an undergarment for the armour. 
It was so skin tight though, that it didn't have any real concealing properties. Then Dravox pulled out a set of thicker 
clothes which went over her torso, thighs and calves, leaving her arms mostly free. Finally she enlisted [weather god]
to help her buckle on the armour.

The rest of the bundle was a strange looking knapsack, which appeared to be made of the same material as the Temple made
mats, then the finale came. [weather god] took the pack of his own back and took out a shield that he handed to Dravox. 
"Here's your shield, we noticed that you've been keeping it oiled. Have you been waiting for an excuse to do this? You 
can take the god out of the Adventurer's society, but you can't take the adventurer out of the god I guess."

"We're not gods, and you know it. But yes, I did oil it and my sword, which I note you're still holding. Better to be 
prepared for these things. Give me my baby." Dravox held her hand out and smiled as [weather god] deposited her sheathed
sword in it, along with its belt. With a motion born of long practice, she fastened it around her waist with the minimum
of motion. Then with a grin fit to take the top off her head, she unsheathed the sword. 

Against the anticipation of, what was by now nearly the whole village, the blade was not the shining silver that good
steel would be. It was a jet black blade that seemed to drink in the light. "This, good people, is a nightblade. It was
forged from the bones of the shadow dragon and it can cut through..." she darted forward, faster than a basic human eye 
could see, and without apparent effort, sliced the corner stones of the temple, causing a large chunk to fall to the 
road surface. "anything." With a look of immense satisfaction, she sheathed it in the scabbard which had the same inky 
blackness inside it. 

"It's with this that I will find Zamir, I will punish whoever it is that took him, be they human or god, and I will 
bring him back." The utter conviction in her voice rallied the crowd, and even Pavel and Amira seemed heartened. 

She clapped her hand to the shoulder of [weather god] with a force that would have crushed a normal human. "Look after 
yourself, and the people of this village. I think the time has come for us to walk among the people again. Let's keep 
everyone safe. See you later everyone!" with these last words, Dravox bent her knees, crouching in the middle of the 
road and then abruptly straightened her legs and rocketed out of the village, leaping from the centre to well past its 
edge. 

Brushing the dust from the cracked cobbles from his front,[weather god] sighed. "She does so like being dramatic."

***

Night in the forest was strange. There were more of the strange small birds, insects was what Arty had called them in
between his abuse and shouting. The voice in his head had been quiet for hours now though, ever since he had just 
ignored it and walked along the trough for a while before trying to sleep in a tree. The fact he couldn't see any 
animals like deer or wolves didn't mean much. He was pretty sure that Arty was somehow responsible, as he had seen the 
signs of life that weren't direct. The new growth on a bush that had been grazed on a few weeks before, the scars across 
a low bough where a stag had managed to scrape it with his antlers when he was in must. There were definitely deer here
and they had been here recently. He'd clearly come to some time after noon, and aside from the insects, he hadn't seen
anything unusual. He'd not sensed many spirits either, the whole forest was nearly as clean as the forests back home. 

This was a problem for Zamir, the gods, the Temple and everyone knew that the Deadlands covered the whole world, except 
for what they'd cleansed. But this was perfectly serviceable land. Indeed, this was exactly the kind of forest he'd 
expected to spend his days hunting in when he was a child. Honestly, if he didn't have to get back to his daughter and 
see his grandchild grow up, Zamir would have been happy to spend his days here in this forest. Without his annoying 
core of course. But at night there were small night birds that, when they sometimes flew into him, had furry bodies, 
like mice with wings. There were the same stars, but without the mountains to frame them, it felt like he could fall 
into the sky. The forest would have helped, but suspicious that there were wolves, Zamir had climbed 
the highest tree he could find. There wasn't a mountain to be seen at the horizon, which was somewhat worrying to Zamir. 

Despite the strange situation and the lack of proper shelter, Zamir fell asleep readily. The hunger in his belly somewhat
assuaged by the fruits he'd found along the way, though he craved something like meat or fish or beans. He resolved that 
if he didn't find something like that tomorrow, he'd try and catch some of the flying mice.

The next morning was clouded and threatened rain. There had still been no sign of his passenger since the hill the day 
before. So he carefully let himself down from the hollow where the three large branches of the oak tree he'd slept in 
met at the trunk, and then dropped to the forest floor. The first thing he noticed was the prints in the soil of a deer. 
He'd spent the last day looking for tracks, and found none. If there was a deer, there was a chance he could get some
meat. Admittedly all he had to use for this was the ceremonial knife he used for cleansing rituals. He'd buckled it on 
automatically as he left to head into the Deadlands and hadn't really thought about it. The tracks were a day or two old
but they'd either lead to food or water, either way he'd have an improvement on his current situation. 

Zamir looked around more closely, to see what else he'd missed in the previous day, or that had been hidden from him, 
but very little showed up, a few truly old tracks under a bush, more of the insects streaming in and out of a nest which
smelt a lot like it had honey in. Which was strange, because honey came from the flower buds of the honeyrose. Sure it 
was thorny, but you squeezed those buds and out came delicious, sweet honey. Clearly things were different here, which 
was to be expected outside the Deadlands. 

He gave up his search and decided to follow the trail. He'd been set on just pressing back home as soon as possible 
yesterday, but he was starting to realise that it was a fool's task without proper preparation. He needed clothing, 
food, water, shelter and not necessarily in that order. Finding and trapping or hunting a deer would go a long way to 
solving many of his immediate problems. Fire would hopefully be fairly easy, he'd made and used a firebow when he didn't 
have a flint and steel to hand before. Then again if he found a flint, his knife would serve to spark it. With that 
in mind, he started to gather some of the dry leaves from the forest floor and tuck them under his belt, so he had 
tinder to be ready when the time came. He also blazed the trees by the tracks, using his very sharp knife to peel off
a diamond of bark, so he could find his way back to the trough, should he need to. 

A glass or so later, he saw that the trail he was following was crossed by a much fresher one, perhaps only laid this 
morning. Marking a blaze on the tree next to the trail in the shape of a cross, so he would know when to turn back toward
the furrow. He scented water in the air, and knew that this was definitely going to be his best chance at getting meat 
that day. 

The bushes were thick to the sides of the track, probably why the deer followed this route, but to approach them unseen, 
he'd have to maneuver off the track and into the undergrowth. Which when you were dressed largely in rags was not a 
pleasant idea. The necessity of food, clothing and other materials couldn't be denied though, and the thought of eating 
venison set his mouth watering. He climbed, carefully, over the first bush on the downwind side of the track. He'd chosen 
a fairly soft bush he knew from home, it had somewhat stiff, waxy leaves, but no thorns or poisons to irritate him.  
On the other side he found that the ground was nearly completely covered with leaf fall from previous years, meaning 
that a silent approach was not going to be possible. Still the water couldn't be too far away, given the smell, the wind
was brisk and would cover his noise well, especially if he kept downwind of the track. So he set off, carefully picking 
his way around brambles, stinging nettles and other interestingly painful plants he knew from home. If he'd never seen a 
plant before, it was definitely avoided. There was only one rule that every hunter had told him when he was a child. 
Be patient. You couldn't rush in and hope to get a kill, so you might as well take your time, be careful and safe, and 
you'd be more likely to get a meal at the end of it and come home safely. 

So he was careful and slow as he moved through the forest. It was a good thing too, because the thick undergrowth had 
hidden exactly how close the watering hole was. Barely fifty paces, along the track, would have seen him in the clearing 
where a pool of water looked very inviting indeed. But he saw something that was even more inviting. It was an old doe, 
white at the muzzle, eyes half fogged. She was lying by the pool, maybe half a pace from the edge of the clearing, on a 
large stone that was bathed in what sunlight came through the clouds. It was a long, slow process, but he edged his way 
closer to the side of the glade where she lay, inching through the leaf litter when the wind blew strongly to hide the 
sound of his passage. At the end he leapt forward to grab the deer, who skittered awake, but in her hurry slipped on the
damp rock, the crack of her breaking leg echoing through the forest. He used his knife to put her out of her misery, 
silently thankful that his hunt had been successful when he had no right to expect it to be. 

Once the body was securely wedged into the branches of a tree just a few steps back into the forest, he slit its throat
and let the still hot blood drain from it. You could get ill leaving the blood in. He then took his miraculously 
unbroken cup and went back to the pool to get some water. It would take him much of the rest of the day to dress and 
butcher the deer. And he wanted a fire to cook it over. The idea of hot meat was too much to pass over, so he scoured 
the edge of the pool as he headed to the stream that fed it, looking for a flint.