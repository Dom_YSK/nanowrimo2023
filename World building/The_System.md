Core stages; 

Seed:
0: can absorb spirits/demons.

Novitiate:
0:can absorb spirits/demons
1: minor storage of spirits/demons internally
2: access to class archetypes, potentially specialised classes
3: Neural link enabled
4: Basic circuits for controlling spirits internally

Core bearer:
0: can absorb spirits/demons
1: moderate storage of spirits/demons internally (class dependant)
2: access to more specialised classes
3: cross-functional circuits for controlling spirits internally
4: very basic circuits for controlling spirits externally for short times (class dependent.) 

